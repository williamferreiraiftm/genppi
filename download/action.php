<?php
  $os  = $_POST['OS'];
  $ram = $_POST['RAM'];
  $opt  = $_POST['OPT'];
  $file = "genppi".$opt.$ram."g-".$os;
  $download_name = "genppi".$opt;
  if ( $os == "Win" ){
     $file = $file.".exe";
     $download_name = $download_name.".exe";
  }else{
    if ( $os == "Mac" ){
       $file = $file.".x";
       $download_name = $download_name.".x";
    }
  }
  
if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($download_name).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;
} else {
    echo "File downloading failed. Please, contact santosardr@ufu.br to solve this issue.";
}
?>

