(declaim (optimize (speed 3) (debug 0) (safety 0) (space 0)))
;;;;Implementação do software Genppi para predição de ppis----------------------
;;;;Projeto de Mestrado em Ciência da Computação - UFU.
;;;;Aluno: William Ferreira dos Ajos
;;;;----------------------------------------------------------------------------

;;Variáveis globais utililizadas nas funções------------------------------------
(defparameter *genomas* (make-hash-table :test #'equalp)
  "Tabela hash para armazenar os genomas de uma análise")
(defparameter *pan-genoma* (make-hash-table :test #'equalp)
  "Tabela hash para armazenar o pan-genoma dos genomas")
(defparameter *perfil-filogenetico* (make-hash-table :test #'equalp)
  "Tabela hash para armazenar o perfil filogenético das
   proteínas que estão em mais de um genoma")
(defparameter *agrupamento-por-perfis-filos-identicos* (make-hash-table :test #'equalp)
  "Tabela hash para fazer o relatório de perfis filogenéticos")
(defparameter *agrupamento-por-perfis-filos-semelhantes* (make-hash-table :test #'equalp)
  "Tabela hash para fazer o relatório de perfis filogenéticos")
(defparameter *pesos-grupos* (make-hash-table :test #'equalp)
  "Tabela hash para fazer o relatório de perfis filogenéticos")
(defparameter *genome-ppi* (make-hash-table :test #'equalp)
  "Tabela hash para armazenar a ppi de cada genoma")
(defparameter *relatorio-vizinhanca-genica* (make-hash-table :test #'equalp)
  "Tabela hash para armazenar o registro do número de genes conservados
   e não conservados para cada expanção de um gene pivô do pan-genoma")
(defparameter *genomes-files* (list)
  "Lista para armazenar os nomes de arquivos de genomas")
(defparameter *fusoes* (make-hash-table :test #'equalp)
  "Tabela hash para armazenar a ppi de cada genoma")
(defstruct proteina localidade similares)
(defstruct expansao localidade conservacoes)
(defstruct fusion ppi rosetta-stone)
(defparameter *ppi-identified-cn* nil
  "Variável global usada na função genppi para dizer se foi
   constatado ppi por vizinhaça gênica conservada ou não")
(defparameter *ppi-identified-pp* nil
  "Variável global usada na função genppi para dizer se foi
  constatado ppi por perfil filogenético conservada ou não")
(defparameter *ppi-identified-gf* nil
  "Variável global usada na função genppi para dizer se foi
   constatado ppi por fusão gênica conservada ou não")

(defstruct ppi-struct (genea "" :type string) (geneb "" :type string  ) (weight 0.0 :type short-float ) (position 0 :type integer))
;;------------------------------------------------------------------------------

;Ler arquivo e coloca-lo em uma lista. Neste caso, uma lista com sub-listas,
;onde, cada sub-lista, conterá dois elementos, o nome da proteina
;e sua sequencia de aminoacidos respectivamente.
;(read-fasta "/home/william/PPIs-Predictor/histograma/sequences.fasta")---------
(declaim (ftype (function ( pathname ) ) read-fasta))
(defun read-fasta (filename)
  ;Le arquivo. Caso o mesmo nao exista, rertorna nil.
  (let ((in (open filename :if-does-not-exist nil))
       	(fastalist nil)
       	(fastaname nil)
       	(proteins nil)
       	(nonaalist (list '+ '- '* '\. '\# '\ ))
        );let init block
    (when in;Quando in nao for nil, ou seja, quando o arquivo foi lido corretamente.
      ;Faz um loop
      ;Este loop, lê a primeira linha do arquivo(cabeçalho da primeira proteína), e atribui à variável fastaname.
      (loop for line = (read-line in nil) until (not (zerop (length line)))
     	  finally (setq fastaname line)
        );loop para pular linhas em branco
      ;(return)
      ;Este when só é executado quando o arquivo lido for um fasta, ou seja, que tenha '> no início.
      (when (char= (elt fastaname 0) #\>) ;verifica o formato fasta.
        (loop for line = (read-line in nil)
       	  while line do
     	    (when (> (length line) 0);quando há conteúdo na linha lida.
     	      (if (char= (elt line 0) #\>);se encontrado novo cabeçalho de proteina.
          		  (progn
         		    ;laco para remover caracteres que não são aminoácidos (nome do cabeçalho).
         		    (dolist (nonaa nonaalist fastalist)
                     		(setq fastalist (delete nonaa fastalist))
                       );dolist

               ;Atribui a variavel proteins, uma lista de proteinas, onde, cada proteina é uma lista de dois elementos,
               ;o nome de cabecalho da proteina e sua sequencia de aminoacidos.
         		    (setq proteins (append proteins (list (list (subseq fastaname 1) fastalist))))

         		    (setq fastaname line fastalist nil)

           		  );progn. Aqui termina as acoes para um novo cabecalho de proteina encontrado.

          		  (setq fastalist (append fastalist
                                      (loop for aa across line collect (intern (string-upcase (string aa)))
                                        );lopp
                                     	);append
                    );setq
              );se encontrado novo cabeçalho
         	  );quando há conteúdo
          );loop para ler uma sequência

        (when fastalist;quando fastalist nao for nil
         	;laco para remover caracteres de não aminoácidos (nome do cabeçalho)
        	 (dolist (nonaa nonaalist fastalist)
              	   (setq fastalist (delete nonaa fastalist))
                  );dolist

          ;Atribui a variavel proteins, uma lista de proteinas, onde, cada proteina é uma lista de dois elementos,
          ;o nome de cabecalho da proteina e sua sequencia de aminoacidos.
        	 (setq proteins (append proteins (list (list (subseq fastaname 1) fastalist))))

          );when fastalist
        );when fasta format
      (close in)
      );when in
    proteins ;retorna uma lista de listas, onde cada elemente da lista raiz, é outra lista
    ;contendo o nome e a sequencia de aminoacidos da proteina.
    );let
  )
;-------------------------------------------------------------------------------

;funçao que gera o histograma das proteinas, ou seja, a distribuição de frequências
;em que cada aminoacido aparece nas proteina.
;(histo-fasta "/home/william/PPIs-Predictor/histograma/sequences.fasta");-------
(declaim (ftype (function ( pathname ) ) histo-fasta))
(defun histo-fasta( filename )
  (let  ((multifastalist (read-fasta filename ))
       	 (aatargets (list 'A 'R 'N 'D 'C 'E 'Q 'G 'H 'I 'L 'K 'M 'F 'P 'S 'T 'W 'Y 'V 'U 'O 'X 'B 'Z 'J));26 aminoacidos
       	 (histolist nil)
       	 (fastaname)
       	 (histogram nil))
    ;Este dolist varre a lista de proteinas do arquivo lido pela funcao read-fasta.
    (dolist (fastalist multifastalist histogram)
            ;o setq abaixo atribui a variavel fastaname, o nome de cabecalho da proteina n.
            (setq fastaname (concatenate 'string (string (gensym))"."(car fastalist))
             	    histolist nil
                  );setq
            ;Este dolist varre a lista de aminoacidos definada em aatargets, e conta a quantidade de vezes
            ;que cada aminoacido contido em aatargets, aparece na proteina n.
            (dolist (aa aatargets histolist)
                   	(setq histolist (append histolist (list (count aa (cadr fastalist)))))
                    );inner do                                        ;cadr pega o segundo elemento da lista, neste caso,
            ;a sequencia de aminoacidos da proteina n.

            ;Aqui, com a func append, esta sendo feita a mesclagem das proteinas alanisadas,
            ;criando um lista unica com sub-listas, onde cada sub-lista é uma representacao
            ;do histograma da proteina, contendo o nome da proteina e a frequencia de vezes que cada aminoacido
            ;analizado aparece na mesma.
            (setq histogram (append histogram (list (list fastaname histolist))))
            );outer do
    histogram; retorna o histograma das proteinas.
    );let
  )
;-------------------------------------------------------------------------------

;Funções para quebrar um lista numa dada posicao--------------------------------
(declaim (ftype (function ( cons fixnum ) ) split-up-to))
(defun split-up-to (lista pos)

  (declare (type cons lista))
  (declare (type fixnum pos))

  (cond
    ((<= pos 0) ())
    ((null lista) lista)
    (t (cons (car lista) (split-up-to (cdr lista) (1- pos))))
    )
  )

(declaim (ftype (function ( cons fixnum ) ) split-after))
(defun split-after (lista pos)

  (declare (type cons lista))
  (declare (type fixnum pos))

  (cond
    ((<= pos 0) lista)
    ((null lista) ())
    (t (split-after (cdr lista) (1- pos)))
    )
  )

(declaim (ftype (function ( cons fixnum ) ) split))
(defun split (lista pos)

  (declare (type cons lista))
  (declare (type fixnum pos))

  (list (split-up-to lista pos) (split-after lista pos))
  )
;-------------------------------------------------------------------------------

;;Função para testar se duas proteínas são similares via histograma-------------
(declaim (ftype (function ( cons cons fixnum fixnum ) ) similar-test))
(defun similar-test( seqA seqB aadifflimit checkpointminlimit )

  (declare (type cons seqA))
  (declare (type cons seqB))
  (declare (type fixnum aadifflimit))
  (declare (type fixnum checkpointminlimit))

  (let ((checkpoint 0))

    (loop
      for nA in (cadr seqA)
      for nB in (cadr seqB)

      ;;Quando a diferenca de histograma entre nA(amino n da seqA) e nB(amino n da seqB)
      ;;for <= ao limite de diferenca tolerada e definida como parametro da funcao (aadifflimit),
      ;;significa-se que as duas proteinas sao simimilares em realacao ao amino n. Neste caso,
      ;;incrementa-se o checkpoint, que contabiliza a quantidade de aminoacidos que estao dentro
      ;;da diferenca tolerada. Por final, no if, verifica se a quantidade de aminoacidos
      ;;considerados similares, é >= ao checkpointminlimit(quantidade de aminoacidos considerados na análise).
      do (when (<= (abs (- nA nB)) aadifflimit ) (incf checkpoint) )
      );loop
    (if (>= checkpoint checkpointminlimit) t nil)
    );let
  )
;-------------------------------------------------------------------------------

;;Bloco de funções para ler arquivos de um diretório----------------------------
(defun component-present-p (value)
  (and value (not (eql value :unspecific))))

(defun directory-pathname-p (p)
  (and
   (not (component-present-p (pathname-name p)))
   (not (component-present-p (pathname-type p)))
   p))

(defun pathname-as-directory (name)
  (let ((pathname (pathname name)))
    (when (wild-pathname-p pathname)
      (error "Can't reliably convert wild pathnames."))
    (if (not (directory-pathname-p name))
      (make-pathname
       :directory (append (or (pathname-directory pathname) (list :relative))
                          (list (file-namestring pathname)))
       :name
       nil
       :type
       nil
       :defaults pathname)
      pathname)))

(defun directory-wildcard (dirname)
  (make-pathname
   :name :wild
   :type #-clisp :wild #+clisp nil
   :defaults (pathname-as-directory dirname)))

(defun list-directory (dirname)
  (when (wild-pathname-p dirname)
    (error "Can only list concrete directory names."))
  (let ((wildcard (directory-wildcard dirname)))
    #+(or sbcl cmu lispworks)
    (directory wildcard)
    #+openmcl
    (directory wildcard :directories t)
    #+allegro
    (directory wildcard :directories-are-files nil)
    #+clisp
    (nconc
     (directory wildcard)
     (directory (clisp-subdirectories-wildcard wildcard)))))
;;------------------------------------------------------------------------------

;;Função para carregar asquivos de proteínas e gerar seus histogramas-----------
(declaim (ftype (function (cons ) ) histo-genomas))
(defun histo-genomas (files);files: Lista de arquivos de proteínas
  (defparameter *ppi-identified-cn* nil)
  (defparameter *ppi-identified-pp* nil)
  (defparameter *ppi-identified-gf* nil)
  (defparameter *genomas* (make-hash-table :test #'equalp))
  (defparameter *genomes-files* (list))
  (let ((laco 0)(genomenumber 0))

    (declare (type boolean *ppi-identified-cn*))
    (declare (type boolean *ppi-identified-pp*))
    (declare (type boolean *ppi-identified-gf*))
    (declare (type hash-table *genomas*))

    (format t "Generating amino acids histogram;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (dotimes (i (length files))
             (unless (or (eql (pathname-type (pathname (elt files i))) nil)
                         (not
                          (or (string= (pathname-type (pathname (elt files i))) "fa")
                              (string= (pathname-type (pathname (elt files i))) "faa")
                              (string= (pathname-type (pathname (elt files i))) "fasta")
                              );or
                          );not
                         );or
               ;A menos que o caminho i seja um diretório, ou não seja um arquivo fasta, faça:
               (setf *genomes-files* (append *genomes-files* (list (pathname-name (pathname (elt files i))))))
               ;;Gera o histograma para cada arquivo de proteínas i.
               (setf (gethash (pathname-name (pathname (elt files i))) *genomas*) (histo-fasta (elt files i)))
               );unless

             (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length files)) 100)) laco)))
                      (format *query-io* "=")
                      (force-output *query-io*)
                      )
             (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length files)) 100)) laco))))
             (incf genomenumber)
             );dolist
    (format t "]~%")
    );let
  );defun
;;------------------------------------------------------------------------------

;;Bloco de funções para gerar o pan-genoma--------------------------------------
(declaim (ftype (function (fixnum fixnum ) ) pan-genoma-1))
(defun pan-genoma-1 (aadifflimit checkpointminlimit)

  (defparameter *pan-genoma* (make-hash-table :test #'equalp))
  (defparameter *perfil-filogenetico* (make-hash-table :test #'equalp))
  (defstruct proteina localidade similares)

  (let ((pangenoma (list (list (list (string (gensym)) (list)))))
        (similares (list))
        (perfil-filo (list))
        (laco 0)
        (genomenumber 0)
        )

    (declare (type hash-table *pan-genoma*))
    (declare (type hash-table *perfil-filogenetico*))
    (declare (type fixnum aadifflimit))
    (declare (type fixnum checkpointminlimit))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))

    (format t "~%Generating pan-genome;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (loop for j being the hash-keys in *genomas* using (hash-value proteins)
      do (progn
          (dolist (p proteins)
                  (dolist (gp pangenoma)
                          (if (similar-test p (first gp) aadifflimit checkpointminlimit)
                            (progn
                             (setf (third gp) (append (third gp) (list (list j (position p proteins :test #'equalp)))))
                             (setf similares (append similares (list (second gp))))
                             );
                            );if similar-test
                          );dolist pangenoma
                  (setf pangenoma (append pangenoma (list (list p (list j (position p proteins :test #'equalp)) similares))))
                  (setf similares nil)
                  );dolist proteins
          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *genomas*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *genomas*)) 100)) laco))))
          (incf genomenumber)
          );progn
      );loop hash table *genomas*

    (setf pangenoma (rest pangenoma))
    (dolist (p pangenoma)
            (unless (eql (third p) nil)
              (setf (gethash (first (first p)) *pan-genoma*)
                    (make-proteina :localidade (second p)
                                   :similares (third p)
                                   );make-proteina
                    );setf pangenoma
              (setf perfil-filo (remove-duplicates (append (list (first (second p))) (loop for i in (third p) collecting (first i))) :test #'string=))
              (if (> (length perfil-filo) 1)
                (setf (gethash (first (second p)) *perfil-filogenetico*)
                      (append (gethash (first (second p)) *perfil-filogenetico*)
                              (list (list (first (first p)) perfil-filo (second (second p))))
                              );append
                      );setf perfil filogenetico
                );if remove-duplicates
              );unlles
            );dolist pangenoma
    (format t "]~%")
    );let
  );defun
;-------------------------------------------------------------------------------

(declaim (ftype (function (fixnum fixnum fixnum fixnum) ) pan-genoma-2))
(defun pan-genoma-2 (aadifflimit checkpointminlimit ppaadifflimit ppcheckpointminlimit)

  (defparameter *pan-genoma* (make-hash-table :test #'equalp))
  (defparameter *perfil-filogenetico* (make-hash-table :test #'equalp))
  (defstruct proteina localidade similares)

  (let ((pangenoma (list (list (list (string (gensym)) (list)))))
        (similares (list))
        (perfil-filo-p (list))
        (laco 0)
        (genomenumber 0)
        )

    (declare (type hash-table *pan-genoma*))
    (declare (type hash-table *perfil-filogenetico*))
    (declare (type fixnum aadifflimit))
    (declare (type fixnum checkpointminlimit))
    (declare (type fixnum ppaadifflimit))
    (declare (type fixnum ppcheckpointminlimit))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))

    (format t "~%Generating pan-genome;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (loop for j being the hash-keys in *genomas* using (hash-value proteins)
      do (progn
          (dolist (p proteins)
                  (dolist (gp pangenoma)
                          (if (similar-test p (first gp) aadifflimit checkpointminlimit)
                            (progn
                             (setf (third gp) (append (third gp) (list (list j (position p proteins :test #'equalp)))))
                             (setf similares (append similares (list (second gp))))
                             );
                            );if similar-test
                          );dolist pangenoma
                  (setf pangenoma (append pangenoma (list (list p (list j (position p proteins :test #'equalp)) similares))))
                  (setf similares nil)
                  )
          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *genomas*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *genomas*)) 100)) laco))))
          (incf genomenumber)
          );dolist proteins
      );loop hash table *genomeas*

    (setf pangenoma (rest pangenoma))
    (dolist (p pangenoma)
            (unless (eql (third p) nil)
              ;;inserindo na tabela hash pan-genoma.
              (setf (gethash (first (first p)) *pan-genoma*)
                    (make-proteina :localidade (second p)
                                   :similares (third p)
                                   );make-proteina
                    );setf pangenoma

              ;;Varrendo os genes similares a p para fazer novo teste de similaridade com p.
              (dolist (similar (third p))
                      (if (similar-test (elt (gethash (first (second p)) *genomas*) (second (second p)))
                                        (elt (gethash (first similar) *genomas*) (second similar))
                                        ppaadifflimit ppcheckpointminlimit
                                        )
                        (setf perfil-filo-p (append perfil-filo-p (list (first similar))))
                        );if similar-test
                      );dolist similar

              ;;inserindo na tabela hash perfil-filogenetico
              (setf perfil-filo-p (remove-duplicates (append (list (first (second p))) perfil-filo-p) :test #'string=))
              (if (> (length perfil-filo-p) 1)
                (setf (gethash (first (second p)) *perfil-filogenetico*)
                      (append (gethash (first (second p)) *perfil-filogenetico*)
                              (list (list (first (first p)) perfil-filo-p (second (second p))))
                              );append
                      );setf perfil filogenetico
                );if remove-duplicates
              ;;Resetando a variavel perfil-filo-p
              (setf perfil-filo-p nil)
              );unlles
            );dolist pangenoma
    (format t "]~%")
    );let
  );defun
;-------------------------------------------------------------------------------

;;Função para predizer ppi por fusão gênica-------------------------------------
(declaim (ftype (function (fixnum fixnum single-float) ) rosetta-stone))
(defun rosetta-stone(aadifflimit checkpointminlimit percentage-rs)

  (defparameter *fusoes* (make-hash-table :test #'equalp))
  (defstruct fusion ppi rosetta-stone)

  (let ((fusoes (list))
        (gene-a)
        (gene-b)
        (fusao)
        (posicao-p)
        (qtd-genes)
        (ppi-fusoes (make-hash-table :test #'equalp))
        (laco 0)
        (genomenumber 0)
        )

    (declare (type hash-table *fusoes*))
    (declare (type hash-table ppi-fusoes))
    (declare (type fixnum aadifflimit))
    (declare (type fixnum checkpointminlimit))
    (declare (type single-float percentage-rs))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))

    (format t "~%Predicting ppi by gene-fusion;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (loop for g being the hash-keys in *genomas* using (hash-value proteins)
      do (progn
          (setf qtd-genes (length proteins))
          (dolist (p proteins)
                  (setf posicao-p (position p proteins :test #'equalp))
                  (cond
                    ((< posicao-p (- qtd-genes 2))
                     (progn
                      (setf gene-a p)
                      (setf gene-b (elt proteins (+ posicao-p 1)))
                      (setf fusao (list "fusao" (mapcar #'+ (second gene-a) (second gene-b))))
                      (setf fusoes (append fusoes (list (list fusao (list g (first gene-a) (first gene-b) posicao-p) (list)))))
                      #|(setf gene-b (elt proteins (+ posicao-p 2)))
                      (setf fusao (list "fusao" (mapcar #'+ (second gene-a) (second gene-b))))
                      (setf fusoes (append fusoes (list (list fusao (list g (first gene-a) (first gene-b) posicao-p) (list)))))|#
                      )
                     );fim condição 1 e ação 1.
                    ((= posicao-p (- qtd-genes 2))
                     (progn
                      (setf gene-a p)
                      (setf gene-b (elt proteins (1+ posicao-p)))
                      (setf fusao (list "fusao" (mapcar #'+ (second gene-a) (second gene-b))))
                      (setf fusoes (append fusoes (list (list fusao (list g (first gene-a) (first gene-b) posicao-p) (list)))))
                      #|(setf gene-b (elt proteins 0))
                      (setf fusao (list "fusao" (mapcar #'+ (second gene-a) (second gene-b))))
                      (setf fusoes (append fusoes (list (list fusao (list g (first gene-a) (first gene-b) posicao-p) (list)))))|#
                      )
                     );fim condição 2 e ação 2.
                    #|((= posicao-p (- qtd-genes 1))
                       (progn
                        (setf gene-a p)
                        (setf gene-b (elt proteins 0))
                        (setf fusao (list "fusao" (mapcar #'+ (second gene-a) (second gene-b))))
                        (setf fusoes (append fusoes (list (list fusao (list g (first gene-a) (first gene-b) posicao-p) (list)))))
                        (setf gene-b (elt proteins 1))
                        (setf fusao (list "fusao" (mapcar #'+ (second gene-a) (second gene-b))))
                        (setf fusoes (append fusoes (list (list fusao (list g (first gene-a) (first gene-b) posicao-p) (list)))))
                        )
                       );fim condição 3 e ação 3.|#
                    );cond
                  (dolist (f fusoes)
                          (unless (or (equalp (first p) (second (second f))) (equalp (first p) (third (second f))))
                            (if (similar-test p (first f) aadifflimit checkpointminlimit)
                              (progn
                               (setf (third f) (append (third f) (list (list (first p) g))))
                               );
                              );if similar-test
                            );unless
                          );dolist fusoes
                  );dolist proteins
          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *genomas*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *genomas*)) 100)) laco))))
          (incf genomenumber)
          );progn
      );loop hash table *genomas*

    (dolist (f fusoes)
            (unless (eql (third f) nil)
              (setf *ppi-identified-gf* t)
              (setf (gethash (first (second f)) ppi-fusoes)
                    (append (gethash (first (second f)) ppi-fusoes)
                            (list
                             (make-ppi-struct
                              :genea (second (second f))
                              :geneb (third (second f))
                              :weight (* 1 percentage-rs)
                              :position (fourth (second f))
                              )
                             );list
                            );append
                    );setf
              (setf (gethash (first (second f)) *fusoes*)
                    (append
                     (gethash (first (second f)) *fusoes*)
                     (list (make-fusion :ppi (list (second (second f)) '-- (third (second f)))
                                        :rosetta-stone (third f)
                                        );make-fusion
                           );list
                     );append
                    );setf fusoes
              );unlles
            );dolist pangenoma
    (format t "]~%")
    (return-from rosetta-stone ppi-fusoes)
    );let
  );defun

;;Bloco de funções para predizer ppis por Perfil Filogenético-------------------
(declaim (ftype (function (cons cons) ) comparar-perfis))
(defun comparar-perfis (lista1 lista2)
  (unless (eql nil (set-exclusive-or lista1 lista2 :test #'string=))
    (length(set-exclusive-or lista1 lista2 :test #'string=))
    )
  )

(declaim (ftype (function (single-float fixnum) ) phylogenetic-profiles-complete))
(defun phylogenetic-profiles-complete (percentage-pp pptolerance)
  (defparameter *agrupamento-por-perfis-filos-identicos* (make-hash-table :test #'equalp)
    "Tabela hash para fazer o relatório de perfis filogenéticos")
  (defparameter *agrupamento-por-perfis-filos-semelhantes* (make-hash-table :test #'equalp)
    "Tabela hash para fazer o relatório de perfis filogenéticos")
  (defparameter *pesos-grupos* (make-hash-table :test #'equalp)
    "Tabela hash para fazer o relatório de perfis filogenéticos")
  (let ((ppi (list));;Lista para armezenas as interações criadas.
                    (grupos-identicos (list));Lista para armazenar os agrupos de proteínas com perfis idênticos.
                    (grupos-similares (list));Lista para armazenar os agrupos de proteínas com perfis idênticos e semelhantes.
                    (pesos-grupos (list))
                    (posicao-grupo);Variável para armazenar a posição de um grupo g.
                    ;(number-proteins-max-group);Variável para armezar o número de proteínas do maior grupo.
                    (soma-grupos 0);Variável para armazenar o número total de proteínas entre grupos.
                    (peso-grupo);Variável para armazenar o peso que será atribuído às proteínas de um grupo g.
                    (dividendo);Variável que armazenar o índice de um grupo de tamanho t, que esteja mais próximo do maior grupo.
                    (divisor);Variável que armazenar o índice do maior grupo.
                    (diferenca-perfis);Variável para armazenar a difença dos perfis filogenéticos de dois grupos.
                    (ppi-hash-table (make-hash-table :test #'equalp));Tabela hash para guardar a ppi de cada genoma.
                    (laco 0)
                    (genomenumber 0)
                    )

    (declare (type hash-table *agrupamento-por-perfis-filos-identicos*))
    (declare (type hash-table *agrupamento-por-perfis-filos-semelhantes*))
    (declare (type hash-table *pesos-grupos*))
    (declare (type hash-table ppi-hash-table))
    (declare (type single-float percentage-pp))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))

    (format t "~%Predicting ppi by phylogenetic profiles;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (loop for k being the hash-keys in *perfil-filogenetico* using (hash-value perfis)
      do (progn
          ;;Agrupando as proteínas por perfil filogenetico----------------------------------------------------------------------------------------------------
          (dolist (p perfis)
                  (setf posicao-grupo (position-if #'(lambda (x) (eql nil (set-exclusive-or (second p) (first x) :test #'string=))) grupos-identicos))
                  (if (eql posicao-grupo nil)
                    (push (list (second p) (list (list (first p) (third p)))) grupos-identicos)
                    (push (list (first p) (third p)) (second (elt grupos-identicos posicao-grupo)))
                    );if
                  );dolist
          ;;Final do Agrupamento-------------------------------------------------------------------------------------------------------------------------------

          ;;Ordenando as proteínas de cada grupo
          (dolist (g grupos-identicos)
                  ;;Ordenando as proteínas de cada grupo de acordo com o seu índice no gemoma.
                  (setf (second g) (sort (second g) #'< :key #'second))
                  ;;Concatenando a um grupo g, o tamanho do grupo g.
                  (setf g (nconc g (list (length (second g)))))
                  )

          (unless (= (length grupos-identicos) 0)
            (cond
              ;;Caso o usuário queira somente perfis idênticos-------------------------------------------------
              ((= pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do menor ao maior.
                (setf grupos-identicos (sort grupos-identicos #'< :key #'third))

                ;;Aramezando os grupos em uma tabela hash que será usada para fazer relatórios.
                (setf (gethash k *agrupamento-por-perfis-filos-identicos*) grupos-identicos)

                ;;armezenando os pesos de cada grupo em uma tabela hash que será usada no relatório.
                (setf (gethash k *pesos-grupos*) (remove-duplicates grupos-identicos :key #'third))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-identicos :key #'third))

                ;;Gerando interações para cada par de proteínas de cada grupo.
                (dolist (g grupos-identicos)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third g) (third x))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        ;;Gerando PPis para todo par de proteínas do grupo g.
                        (dotimes (i (1- (third g)))
                            			  (loop for j from (1+ i) to (1- (third g)) do
                         			       (progn
                              					 (push (make-ppi-struct
                                     						:genea (first (elt (second g) i))
                                     						:geneb (first (elt (second g) j))
                                     						:weight peso-grupo
                                     						:position (second (elt (second g) i)))
                              					       ppi)
                              					 )
                                   );loop-dotimes
                                 );dotimes
                        (setf g (nconc g (list peso-grupo)))
                        );dolist-grupos-identicos
                );progn
               );condição e ação 1-----------------------------------------------------------------------------

              ;;Caso o usuário queira considerar perfis semelhantes--------------------------------------------
              ((> pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do maior ao menor.
                (setf grupos-identicos (sort grupos-identicos #'> :key #'third))

                ;;Agregando a um grupo g, todos os grupos similares ao grupo g.--------------------------------
                (dotimes (i (1- (length grupos-identicos)))
                         (setf grupos-similares  (append grupos-similares (list (list (elt grupos-identicos i) (list )))))
                         (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos i))))
                         (loop for j from (1+ i) to (1- (length grupos-identicos))
                           do (progn
                               (unless (> (comparar-perfis (first (elt grupos-identicos i)) (first (elt grupos-identicos j))) pptolerance)
                                 ;;A menos que a diferenca entre os perfis de um par de grupos, seja maior que a diferenca tolerada, faça:
                                 ;;Agregando ao grupo gi, o grupo gj, que é similar ao grupo gi.
                                 (push (elt grupos-identicos j) (second (elt grupos-similares i)))
                                 ;;Somando a quantidade total de proteínas entre os grupos similares.
                                 (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos j))))
                                 );unlles
                               );progn-do
                           );loop-dotimes
                         (setf (elt grupos-similares i) (nconc (elt grupos-similares i) (list soma-grupos)))
                         (setf soma-grupos 0)
                         );dotimes----------------------------------------------------------------------------

                ;;Agregando o último grupo que não foi considerado no dotimes acima, à lista de grupos similares.
                (setf grupos-similares  (append grupos-similares (list (list (first (last grupos-identicos)) (list ) (length (second (first (last grupos-identicos))))))))

                ;;Ordenando os grupos similares por tamanho, do menor ao maior.
                (setf grupos-similares (sort grupos-similares #'< :key #'(lambda (x) (third (first x)))))

                ;;Aramezando os grupos em uma tabela hash global que será usada para fazer relatórios.
                (setf (gethash k *agrupamento-por-perfis-filos-semelhantes*) grupos-similares)

                ;;armezenando os pesos de cada grupo em uma tabela hash que será usada no relatório.
                (setf (gethash k *pesos-grupos*) (remove-duplicates grupos-similares :key #'(lambda (x) (third (first x)))))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-similares :key #'(lambda (x) (third (first x)))))

                ;;Varrendo os grupos formados para criar arestas entre as prteínas indênticas e similares.
                (dolist (g grupos-similares)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third (first g)) (third (first x)))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        ;;Criando arestas entre as proteínas com perfis idênticos entre si.
                        (dotimes (i (1- (third (first g))))
                                 (loop for j from (1+ i) to (1- (third (first g)))
                           				    do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second (first g)) i))
                                        						:geneb (first (elt (second (first g)) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second (first g)) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop j
                                 );dotimes i

                        ;;Varrendo os grupos de proteínas com perfis semelhantes ao perfil do grupo de proteínas com perfis idênticos.
                        (dolist (grupo-similar (second g))
                                ;Calculando a difença entre os perfis.
                                (setf diferenca-perfis (comparar-perfis (first (first g)) (first grupo-similar)))
                                ;;Varrendo as proteínas de um grupo com perfil semelhante.
                                (dolist (ps (second grupo-similar))
                                        ;;Varrendo as protínas de perfis idênticos de um grupo g.
                                        (dolist (pg (second (first g)))
                                         					  ;;Crianto uma interação.
                                          					 (push (make-ppi-struct
                                                 						:genea (if (< (second pg) (second ps)) (first pg) (first ps) )
                                                 						:geneb (if (> (second pg) (second ps)) (first pg) (first ps) )
                                                 						:weight (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100)))
                                                 						:position (if (< (second pg) (second ps))  (second pg) (second ps) ) )
                                          					       ppi)
                                                );dolist pg
                                        );dolist ps
                                );dolist grupo-similar
                        );dolist grupos-similares
                );progn
               );condição e ação 2----------------------------------------------------------------------------
              );cond
            );unless

          ;;Se foi predito alguma ppi, muda-se o identificador de ppi para true.
          (if (> (length ppi) 0)
            (setf *ppi-identified-pp* t)
            )

          ;;Setando a ppi do genoma k na tabela hash de ppi.
          (setf (gethash k ppi-hash-table) ppi)
          ;(setf (gethash k *agrupamento-por-perfis-filos-identicos*) ppi)

          ;;Resetando as variáveis.
          (setf grupos-identicos nil)
          (setf grupos-similares nil)
          (setf ppi nil)

          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco))))
          (incf genomenumber)

          );progn-loop
      );loop hash table
    (format t "]~%")
    (return-from phylogenetic-profiles-complete ppi-hash-table)
    ))
;-------------------------------------------------------------------------------

(defun phylogenetic-profiles-ppiterlimit (percentage-pp pptolerance ppiterlimit)
  (let ((ppi (list));;Lista para armezenas as interações criadas.
                    (grupos-identicos (list));Lista para armazenar os agrupos de proteínas com perfis idênticos.
                    (grupos-similares (list));Lista para armazenar os agrupos de proteínas com perfis idênticos e semelhantes.
                    (pesos-grupos (list));Lista para armazenar o peso que será atribuído a cada grupo.
                    (posicao-grupo);Variável para armazenar a posição de um grupo g.
                    ;(number-proteins-max-group);Variável para armezar o número de proteínas do maior grupo.
                    ;(max-proteins-group);Variável para armazenar o grupo com maior número de proteínas.
                    (soma-grupos 0);Variável para armazenar o número total de proteínas entre grupos.
                    (peso-grupo);Variável para armazenar o peso que será atribuído às proteínas de um grupo g.
                    (dividendo);Variável que armazenar o índice de um grupo de tamanho t, que esteja mais próximo do maior grupo.
                    (divisor);Variável que armazenar o índice do maior grupo.
                    (diferenca-perfis);Variável para armazenar a difença dos perfis filogenéticos de dois grupos.
                    (total-arestas);Variável para armazenar a soma dos totais de arestas que cada grupo produzirá.
                    ;(subtracao);Variável para armazenar a quantidade total de arestas após exclusão do grupo mais populoso.
                    ;(resto);Variável para calcular a quantidade de genes que serão aproveitados do grupo que será excluído.
                    ;(indice-de-corte);Variável para armazenar o ídice que marca em qual posião o maior grupo será cortato.
                    (ppi-hash-table (make-hash-table :test #'equalp));Tabela hash para guardar a ppi de cada genoma.
                    (laco 0)
                    (genomenumber 0)
                    )

    (declare (type single-float percentage-pp))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))
    (declare (type fixnum pptolerance))
    (declare (type fixnum ppiterlimit))

    (format t "~%Predicting ppi by phylogenetic profiles;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (loop for k being the hash-keys in *perfil-filogenetico* using (hash-value perfis)
      do (progn
          ;;Agrupando as proteínas por perfil filogenetico----------------------------------------------------------------------------------------------------
          (dolist (p perfis)
                  (setf posicao-grupo (position-if #'(lambda (x) (eql nil (set-exclusive-or (second p) (first x) :test #'string=))) grupos-identicos))
                  (if (eql posicao-grupo nil)
                    (push (list (second p) (list (list (first p) (third p)))) grupos-identicos)
                    (push (list (first p) (third p)) (second (elt grupos-identicos posicao-grupo)))
                    );if
                  );dolist
          ;;Final do Agrupamento-------------------------------------------------------------------------------------------------------------------------------

          ;;Concatenando o tamanho de cada grupo g
          (dolist (g grupos-identicos)
                  ;;Concatenando a um grupo g, o tamanho do grupo g.
                  (setf g (nconc g (list (length (second g)))))
                  )

          (unless (= (length grupos-identicos) 0)
            (cond
              ;;Caso o usuário queira somente perfis idênticos-------------------------------------------------
              ((= pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do menor ao maior.
                (setf grupos-identicos (sort grupos-identicos #'< :key #'third))

                ;;Bloco para exclui grupos até que o total de arestas produzidas não seja superior que o limiar recebido---------------------------------------------
                (block remove-groups
                       (loop
                         (setf total-arestas (loop for i in grupos-identicos
                                               summing (/ (* (length (second i)) (- (length (second i)) 1)) 2) into total
                                               finally (return total)))
                         (if (<= total-arestas ppiterlimit)
                           (return-from remove-groups)
                           (setf grupos-identicos (remove (first (last grupos-identicos)) grupos-identicos :test #'equalp))
                           #|(progn
                              (setf number-proteins-max-group (third (first (last grupos-identicos))))
                              (setf subtracao (- total-arestas (/ (* number-proteins-max-group (- number-proteins-max-group 1)) 2)))
                              (if (< subtracao ppiterlimit)
                                (progn
                                 (setf resto (- ppiterlimit subtracao))
                                 (do ((x 2 (incf x)))
                                   ((> (/(* x (- x 1)) 2) resto));se (/(* x (- x 1)) 2) for maior que o resto, então para o loop.
                                   (setf indice-de-corte x)
                                   );do
                                 (setf (second (first (last grupos-identicos))) (split-up-to (second (first (last grupos-identicos))) indice-de-corte))
                                 ;(setf (third (first (last grupos-identicos))) (length (second (first (last grupos-identicos)))))
                                 );progn
                                (progn
                                 (setf grupos-identicos (remove (first (last grupos-identicos)) grupos-identicos :test #'equalp))
                                 );progn
                                );if-subtracao < limiar
                              );progn|#
                           );if-total-arestas < limiar
                         );loop
                       );block remove-groups
                ;;Fim da remoção de grupos---------------------------------------------------------------------------------------------------------------

                ;;Ordenando as proteínas de cada grupo restante antes de criar interações.
                (dolist (g grupos-identicos)
                        ;;Ordenando as proteínas de cada grupo de acordo com o seu índice no gemoma.
                        (setf (second g) (sort (second g) #'< :key #'second))
                        )

                ;;Ordenando os grupos por tamanho, do menor ao maior.
                (setf grupos-identicos (sort grupos-identicos #'< :key #'third))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-identicos :key #'third))

                ;;Gerando interações para cada par de proteínas de cada grupo.
                (dolist (g grupos-identicos)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third g) (third x))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        ;;Gerando PPis para todo par de proteínas do grupo g.
                        (dotimes (i (1- (length(second g))))
                                 (loop for j from (1+ i) to (1- (length(second g)))
                                   do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second g) i))
                                        						:geneb (first (elt (second g) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second g) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop-dotimes
                                 );dotimes
                        );dolist-grupos-identicos
                );progn
               );condição e ação 1-----------------------------------------------------------------------------

              ;;Caso o usuário queira considerar perfis semelhantes--------------------------------------------
              ((> pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do maior ao menor.
                (setf grupos-identicos (sort grupos-identicos #'> :key #'third))

                ;;Agregando a um grupo g, todos os grupos similares ao grupo g.--------------------------------
                (dotimes (i (1- (length grupos-identicos)))
                         (setf grupos-similares  (append grupos-similares (list (list (elt grupos-identicos i) (list )))))
                         (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos i))))
                         (loop for j from (1+ i) to (1- (length grupos-identicos))
                           do (progn
                               (unless (> (comparar-perfis (first (elt grupos-identicos i)) (first (elt grupos-identicos j))) pptolerance)
                                 ;;A menos que a diferenca entre os perfis de um par de grupos, seja maior que a diferenca tolerada, faça:
                                 ;;Agregando ao grupo gi, o grupo gj, que é similar ao grupo gi.
                                 (push (elt grupos-identicos j) (second (elt grupos-similares i)))
                                 ;;Somando a quantidade total de proteínas entre os grupos similares.
                                 (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos j))))
                                 );unlles
                               );progn-do
                           );loop-dotimes
                         (setf (elt grupos-similares i) (nconc (elt grupos-similares i) (list soma-grupos)))
                         (setf soma-grupos 0)
                         );dotimes----------------------------------------------------------------------------

                ;;Agregando o último grupo que não foi considerado no dotimes acima, à lista de grupos similares.
                (setf grupos-similares  (append grupos-similares (list (list (first (last grupos-identicos)) (list ) (length (second (first (last grupos-identicos))))))))

                ;;Ordenando os grupos por tamanho, do menor ao maior.
                (setf grupos-similares (sort grupos-similares #'< :key #'(lambda (x) (third (first x)))))

                (block remove-groups
                       (loop
                         (setf total-arestas (loop for g in grupos-similares
                                               summing (+
                                                        ;;Calculando quantos pares de genes com perfis idênticos serão gerados.
                                                        (/ (* (length (second (first g))) (- (length (second (first g))) 1)) 2)
                                                        ;;Multiplicando o número de genes de perfis idênticos, pelo número de genes de perfis similares.
                                                        (* (length (second (first g)))
                                                           ;;Somando a quantidade de genes de perfis similares.
                                                           (loop for gs in (second g) summing (length (second gs)) into total finally (return total))
                                                           );*
                                                        );+
                                               into total
                                               finally (return total))
                               );setf total-arestas
                         (if (<= total-arestas ppiterlimit)
                           (return-from remove-groups)
                           (setf grupos-similares (remove (first (last grupos-similares)) grupos-similares :test #'equalp))
                           #|(progn
                              ;(setf number-proteins-max-group (third (first (last grupos-similares))))
                              (setf max-proteins-group (first (last grupos-similares)))
                              (setf grupos-similares (remove (first (last grupos-similares)) grupos-similares :test #'equalp))
                              (setf subtracao (- total-arestas
                                                 (+
                                                  ;;Calculando quantos pares de genes com perfis idênticos serão gerados.
                                                  (/ (* (length (second (first max-proteins-group))) (- (length (second (first max-proteins-group))) 1)) 2)
                                                  ;;Multiplicando o número de genes de perfis idênticos, pelo número de genes de perfis similares.
                                                  (* (length (second (first max-proteins-group)))
                                                     ;;Somando a quantidade de genes de perfis similares.
                                                     (loop for gs in (second max-proteins-group) summing (length (second gs)) into total finally (return total))
                                                     );*
                                                  );+
                                                 );-
                                    );setf subtracao
                              (if (< subtracao ppiterlimit)
                                (progn

                                 (setf resto (- ppiterlimit subtracao))
                                 (do ((x 2 (incf x)))
                                   ((> (/(* x (- x 1)) 2) resto));se (/(* x (- x 1)) 2) for maior que o resto, então para o loop.
                                   (setf indice-de-corte x)
                                   );do
                                 (setf (second (first (last grupos-identicos))) (split-up-to (second (first (last grupos-identicos))) indice-de-corte))
                                 (setf (third (first (last grupos-identicos))) (length (second (first (last grupos-identicos)))))
                                 );progn
                                (progn
                                 (setf grupos-similares (remove max-proteins-group grupos-similares :test #'equalp))
                                 );progn
                                );if-subtracao < limiar
                              );progn|#
                           );if-total-arestas < limiar
                         );loop
                       );block remove-groups
                ;;Fim da remoção de grupos---------------------------------------------------------------------------------------------------------------

                ;;Ordenando as proteínas de cada grupo de acordo com o seu índice no gemoma.
                (dolist (g grupos-similares)
                        (setf (second (first g)) (sort (second (first g)) #'< :key #'second))
                        )

                ;;Ordenando os grupos similares por tamanho, do menor ao maior.
                (setf grupos-similares (sort grupos-similares #'< :key #'(lambda (x) (third (first x)))))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-similares :key #'(lambda (x) (third (first x)))))

                ;;Varrendo os grupos formados para criar arestas entre as prteínas indênticas e similares.
                (dolist (g grupos-similares)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third (first g)) (third (first x)))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        ;;Criando arestas entre as proteínas com perfis idênticos entre si.
                        (dotimes (i (1- (third (first g))))
                                 (loop for j from (1+ i) to (1- (third (first g)))
                                   do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second (first g)) i))
                                        						:geneb (first (elt (second (first g)) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second (first g)) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop j
                                 );dotimes i

                        ;;Varrendo os grupos de proteínas com perfis semelhantes ao perfil do grupo de proteínas com perfis idênticos.
                        (dolist (grupo-similar (second g))
                                ;Calculando a difença entre os perfis.
                                (setf diferenca-perfis (comparar-perfis (first (first g)) (first grupo-similar)))
                                ;;Varrendo as proteínas de um grupo com perfil semelhante.
                                (dolist (ps (second grupo-similar))
                                        ;;Varrendo as protínas de perfis idênticos de um grupo g.
                                  				  (dolist (pg (second (first g)))
                                     				    	  ;;Crianto uma interação.
                                          					 (push (make-ppi-struct
                                                 						:genea (if (< (second pg) (second ps)) (first pg) (first ps) )
                                                 						:geneb (if (> (second pg) (second ps)) (first pg) (first ps) )
                                                 						:weight (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100)))
                                                 						:position (if (< (second pg) (second ps))  (second pg) (second ps) ) )
                                          					       ppi)
                                                );dolist pg
                                        );dolist ps
                                );dolist grupo-similar
                        );dolist grupos-similares
                );progn
               );condição e ação 2----------------------------------------------------------------------------
              );cond
            );unless

          ;;Se foi predito alguma ppi, muda-se o identificador de ppi para true.
          (if (> (length ppi) 0)
            (setf *ppi-identified-pp* t)
            )

          ;;Setando a ppi do genoma k na tabela hash de ppi.
          (setf (gethash k ppi-hash-table) ppi)

          ;;Resetando as variáveis.
          (setf grupos-identicos nil)
          (setf grupos-similares nil)
          (setf ppi nil)

          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco))))
          (incf genomenumber)

          );progn-loop
      );loop hash table
    (format t "]~%")
    (return-from phylogenetic-profiles-ppiterlimit ppi-hash-table)
    ))
;-------------------------------------------------------------------------------

(defun phylogenetic-profiles-trim (percentage-pp pptolerance trim)
  (let ((ppi (list));;Lista para armezenas as interações criadas.
                    (ppi-by-score (list));Lista para armazenar PPIs por portuação.
                    (grupos-identicos (list));Lista para armazenar os agrupos de proteínas com perfis idênticos.
                    (grupos-similares (list));Lista para armazenar os agrupos de proteínas com perfis idênticos e semelhantes.
                    (pesos-grupos (list))
                    (posicao-grupo);Variável para armazenar a posição de um grupo g.
                    ;(number-proteins-max-group);Variável para armezar o número de proteínas do maior grupo.
                    (soma-grupos 0);Variável para armazenar o número total de proteínas entre grupos.
                    (peso-grupo);Variável para armazenar o peso que será atribuído às proteínas de um grupo g.
                    (dividendo);Variável que armazenar o índice de um grupo de tamanho t, que esteja mais próximo do maior grupo.
                    (divisor);Variável que armazenar o índice do maior grupo.
                    (diferenca-perfis);Variável para armazenar a difença dos perfis filogenéticos de dois grupos.
                    (ppi-hash-table (make-hash-table :test #'equalp));Tabela hash para guardar a ppi de cada genoma.
                    (laco 0)
                    (genomenumber 0)
                    )

    (declare (type single-float percentage-pp))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))
    (declare (type fixnum pptolerance))
    (declare (type fixnum trim))

    (format t "~%Predicting ppi by phylogenetic profiles;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (loop for k being the hash-keys in *perfil-filogenetico* using (hash-value perfis)
      do (progn
          ;;Agrupando as proteínas por perfil filogenetico----------------------------------------------------------------------------------------------------
          (dolist (p perfis)
                  (setf posicao-grupo (position-if #'(lambda (x) (eql nil (set-exclusive-or (second p) (first x) :test #'string=))) grupos-identicos))
                  (if (eql posicao-grupo nil)
                    (push (list (second p) (list (list (first p) (third p)))) grupos-identicos)
                    (push (list (first p) (third p)) (second (elt grupos-identicos posicao-grupo)))
                    );if
                  );dolist
          ;;Final do Agrupamento-------------------------------------------------------------------------------------------------------------------------------

          ;;Ordenando as proteínas de cada grupo e Concatenando o tamanho do grupo
          (dolist (g grupos-identicos)
                  ;;Ordenando as proteínas de cada grupo de acordo com o seu índice no gemoma.
                  (setf (second g) (sort (second g) #'< :key #'second))
                  ;;Concatenando a um grupo g, o tamanho do grupo g.
                  (setf g (nconc g (list (length (second g)))))
                  )

          (unless (= (length grupos-identicos) 0)
            (cond
              ;;Caso o usuário queira somente perfis idênticos-------------------------------------------------
              ((= pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do menor ao maior.
                (setf grupos-identicos (sort grupos-identicos #'< :key #'third))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-identicos :key #'third))

                ;;Gerando interações para cada par de proteínas de cada grupo.
                (dolist (g grupos-identicos)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third g) (third x))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        (unless (= (third g) 1)
                          (push (list peso-grupo (list)) ppi-by-score)
                          )

                        ;;Gerando PPis para todo par de proteínas do grupo g.
                        (dotimes (i (1- (third g)))
                                 (loop for j from (1+ i) to (1- (third g))
                                   do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second g) i))
                                        						:geneb (first (elt (second g) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second g) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop-dotimes
                                 );dotimes
                        );dolist-grupos-identicos

                (setf ppi-by-score (remove-duplicates ppi-by-score :test #'= :key #'first))

                (dolist (p ppi)
                        (setf posicao-grupo (position-if #'(lambda (x) (= (ppi-struct-weight p) (first x))) ppi-by-score))
                        (push p (second (elt ppi-by-score posicao-grupo)))
                        );dolist

                (setf ppi nil)

                (dolist (p ppi-by-score)
                        (setf (second p) (sort (second p) #'< :key #'(lambda (x) (ppi-struct-position x))))
                        (if (<= (length (second p)) trim)
                          (setf ppi (append ppi (second p)))
                          (setf ppi (append ppi (split-up-to (second p) trim)))
                          );if
                        );dolist

                (setf ppi-by-score nil)

                );progn
               );condição e ação 1-----------------------------------------------------------------------------

              ;;Caso o usuário queira considerar perfis semelhantes--------------------------------------------
              ((> pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do maior ao menor.
                (setf grupos-identicos (sort grupos-identicos #'> :key #'third))

                ;;Agregando a um grupo g, todos os grupos similares ao grupo g.--------------------------------
                (dotimes (i (1- (length grupos-identicos)))
                         (setf grupos-similares  (append grupos-similares (list (list (elt grupos-identicos i) (list )))))
                         (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos i))))
                         (loop for j from (1+ i) to (1- (length grupos-identicos))
                           do (progn
                               (unless (> (comparar-perfis (first (elt grupos-identicos i)) (first (elt grupos-identicos j))) pptolerance)
                                 ;;A menos que a diferenca entre os perfis de um par de grupos, seja maior que a diferenca tolerada, faça:
                                 ;;Agregando ao grupo gi, o grupo gj, que é similar ao grupo gi.
                                 (push (elt grupos-identicos j) (second (elt grupos-similares i)))
                                 ;;Somando a quantidade total de proteínas entre os grupos similares.
                                 (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos j))))
                                 );unlles
                               );progn-do
                           );loop-dotimes
                         (setf (elt grupos-similares i) (nconc (elt grupos-similares i) (list soma-grupos)))
                         (setf soma-grupos 0)
                         );dotimes----------------------------------------------------------------------------

                ;;Agregando o último grupo que não foi considerado no dotimes acima, à lista de grupos similares.
                (setf grupos-similares  (append grupos-similares (list (list (first (last grupos-identicos)) (list ) (length (second (first (last grupos-identicos))))))))

                ;;Ordenando os grupos similares por tamanho, do menor ao maior.
                (setf grupos-similares (sort grupos-similares #'< :key #'(lambda (x) (third (first x)))))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-similares :key #'(lambda (x) (third (first x)))))

                ;;Varrendo os grupos formados para criar arestas entre as prteínas indênticas e similares.
                (dolist (g grupos-similares)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third (first g)) (third (first x)))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        (unless (= (third g) 1)
                          (push (list peso-grupo (list)) ppi-by-score)
                          )

                        ;;Criando arestas entre as proteínas com perfis idênticos entre si.
                        (dotimes (i (1- (third (first g))))
                                 (loop for j from (1+ i) to (1- (third (first g)))
                           				    do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second (first g)) i))
                                        						:geneb (first (elt (second (first g)) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second (first g)) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop j
                                 );dotimes i

                        ;;Varrendo os grupos de proteínas com perfis semelhantes ao perfil do grupo de proteínas com perfis idênticos.
                        (dolist (grupo-similar (second g))
                                ;;Calculando a difença entre os perfis.
                                (setf diferenca-perfis (comparar-perfis (first (first g)) (first grupo-similar)))
                                ;;inserindo o novo peso gerado à lista de PPis por peso.
                                (push (list (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100))) (list)) ppi-by-score)
                                ;;Varrendo as proteínas de um grupo com perfil semelhante.
                                (dolist (ps (second grupo-similar))
                                        ;;Varrendo as protínas de perfis idênticos de um grupo g.
                                  				  (dolist (pg (second (first g)))
                                        				    ;;Crianto uma interação.
                                        				    (push (make-ppi-struct
                                                 						:genea (if (< (second pg) (second ps)) (first pg) (first ps) )
                                                 						:geneb (if (> (second pg) (second ps)) (first pg) (first ps) )
                                                 						:weight (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100)))
                                                 						:position (if (< (second pg) (second ps))  (second pg) (second ps) ) )
                                               					  ppi)
                                       				     );dolist pg
                                        );dolist ps
                                );dolist grupo-similar
                        );dolist grupos-similares

                (setf ppi-by-score (remove-duplicates ppi-by-score :test #'= :key #'first))

                (dolist (p ppi)
                        (setf posicao-grupo (position-if #'(lambda (x) (= (ppi-struct-weight p) (first x))) ppi-by-score))
                        (push p (second (elt ppi-by-score posicao-grupo)))
                        );dolist

                (setf ppi nil)

                (dolist (p ppi-by-score)
                        (setf (second p) (sort (second p) #'< :key #'(lambda (x) (ppi-struct-position x))))
                        (if (<= (length (second p)) trim)
                          (setf ppi (append ppi (second p)))
                          (setf ppi (append ppi (split-up-to (second p) trim)))
                          );if
                        );dolist

                (setf ppi-by-score nil)

                );progn
               );condição e ação 2----------------------------------------------------------------------------
              );cond
            );unless

          ;;Se foi predito alguma ppi, muda-se o identificador de ppi para true.
          (if (> (length ppi) 0)
            (setf *ppi-identified-pp* t)
            )

          ;;Setando a ppi do genoma k na tabela hash de ppi.
          (setf (gethash k ppi-hash-table) ppi)
          ;(setf (gethash k *agrupamento-por-perfis-filos-identicos*) ppi)

          ;;Resetando as variáveis.
          (setf grupos-identicos nil)
          (setf grupos-similares nil)
          (setf ppi nil)

          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco))))
          (incf genomenumber)

          );progn-loop
      );loop hash table
    (format t "]~%")
    (return-from phylogenetic-profiles-trim ppi-hash-table)
    ))
;-------------------------------------------------------------------------------

(defun phylogenetic-profiles-threshold (percentage-pp pptolerance ppthreshold plusminus)
  (let ((ppi (list));;Lista para armezenas as interações criadas.
                    (grupos-identicos (list));Lista para armazenar os agrupos de proteínas com perfis idênticos.
                    (grupos-similares (list));Lista para armazenar os agrupos de proteínas com perfis idênticos e semelhantes.
                    (pesos-grupos (list))
                    (posicao-grupo);Variável para armazenar a posição de um grupo g.
                    ;(number-proteins-max-group);Variável para armezar o número de proteínas do maior grupo.
                    (soma-grupos 0);Variável para armazenar o número total de proteínas entre grupos.
                    (peso-grupo);Variável para armazenar o peso que será atribuído às proteínas de um grupo g.
                    (dividendo);Variável que armazenar o índice de um grupo de tamanho t, que esteja mais próximo do maior grupo.
                    (divisor);Variável que armazenar o índice do maior grupo.
                    (diferenca-perfis);Variável para armazenar a difença dos perfis filogenéticos de dois grupos.
                    (ppi-hash-table (make-hash-table :test #'equalp));Tabela hash para guardar a ppi de cada genoma.
                    (laco 0)
                    (genomenumber 0)
                    )

    (declare (type single-float percentage-pp))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))
    (declare (type fixnum pptolerance))
    (declare (type fixnum ppthreshold))
    (declare (type (simple-array character (1)) plusminus))

    (format t "~%Predicting ppi by phylogenetic profiles;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (loop for k being the hash-keys in *perfil-filogenetico* using (hash-value perfis)
      do (progn
          ;;Agrupando as proteínas por perfil filogenetico----------------------------------------------------------------------------------------------------
          (dolist (p perfis)
                  (setf posicao-grupo (position-if #'(lambda (x) (eql nil (set-exclusive-or (second p) (first x) :test #'string=))) grupos-identicos))
                  (if (eql posicao-grupo nil)
                    (progn (push (list (second p) (list (list (first p) (third p)))) grupos-identicos) #|(format t "entrou 1~%")|#)
                    (progn (push (list (first p) (third p)) (second (elt grupos-identicos posicao-grupo))) #|(format t "entrou 2~%")|#)
                    );if
                  );dolist
          ;;Final do Agrupamento-------------------------------------------------------------------------------------------------------------------------------

          ;;Descartando grupos menos que o threshold informado pelo usuário.
          (if (string= plusminus "<")
            (dolist (g grupos-identicos)
                    (unless (< (length (first g)) ppthreshold)
                      (setf grupos-identicos (remove g grupos-identicos :test #'equalp))
                      );unless
                    );dolist
            (dolist (g grupos-identicos)
                    (unless (> (length (first g)) ppthreshold)
                      (setf grupos-identicos (remove g grupos-identicos :test #'equalp))
                      );unless
                    );dolist
            );if string=

          ;;Ordenando as proteínas de cada grupo
          (dolist (g grupos-identicos)
                  ;;Ordenando as proteínas de cada grupo de acordo com o seu índice no gemoma.
                  (setf (second g) (sort (second g) #'< :key #'second))
                  ;;Concatenando a um grupo g, o tamanho do grupo g.
                  (setf g (nconc g (list (length (second g)))))
                  );dolist

          (unless (= (length grupos-identicos) 0)
            (cond
              ;;Caso o usuário queira somente perfis idênticos-------------------------------------------------
              ((= pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do menor ao maior.
                (setf grupos-identicos (sort grupos-identicos #'< :key #'third))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-identicos :key #'third))

                ;;Gerando interações para cada par de proteínas de cada grupo.
                (dolist (g grupos-identicos)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third g) (third x))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        ;;Gerando PPis para todo par de proteínas do grupo g.
                        (dotimes (i (1- (third g)))
                                 (loop for j from (1+ i) to (1- (third g))
                                   do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second g) i))
                                        						:geneb (first (elt (second g) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second g) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop-dotimes
                                 );dotimes
                        );dolist-grupos-identicos
                );progn
               );condição e ação 1-----------------------------------------------------------------------------

              ;;Caso o usuário queira considerar perfis semelhantes--------------------------------------------
              ((> pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do maior ao menor.
                (setf grupos-identicos (sort grupos-identicos #'> :key #'third))

                ;;Agregando a um grupo g, todos os grupos similares ao grupo g.--------------------------------
                (dotimes (i (1- (length grupos-identicos)))
                         (setf grupos-similares  (append grupos-similares (list (list (elt grupos-identicos i) (list )))))
                         (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos i))))
                         (loop for j from (1+ i) to (1- (length grupos-identicos))
                           do (progn
                               (unless (> (comparar-perfis (first (elt grupos-identicos i)) (first (elt grupos-identicos j))) pptolerance)
                                 ;;A menos que a diferenca entre os perfis de um par de grupos, seja maior que a diferenca tolerada, faça:
                                 ;;Agregando ao grupo gi, o grupo gj, que é similar ao grupo gi.
                                 (push (elt grupos-identicos j) (second (elt grupos-similares i)))
                                 ;;Somando a quantidade total de proteínas entre os grupos similares.
                                 (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos j))))
                                 );unlles
                               );progn-do
                           );loop-dotimes
                         (setf (elt grupos-similares i) (nconc (elt grupos-similares i) (list soma-grupos)))
                         (setf soma-grupos 0)
                         );dotimes----------------------------------------------------------------------------

                ;;Agregando o último grupo que não foi considerado no dotimes acima, à lista de grupos similares.
                (setf grupos-similares  (append grupos-similares (list (list (first (last grupos-identicos)) (list ) (length (second (first (last grupos-identicos))))))))

                ;;Ordenando os grupos similares por tamanho, do menor ao maior.
                (setf grupos-similares (sort grupos-similares #'< :key #'(lambda (x) (third (first x)))))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-similares :key #'(lambda (x) (third (first x)))))

                ;;Varrendo os grupos formados para criar arestas entre as prteínas indênticas e similares.
                (dolist (g grupos-similares)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third (first g)) (third (first x)))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        ;;Criando arestas entre as proteínas com perfis idênticos entre si.
                        (dotimes (i (1- (third (first g))))
                                 (loop for j from (1+ i) to (1- (third (first g)))
                                   do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second (first g)) i))
                                        						:geneb (first (elt (second (first g)) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second (first g)) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop j
                                 );dotimes i

                        ;;Varrendo os grupos de proteínas com perfis semelhantes ao perfil do grupo de proteínas com perfis idênticos.
                        (dolist (grupo-similar (second g))
                                ;Calculando a difença entre os perfis.
                                (setf diferenca-perfis (comparar-perfis (first (first g)) (first grupo-similar)))
                                ;;Varrendo as proteínas de um grupo com perfil semelhante.
                                (dolist (ps (second grupo-similar))
                                        ;;Varrendo as protínas de perfis idênticos de um grupo g.
                                        (dolist (pg (second (first g)))
                                           					;;Crianto uma interação.
                                           					(push (make-ppi-struct
                                                 						:genea (if (< (second pg) (second ps)) (first pg) (first ps) )
                                                 						:geneb (if (> (second pg) (second ps)) (first pg) (first ps) )
                                                 						:weight (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100)))
                                                 						:position (if (< (second pg) (second ps))  (second pg) (second ps) ) )
                                          					       ppi)
                                                );dolist pg
                                        );dolist ps
                                );dolist grupo-similar
                        );dolist grupos-similares
                );progn
               );condição e ação 2----------------------------------------------------------------------------
              );cond
            );unless

          ;;Se foi predito alguma ppi, muda-se o identificador de ppi para true.
          (if (> (length ppi) 0)
            (setf *ppi-identified-pp* t)
            )

          ;;Setando a ppi do genoma k na tabela hash de ppi.
          (setf (gethash k ppi-hash-table) ppi)

          ;;Resetando as variáveis.
          (setf grupos-identicos nil)
          (setf grupos-similares nil)
          (setf ppi nil)

          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco))))
          (incf genomenumber)

          );progn-loop
      );loop hash table
    (format t "]~%")
    (return-from phylogenetic-profiles-threshold ppi-hash-table)
    ))
;-------------------------------------------------------------------------------

(defun phylogenetic-profiles-delete-clusters (percentage-pp pptolerance grouplimit)
  (let ((ppi (list));;Lista para armezenas as interações criadas.
                    (ppi-by-score (list));Lista para armazenar PPIs por portuação.
                    (grupos-identicos (list));Lista para armazenar os agrupos de proteínas com perfis idênticos.
                    (grupos-similares (list));Lista para armazenar os agrupos de proteínas com perfis idênticos e semelhantes.
                    (pesos-grupos (list))
                    (posicao-grupo);Variável para armazenar a posição de um grupo g.
                    ;(number-proteins-max-group);Variável para armezar o número de proteínas do maior grupo.
                    (soma-grupos 0);Variável para armazenar o número total de proteínas entre grupos.
                    (peso-grupo);Variável para armazenar o peso que será atribuído às proteínas de um grupo g.
                    (dividendo);Variável que armazenar o índice de um grupo de tamanho t, que esteja mais próximo do maior grupo.
                    (divisor);Variável que armazenar o índice do maior grupo.
                    (diferenca-perfis);Variável para armazenar a difença dos perfis filogenéticos de dois grupos.
                    (ppi-hash-table (make-hash-table :test #'equalp));Tabela hash para guardar a ppi de cada genoma.
                    (laco 0)
                    (genomenumber 0)
                    )

    (declare (type single-float percentage-pp))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))
    (declare (type fixnum pptolerance))
    (declare (type fixnum grouplimit))

    (format t "~%Predicting ppi by phylogenetic profiles;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (loop for k being the hash-keys in *perfil-filogenetico* using (hash-value perfis)
      do (progn
          ;;Agrupando as proteínas por perfil filogenetico----------------------------------------------------------------------------------------------------
          (dolist (p perfis)
                  (setf posicao-grupo (position-if #'(lambda (x) (eql nil (set-exclusive-or (second p) (first x) :test #'string=))) grupos-identicos))
                  (if (eql posicao-grupo nil)
                    (push (list (second p) (list (list (first p) (third p)))) grupos-identicos)
                    (push (list (first p) (third p)) (second (elt grupos-identicos posicao-grupo)))
                    );if
                  );dolist
          ;;Final do Agrupamento-------------------------------------------------------------------------------------------------------------------------------

          ;;Removendo grupos que produziriam um número indesejado de arestas com mesma pontuação.
          (dolist (g grupos-identicos)
                  (unless (<= (/ (* (length (second g)) (- (length (second g)) 1)) 2) grouplimit)
                    (setf grupos-identicos (remove g grupos-identicos :test #'equalp))
                    );unless
                  );dolist

          ;;Ordenando as proteínas de cada grupo
          (dolist (g grupos-identicos)
                  ;;Ordenando as proteínas de cada grupo de acordo com o seu índice no gemoma.
                  (setf (second g) (sort (second g) #'< :key #'second))
                  ;;Concatenando a um grupo g, o tamanho do grupo g.
                  (setf g (nconc g (list (length (second g)))))
                  )

          (unless (= (length grupos-identicos) 0)
            (cond
              ;;Caso o usuário queira somente perfis idênticos-------------------------------------------------
              ((= pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do menor ao maior.
                (setf grupos-identicos (sort grupos-identicos #'< :key #'third))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-identicos :key #'third))

                ;;Gerando interações para cada par de proteínas de cada grupo.
                (dolist (g grupos-identicos)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third g) (third x))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        ;;Gerando PPis para todo par de proteínas do grupo g.
                        (dotimes (i (1- (third g)))
                                 (loop for j from (1+ i) to (1- (third g))
                                   do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second g) i))
                                        						:geneb (first (elt (second g) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second g) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop-dotimes
                                 );dotimes
                        );dolist-grupos-identicos
                );progn
               );condição e ação 1-----------------------------------------------------------------------------

              ;;Caso o usuário queira considerar perfis semelhantes--------------------------------------------
              ((> pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do maior ao menor.
                (setf grupos-identicos (sort grupos-identicos #'> :key #'third))

                ;;Agregando a um grupo g, todos os grupos similares ao grupo g.--------------------------------
                (dotimes (i (1- (length grupos-identicos)))
                         (setf grupos-similares  (append grupos-similares (list (list (elt grupos-identicos i) (list )))))
                         (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos i))))
                         (loop for j from (1+ i) to (1- (length grupos-identicos))
                           do (progn
                               (unless (> (comparar-perfis (first (elt grupos-identicos i)) (first (elt grupos-identicos j))) pptolerance)
                                 ;;A menos que a diferenca entre os perfis de um par de grupos, seja maior que a diferenca tolerada, faça:
                                 ;;Agregando ao grupo gi, o grupo gj, que é similar ao grupo gi.
                                 (push (elt grupos-identicos j) (second (elt grupos-similares i)))
                                 ;;Somando a quantidade total de proteínas entre os grupos similares.
                                 (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos j))))
                                 );unlles
                               );progn-do
                           );loop-dotimes
                         (setf (elt grupos-similares i) (nconc (elt grupos-similares i) (list soma-grupos)))
                         (setf soma-grupos 0)
                         );dotimes----------------------------------------------------------------------------

                ;;Agregando o último grupo que não foi considerado no dotimes acima, à lista de grupos similares.
                (setf grupos-similares  (append grupos-similares (list (list (first (last grupos-identicos)) (list ) (length (second (first (last grupos-identicos))))))))

                ;;Ordenando os grupos similares por tamanho, do menor ao maior.
                (setf grupos-similares (sort grupos-similares #'< :key #'(lambda (x) (third (first x)))))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-similares :key #'(lambda (x) (third (first x)))))

                ;;Varrendo os grupos formados para criar arestas entre as prteínas indênticas e similares.
                (dolist (g grupos-similares)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third (first g)) (third (first x)))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        (unless (= (third g) 1)
                          (push (list peso-grupo (list)) ppi-by-score)
                          )

                        ;;Criando arestas entre as proteínas com perfis idênticos entre si.
                        (dotimes (i (1- (third (first g))))
                                 (loop for j from (1+ i) to (1- (third (first g)))
                                   do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second (first g)) i))
                                        						:geneb (first (elt (second (first g)) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second (first g)) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop j
                                 );dotimes i

                        ;;Varrendo os grupos de proteínas com perfis semelhantes ao perfil do grupo de proteínas com perfis idênticos.
                        (dolist (grupo-similar (second g))
                                ;Calculando a difença entre os perfis.
                                (setf diferenca-perfis (comparar-perfis (first (first g)) (first grupo-similar)))
                                ;;inserindo o novo peso gerado à lista de PPis por peso.
                                (push (list (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100))) (list)) ppi-by-score)
                                ;;Varrendo as proteínas de um grupo com perfil semelhante.
                                (dolist (ps (second grupo-similar))
                                        ;;Varrendo as protínas de perfis idênticos de um grupo g.
                                        (dolist (pg (second (first g)))
                                                ;;Crianto uma interação.
                                          					 (push (make-ppi-struct
                                                 						:genea (if (< (second pg) (second ps)) (first pg) (first ps) )
                                                 						:geneb (if (> (second pg) (second ps)) (first pg) (first ps) )
                                                 						:weight (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100)))
                                                 						:position (if (< (second pg) (second ps))  (second pg) (second ps) ) )
                                          					       ppi)
                                                );dolist pg
                                        );dolist ps
                                );dolist grupo-similar

                        );dolist grupos-similares

                (setf ppi-by-score (remove-duplicates ppi-by-score :test #'= :key #'first))

                (dolist (p ppi)
                        (setf posicao-grupo (position-if #'(lambda (x) (= (ppi-struct-weight p) (first x))) ppi-by-score))
                        (push p (second(elt ppi-by-score posicao-grupo)))
                        );dolist

                (setf ppi nil)

                (dolist (p ppi-by-score)
                        (setf (second p) (sort (second p) #'< :key #'(lambda (x) (ppi-struct-position x))))
                        (if (<= (length (second p)) grouplimit)
                          (setf ppi (append ppi (second p)))
                          (setf ppi (append ppi (split-up-to (second p) grouplimit)))
                          );if
                        );dolist

                (setf ppi-by-score nil)

                );progn
               );condição e ação 2----------------------------------------------------------------------------
              );cond
            );unless

          ;;Se foi predito alguma ppi, muda-se o identificador de ppi para true.
          (if (> (length ppi) 0)
            (setf *ppi-identified-pp* t)
            )

          ;;Setando a ppi do genoma k na tabela hash de ppi.
          (setf (gethash k ppi-hash-table) ppi)
          ;(setf (gethash k *agrupamento-por-perfis-filos-identicos*) ppi)

          ;;Resetando as variáveis.
          (setf grupos-identicos nil)
          (setf grupos-similares nil)
          (setf ppi nil)

          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco))))
          (incf genomenumber)
          );progn-loop
      );loop hash table
    (format t "]~%")
    (return-from phylogenetic-profiles-delete-clusters ppi-hash-table)
    ))

(defun phylogenetic-profiles-delete-perfil (percentage-pp pptolerance string-perfis)
  (let ((ppi (list));;Lista para armezenas as interações criadas.
                    (grupos-identicos (list));Lista para armazenar os agrupos de proteínas com perfis idênticos.
                    (grupos-similares (list));Lista para armazenar os agrupos de proteínas com perfis idênticos e semelhantes.
                    (pesos-grupos (list))
                    (perfis-indesejados (list))
                    (perfil)
                    (posicao-grupo);Variável para armazenar a posição de um grupo g.
                    ;(number-proteins-max-group);Variável para armezar o número de proteínas do maior grupo.
                    (soma-grupos 0);Variável para armazenar o número total de proteínas entre grupos.
                    (peso-grupo);Variável para armazenar o peso que será atribuído às proteínas de um grupo g.
                    (dividendo);Variável que armazenar o índice de um grupo de tamanho t, que esteja mais próximo do maior grupo.
                    (divisor);Variável que armazenar o índice do maior grupo.
                    (diferenca-perfis);Variável para armazenar a difença dos perfis filogenéticos de dois grupos.
                    (ppi-hash-table (make-hash-table :test #'equalp));Tabela hash para guardar a ppi de cada genoma.
                    (laco 0)
                    (genomenumber 0)
                    )

    (declare (type single-float percentage-pp))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))
    (declare (type fixnum pptolerance))

    (format t "~%Predicting ppi by phylogenetic profiles;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    (setf string-perfis (concatenate 'string string-perfis ";"))

    ;;extraindo os perfis indesejados da string-perfis
    (loop for c across string-perfis
      do (progn
          (if (char= c #\;)
            (progn
             (unless (eql perfil nil)
               (setf perfis-indesejados (append perfis-indesejados (list (parse-integer perfil))))
               (setf perfil nil)
               )
             )
            (setf perfil (concatenate 'string perfil (list c)))
            );if char=
          );progn
      );loop

    (loop for k being the hash-keys in *perfil-filogenetico* using (hash-value perfis)
      do (progn
          ;;Agrupando as proteínas por perfil filogenetico----------------------------------------------------------------------------------------------------
          (dolist (p perfis)
                  (setf posicao-grupo (position-if #'(lambda (x) (eql nil (set-exclusive-or (second p) (first x) :test #'string=))) grupos-identicos))
                  (if (eql posicao-grupo nil)
                    (push (list (second p) (list (list (first p) (third p)))) grupos-identicos)
                    (push (list (first p) (third p)) (second (elt grupos-identicos posicao-grupo)))
                    );if
                  );dolist
          ;;Final do Agrupamento-------------------------------------------------------------------------------------------------------------------------------

          ;;Descartando perfis indesejados.
          (dolist (g grupos-identicos)
                  (dolist (p perfis-indesejados)
                          (if (= p (length (first g)))
                            (setf grupos-identicos (remove g grupos-identicos :test #'equalp))
                            );if
                          );dolist perfis-indesejados
                  );dolist grupos-indenticos

          ;;Ordenando as proteínas de cada grupo
          (dolist (g grupos-identicos)
                  ;;Ordenando as proteínas de cada grupo de acordo com o seu índice no gemoma.
                  (setf (second g) (sort (second g) #'< :key #'second))
                  ;;Concatenando a um grupo g, o tamanho do grupo g.
                  (setf g (nconc g (list (length (second g)))))
                  );dolist

          (unless (= (length grupos-identicos) 0)
            (cond
              ;;Caso o usuário queira somente perfis idênticos-------------------------------------------------
              ((= pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do menor ao maior.
                (setf grupos-identicos (sort grupos-identicos #'< :key #'third))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-identicos :key #'third))

                ;;Gerando interações para cada par de proteínas de cada grupo.
                (dolist (g grupos-identicos)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third g) (third x))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        ;;Gerando PPis para todo par de proteínas do grupo g.
                        (dotimes (i (1- (third g)))
                                 (loop for j from (1+ i) to (1- (third g))
                                   do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second g) i))
                                        						:geneb (first (elt (second g) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second g) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop-dotimes
                                 );dotimes
                        );dolist-grupos-identicos
                );progn
               );condição e ação 1-----------------------------------------------------------------------------

              ;;Caso o usuário queira considerar perfis semelhantes--------------------------------------------
              ((> pptolerance 0)
               (progn

                ;;Ordenando os grupos por tamanho, do maior ao menor.
                (setf grupos-identicos (sort grupos-identicos #'> :key #'third))

                ;;Agregando a um grupo g, todos os grupos similares ao grupo g.--------------------------------
                (dotimes (i (1- (length grupos-identicos)))
                         (setf grupos-similares  (append grupos-similares (list (list (elt grupos-identicos i) (list )))))
                         (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos i))))
                         (loop for j from (1+ i) to (1- (length grupos-identicos))
                           do (progn
                               (unless (> (comparar-perfis (first (elt grupos-identicos i)) (first (elt grupos-identicos j))) pptolerance)
                                 ;;A menos que a diferenca entre os perfis de um par de grupos, seja maior que a diferenca tolerada, faça:
                                 ;;Agregando ao grupo gi, o grupo gj, que é similar ao grupo gi.
                                 (push (elt grupos-identicos j) (second (elt grupos-similares i)))
                                 ;;Somando a quantidade total de proteínas entre os grupos similares.
                                 (setf soma-grupos (+ soma-grupos (third (elt grupos-identicos j))))
                                 );unlles
                               );progn-do
                           );loop-dotimes
                         (setf (elt grupos-similares i) (nconc (elt grupos-similares i) (list soma-grupos)))
                         (setf soma-grupos 0)
                         );dotimes----------------------------------------------------------------------------

                ;;Agregando o último grupo que não foi considerado no dotimes acima, à lista de grupos similares.
                (setf grupos-similares  (append grupos-similares (list (list (first (last grupos-identicos)) (list ) (length (second (first (last grupos-identicos))))))))

                ;;Ordenando os grupos similares por tamanho, do menor ao maior.
                (setf grupos-similares (sort grupos-similares #'< :key #'(lambda (x) (third (first x)))))

                ;;Removendo grupos com a mesma quantidade de genes e atribuindo o resultado à lista pesos-grupos.
                (setf pesos-grupos (remove-duplicates grupos-similares :key #'(lambda (x) (third (first x)))))

                ;;Varrendo os grupos formados para criar arestas entre as prteínas indênticas e similares.
                (dolist (g grupos-similares)

                        ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                        (setf dividendo (1+ (position-if #'(lambda (x) (= (third (first g)) (third (first x)))) pesos-grupos)))

                        ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                        (setf divisor (length pesos-grupos))

                        ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                        (setf peso-grupo (* (/ dividendo divisor) percentage-pp))

                        ;;Criando arestas entre as proteínas com perfis idênticos entre si.
                        (dotimes (i (1- (third (first g))))
                                 (loop for j from (1+ i) to (1- (third (first g)))
                                   do (progn
                                 					 (push (make-ppi-struct
                                        						:genea (first (elt (second (first g)) i))
                                        						:geneb (first (elt (second (first g)) j))
                                        						:weight peso-grupo
                                        						:position (second (elt (second (first g)) i)))
                                 					       ppi)
                                       );progn-do
                                   );loop j
                                 );dotimes i

                        ;;Varrendo os grupos de proteínas com perfis semelhantes ao perfil do grupo de proteínas com perfis idênticos.
                        (dolist (grupo-similar (second g))
                                ;Calculando a difença entre os perfis.
                                (setf diferenca-perfis (comparar-perfis (first (first g)) (first grupo-similar)))
                                ;;Varrendo as proteínas de um grupo com perfil semelhante.
                                (dolist (ps (second grupo-similar))
                                        ;;Varrendo as protínas de perfis idênticos de um grupo g.
                                        (dolist (pg (second (first g)))
                                                ;;Crianto uma interação.
                                          					 (push (make-ppi-struct
                                                 						:genea (if (< (second pg) (second ps)) (first pg) (first ps) )
                                                 						:geneb (if (> (second pg) (second ps)) (first pg) (first ps) )
                                                 						:weight (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100)))
                                                 						:position (if (< (second pg) (second ps))  (second pg) (second ps) ) )
                                          					       ppi)
                                                );dolist pg
                                        );dolist ps
                                );dolist grupo-similar
                        );dolist grupos-similares
                );progn
               );condição e ação 2----------------------------------------------------------------------------
              );cond
            );unless

          ;;Se foi predito alguma ppi, muda-se o identificador de ppi para true.
          (if (> (length ppi) 0)
            (setf *ppi-identified-pp* t)
            )

          ;;Setando a ppi do genoma k na tabela hash de ppi.
          (setf (gethash k ppi-hash-table) ppi)
          ;(setf (gethash k *agrupamento-por-perfis-filos-identicos*) ppi)

          ;;Resetando as variáveis.
          (setf grupos-identicos nil)
          (setf grupos-similares nil)
          (setf ppi nil)

          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *perfil-filogenetico*)) 100)) laco))))
          (incf genomenumber)

          );progn-loop
      );loop hash table
    (format t "]~%")
    (return-from phylogenetic-profiles-delete-perfil ppi-hash-table)
    ))
;;------------------------------------------------------------------------------

;;Bloco de funções para predizer ppis por vizinhaça gênica conservada-----------
(declaim (ftype (function (single-float fixnum fixnum fixnum fixnum fixnum fixnum fixnum fixnum fixnum fixnum) ) conserved-neighbourhood-fixed))
(defun conserved-neighbourhood-fixed(percentage-cn w1 cw1 w2 cw2 w3 cw3 w4 cw4 aadifflimit checkpointminlimit)
  (defparameter *relatorio-vizinhanca-genica* (make-hash-table :test #'equalp)
    "Tabela hash para armazenar o registro do número de genes conservados
     e não conservados para cada expanção de um gene pivô do pan-genoma")
  (defstruct expansao localidade conservacoes)
  (let((ppi (make-hash-table :test #'equalp));Tabela hash para armazenar as PPIs constatadas
                                             (genomas (list));Lista para guardar genomas nos quais foram contatada a conservação gênica
                                             (pivo-um);Variavel para armazenar cada proteina pivo do pan-genoma
                                             (pesos);Variável para armazenar as forças das interações entre as proteínas
                                             (qtd-ppi);Variável para contabilizar o número de vizinhança conservada identificada
                                             (conservacao);Variável booleana para constatar uma conservação gênica.
                                             (interaction)
                                             (maior-peso)
                                             (laco 0)
                                             (genomenumber 0)
                                             )

    (declare (type hash-table *relatorio-vizinhanca-genica*))
    (declare (type single-float percentage-cn))
    (declare (type fixnum w1))
    (declare (type fixnum cw1))
    (declare (type fixnum w2))
    (declare (type fixnum cw2))
    (declare (type fixnum w3))
    (declare (type fixnum cw3))
    (declare (type fixnum w4))
    (declare (type fixnum cw4))
    (declare (type fixnum aadifflimit))
    (declare (type fixnum checkpointminlimit))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))

    (format t "~%Predicting ppi by conserved gene neighborhood;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    ;;O loop a seguir varre a tabela hash *pan-genoma* e para cada proteína verifica-se
    ;;se há similaridade proteica entre a sua vizinhança direita e a vizinhança direita
    ;;das proteinas similares a ela.
    (loop for k being the hash-keys in *pan-genoma* using (hash-value v)
      do (progn
          (setf pivo-um (proteina-localidade v))
          (setf pesos (loop for i from 1 to w1 collecting (list 1 1)))
          (setf genomas (list))
          (setf qtd-ppi 0)

          ;;Varre a lista de proteinas similares (pivo-2) a cada proteina do pan-genoma (pivo-um)
          (dolist (pivo-dois (proteina-similares v))

                  ;;Loop de expanção
                  (loop for i from 1 to w1
                    do (progn
                        (setf conservacao nil)
                        (cond
                          ;condicao e acao 1----------------------------------------------------------------------------------------
                          ((and (< (+ (second pivo-um) i) (length (gethash (first pivo-um) *genomas*)))
                                (< (+ (second pivo-dois) i) (length (gethash (first pivo-dois) *genomas*))))

                           (if (similar-test (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) i))
                                             (elt (gethash (first pivo-dois) *genomas*) (+ (second pivo-dois) i))
                                             aadifflimit checkpointminlimit)(setf conservacao t));if similar-test
                           );condicao e acao 1--------------------------------------------------------------------------------------

                          ;condicao e acao 2----------------------------------------------------------------------------------------
                          ((and (>= (+ (second pivo-um) i) (length (gethash (first pivo-um) *genomas*)))
                                (< (+ (second pivo-dois) i) (length (gethash (first pivo-dois) *genomas*))))

                           (if (similar-test (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) i) (length (gethash (first pivo-um) *genomas*))))
                                             (elt (gethash (first pivo-dois) *genomas*) (+ (second pivo-dois) i))
                                             aadifflimit checkpointminlimit)(setf conservacao t));if similar-test
                           );condicao e acao 2-------------------------------------------------------------------------------------

                          ;condicao e acao 3----------------------------------------------------------------------------------------
                          ((and (< (+ (second pivo-um) i) (length (gethash (first pivo-um) *genomas*)))
                                (>= (+ (second pivo-dois) i) (length (gethash (first pivo-dois) *genomas*))))

                           (if (similar-test (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) i))
                                             (elt (gethash (first pivo-dois) *genomas*) (- (+ (second pivo-dois) i) (length (gethash (first pivo-dois) *genomas*))))
                                             aadifflimit checkpointminlimit)(setf conservacao t));if similar-test
                           );condicao e acao 3----------------------------------------------------------------------------------------

                          ;condicao e acao 4------------------------------------------------------------------------------------------
                          ((and (>= (+ (second pivo-um) i) (length (gethash (first pivo-um) *genomas*)))
                                (>= (+ (second pivo-dois) i) (length (gethash (first pivo-dois) *genomas*))))

                           (if (similar-test (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) i) (length (gethash (first pivo-um) *genomas*))))
                                             (elt (gethash (first pivo-dois) *genomas*) (- (+ (second pivo-dois) i) (length (gethash (first pivo-dois) *genomas*))))
                                             aadifflimit checkpointminlimit)(setf conservacao t));if similar-test
                           );condicao e acao 4----------------------------------------------------------------------------------------
                          );Fim cond

                        ;Bloco de ações para quando for constatado uma conservação gênica na expanção---------------------------------
                        (if (eql conservacao t)
                          (progn
                           ;;Caso haja uma conservação constatada, incremento os pesos do relatório de conservação.
                           (incf (first (elt pesos (1- i))))

                           (unless (or (equalp (first pivo-um) (first pivo-dois))
                                       (if (find-if #'(lambda (x) (equalp x (list (1- i) (first pivo-dois)))) genomas) t nil)
                                       );or
                             ;;A menos que o gene pivô 1 esteja no mesmo genoma que o gene pivô 2, ou se o gene pivô 2
                             ;;estiver em outro genoma, mas ja tiver sido constado vizinhanca gênica conserada
                             ;;nesse genoma, faça:
                             ;;A seguir incrementa-se o peso para cada vizinhanca gênica conservada constada entre genomas
                             ;;distintos.
                             (incf (second (elt pesos (1- i))))
                             ;;A seguir guarda-se os genomas onde foram constadas vizinhaca genica conservada para que o peso nao
                             ;;seja incrementado novamente, caso outra vizinhaca genica conservada seja constada nesse mesmo genoma.
                             (push (list (1- i) (first pivo-dois)) genomas)
                             );unless
                           );progn
                          );if constatação de conservação gênica-------------------------------------------------------------------------
                        );prong expanção
                    );Fim loop de expanção
                  );Fim dolist de expanção

          (setf (gethash k *relatorio-vizinhanca-genica*)
                (make-expansao :localidade pivo-um
                               :conservacoes pesos
                               );make-proteina
                );setf relatório-vizinhaça-gênica

          ;Verificando quantos genes conservados existentes dentro da janela de expanção-------------------------------------------------------
          (cond
            ;Caso haja pelo menos cw1(número requerido de genes conservados) na janela de tamanho w1
            ((>= (count-if #'(lambda (x) (> (first x) 1)) pesos) cw1) (setf qtd-ppi w1))
            ;Caso haja pelo menos cw2(número requerido de genes conservados) na janela de tamanho w2
            ((>= (count-if #'(lambda (x) (> (first x) 1)) (split-up-to pesos w2)) cw2) (setf qtd-ppi w2))
            ;Caso haja pelo menos cw3(número requerido de genes conservados) na janela de tamanho w3
            ((>= (count-if #'(lambda (x) (> (first x) 1)) (split-up-to pesos w3)) cw3) (setf qtd-ppi w3))
            ;Caso haja pelo menos cw4(número requerido de genes conservados) na janela de tamanho w4
            ((>= (count-if #'(lambda (x) (> (first x) 1)) (split-up-to pesos w4)) cw4) (setf qtd-ppi w4))
            );cond-------------------------------------------------------------------------------------------------------------------------

          ;;A menos que nao tenha nenhum gene conservado na janela de 10, faça:------------------------------------------------------------
          (unless (= qtd-ppi 0)
            (setf *ppi-identified-cn* t)
            ;;Criando arestas entre o gene pivô e os demais genes da janela de expansão:---------------------------------------------------
            (dotimes (i qtd-ppi)
                     (setf (gethash (first pivo-um) ppi)
                           (append (gethash (first pivo-um) ppi)
                            				   (list
                           				     (make-ppi-struct
                               						:genea k
                               						:geneb (if (< (+ (second pivo-um) (1+ i)) (length (gethash (first pivo-um) *genomas*)))
                                    							   (first (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) (1+ i))))
                                    							   (first (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) (1+ i)) (length (gethash (first pivo-um) *genomas*))))) )
                               						:weight (if (> (first (elt pesos i)) 1) (* 1.0 (first (elt pesos i))) 1.0)
                               						:position (second pivo-um)
                               						)
                           				     )
                                   );append
                           );setf
                     );dotimes

            ;;Criando arestas entre os genes não conservados dentro janela de enpanção:----------------------------------------------------
            (dotimes (i (1- qtd-ppi))
                     (unless (> (first (elt pesos i)) 1)
                       (loop for j from (1+ i) to (1- qtd-ppi)
                         do (progn
                    			      (setf interaction (make-ppi-struct
                                         						 :genea (if (< (+ (second pivo-um) (+ i 1)) (length (gethash (first pivo-um) *genomas*)))
                                                         (first (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) (+ i 1))))
                                                         (first (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) (+ i 1)) (length (gethash (first pivo-um) *genomas*))))) )
                                         						 :geneb (if (< (+ (second pivo-um) (+ j 1)) (length (gethash (first pivo-um) *genomas*)))
                                                         (first (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) (+ j 1))))
                                                         (first (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) (+ j 1)) (length (gethash (first pivo-um) *genomas*))))) )
                                         						 :weight 1.0
                                         						 :position (if (< (+ (second pivo-um) (+ i 1)) (length (gethash (first pivo-um) *genomas*)))
                                                            (+ (second pivo-um) (+ i 1))
                                                            (- (+ (second pivo-um) (+ i 1)) (length (gethash (first pivo-um) *genomas*)))
                                                            )
                                         						 )
                                   );setf interaction
                    			      ;;Esse if é para evitar duplicidade de interações na ppi
                    			      (if (and (eql nil (find-if #'(lambda (x) (and
                                                                          (equalp (ppi-struct-genea interaction) (ppi-struct-genea x))
                                                                			    (equalp (ppi-struct-geneb interaction) (ppi-struct-geneb x))
                                                                       );and
                                                                  );lambda
                                                							 (gethash (first pivo-um) ppi)))
                      				            (eql nil (gethash (ppi-struct-genea interaction)  *pan-genoma*))
                             					    );and
                               ;;Se a interação da vez já não estiver contida na ppi, e se a proteína pi não estiver contida no pan-genoma, faça:
                               (setf (gethash (first pivo-um) ppi) (append (gethash (first pivo-um) ppi) (list interaction)))
                               );if and
                             );progn
                         );loop
                       );unless
                     );dotimes
            );unless------------------------------------------------------------------------------------------------------------------------

          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *pan-genoma*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *pan-genoma*)) 100)) laco))))
          (incf genomenumber)

          );Fim do bloco de acoes para cada iteracao na tabela hash *pan-genoma*
      );Fim do loop na tabela hash *pan-genoma*

    ;;Normalizando os pesos das arestas pelo maior peso de cada genoma
    (loop for k being the hash-keys in ppi using (hash-value v)
      do (progn
          (setf maior-peso (loop for i in v
                             maximizing (ppi-struct-weight i) into max
                             finally (return max)))
          (dolist (p v)
                  (setf (ppi-struct-weight p) (* (/ (ppi-struct-weight p) maior-peso) percentage-cn))
                  )
          );progn
      );loop

    (format t "]~%")
    (return-from conserved-neighbourhood-fixed ppi)
    ))
;-------------------------------------------------------------------------------

;;Função dois para predizer ppis por vizinhaça gênica conservada------------------
(declaim (ftype (function (single-float fixnum fixnum fixnum) ) conserved-neighbourhood-dynamic))
(defun conserved-neighbourhood-dynamic(percentage-cn janela aadifflimit checkpointminlimit)
  (defparameter *relatorio-vizinhanca-genica* (make-hash-table :test #'equalp)
    "Tabela hash para armazenar o registro do número de genes conservados
   e não conservados para cada expanção de um gene pivô do pan-genoma")
  (defstruct expansao localidade conservacoes)
  (let ((ppi (make-hash-table :test #'equalp));Lista para armazenar as PPIs constatadas
                                              (pivo-um);Variavel para armazenar cada proteina pivo do pan-genoma
                                              (pos); Variável para guardar a última posição de uma vizinhança gênica conservada
                                              (pesos);Variável para armazenar as forças das interações entre as proteínas.
                                              (conservacao);Variável booleana para constatar uma conservação gênica.
                                              (genomas (list));Lista para auxiliar na incrementacao do peso
                                              (interaction)
                                              (maior-peso)
                                              (laco 0)
                                              (genomenumber 0)
                                              (total-expancoes)
                                              )

    (declare (type hash-table *relatorio-vizinhanca-genica*))
    (declare (type single-float percentage-cn))
    (declare (type fixnum janela))
    (declare (type fixnum aadifflimit))
    (declare (type fixnum checkpointminlimit))
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))

    (format t "~%Predicting ppi by conserved gene neighborhood;~%")
    (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
    (format *query-io* "[")
    (force-output *query-io*)

    ;;O loop a seguir varre a tabela hash *pan-genoma* e para cada proteína verifica-se
    ;;se ha similaridade proteica entre a sua vizinhança direita e a vizinhança direita
    ;;das proteinas similares a ela.
    (loop for k being the hash-keys in *pan-genoma* using (hash-value v)
      do (progn
          (setf pivo-um (proteina-localidade v))
          (setf pesos (loop for i from 1 to janela collecting (list 1 1)))
          (setf genomas (list))

          (dolist (pivo-dois (proteina-similares v))

                  (block expancao
                         (setf total-expancoes 0)
                         (setf pos 0)
                         (loop
                           (setf conservacao nil)
                           (loop for i from 1 to janela
                             do (progn
                                 (incf total-expancoes)
                                 (if (or (= total-expancoes (1- (length (gethash (first pivo-um) *genomas*))))
                                         (= total-expancoes (1- (length (gethash (first pivo-dois) *genomas*)))))
                                   (return-from expancao))
                                 (cond
                                   ;condicao e acao 1------------------------------------------------------------------------------------------
                                   ((and (< (+ (second pivo-um) (+ pos i)) (length (gethash (first pivo-um) *genomas*)))
                                         (< (+ (second pivo-dois) (+ pos i)) (length (gethash (first pivo-dois) *genomas*))))

                                    (if (similar-test (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) (+ pos i)))
                                                      (elt (gethash (first pivo-dois) *genomas*) (+ (second pivo-dois) (+ pos i)))
                                                      aadifflimit checkpointminlimit)(setf conservacao t))
                                    );condicao e acao 1----------------------------------------------------------------------------------------

                                   ;condicao e acao 2------------------------------------------------------------------------------------------
                                   ((and (>= (+ (second pivo-um) (+ pos i)) (length (gethash (first pivo-um) *genomas*)))
                                         (< (+ (second pivo-dois) (+ pos i)) (length (gethash (first pivo-dois) *genomas*))))

                                    (if (similar-test (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) (+ pos i)) (length (gethash (first pivo-um) *genomas*))))
                                                      (elt (gethash (first pivo-dois) *genomas*) (+ (second pivo-dois) (+ pos i)))
                                                      aadifflimit checkpointminlimit)(setf conservacao t))
                                    );condicao e acao 2----------------------------------------------------------------------------------------

                                   ;condicao e acao 3------------------------------------------------------------------------------------------
                                   ((and (< (+ (second pivo-um) (+ pos i)) (length (gethash (first pivo-um) *genomas*)))
                                         (>= (+ (second pivo-dois) (+ pos i)) (length (gethash (first pivo-dois) *genomas*))))

                                    (if (similar-test (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) (+ pos i)))
                                                      (elt (gethash (first pivo-dois) *genomas*) (- (+ (second pivo-dois) (+ pos i)) (length (gethash (first pivo-dois) *genomas*))))
                                                      aadifflimit checkpointminlimit)(setf conservacao t))
                                    );condicao e acao 3----------------------------------------------------------------------------------------

                                   ;condicao e acao 4------------------------------------------------------------------------------------------
                                   ((and (>= (+ (second pivo-um) (+ pos i)) (length (gethash (first pivo-um) *genomas*)))
                                         (>= (+ (second pivo-dois) (+ pos i)) (length (gethash (first pivo-dois) *genomas*))))

                                    (if (similar-test (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) (+ pos i)) (length (gethash (first pivo-um) *genomas*))))
                                                      (elt (gethash (first pivo-dois) *genomas*) (- (+ (second pivo-dois) (+ pos i)) (length (gethash (first pivo-dois) *genomas*))))
                                                      aadifflimit checkpointminlimit)(setf conservacao t))
                                    );condicao e acao 4----------------------------------------------------------------------------------------
                                   );cond

                                 ;Bloco de ações para quando for constatado uma conservação gênica na expanção---------------------------------
                                 (if (eql conservacao t)
                                   (progn
                                    ;;Bloco de código para fins de relatório-------------------------------------------------------------------
                                    ;;Expandindo o tamanho do relative-de-conservação caso seja necessário
                                    (unless (>= (length (split-after pesos (+ pos i))) janela)
                                      (setf pesos (append pesos
                                                          (loop for i from 1 to
                                                            (- janela (length (split-after pesos (+ pos i))))
                                                            collecting (list 1 1))
                                                          );append
                                            );setf
                                      );unless
                                    ;;Caso haja uma conservação constatada, incremento os pesos do relatório de conservação.
                                    (incf (first (elt pesos (+ pos (1- i)))))
                                    ;;Fim do bloco de código para fins de relatório-----------------------------------------------------------

                                    ;;A menos que a proteina similar esteja no mesmo genoma da proteina analisada, ou se a proteina
                                    ;;similiar estiver em outro genoma, mas ja tiver sido constado vizinhanca gênica conserada
                                    ;;nesse genoma, faça:
                                    (unless (or (equalp (first pivo-um) (first pivo-dois))
                                                (if (find-if #'(lambda (x) (equalp x (list (+ pos (1- i)) (first pivo-dois)))) genomas) t nil)
                                                );or

                                      (incf (second (elt pesos (+ pos (1- i)))))
                                      ;;A seguir guarda-se os genomas onde foram constadas vizinhaca genica conservada para que o peso nao
                                      ;;seja incrementado novamente, caso outra vizinhaca genica conservada seja constada nesse mesmo genoma.
                                      (push (list (+ pos (1- i)) (first pivo-dois)) genomas)
                                      );unless
                                    (setf pos (+ pos i))
                                    (setf i janela)
                                    );progn
                                   ;;Se não houver conservação gênica na expanção, faça:
                                   (if (= i janela); Se já expandiu até o limite da janela, encerra-se a expanção.
                                     (return-from expancao))
                                   );if constatação de conservação gênica-------------------------------------------------------------------------
                                 (setf conservacao nil)
                                 );progn
                             );loop janena
                           ); loop expanção
                         ); Fim bloco expanção
                  );dolist similares

          (setf (gethash k *relatorio-vizinhanca-genica*)
                (make-expansao :localidade pivo-um
                               :conservacoes pesos
                               );make-proteina
                );setf relatório-vizinhaça-gênica

          ;(terpri)
          ;(format t "~a => ~a~%" k pesos)

          ;A menos que nao tenha nenhum gene conservado na janela de expanção, faça:
          (unless (= (length pesos) janela)
            (setf *ppi-identified-cn* t)
            ;;Criando arestas entre o gene pivô e os demais genes da janela de enpanção:------------------------------------------------------------------------------------------------------
            (dotimes (i (length pesos))
                     (setf (gethash (first pivo-um) ppi)
                           (append (gethash (first pivo-um) ppi)
                                   (list (make-ppi-struct
                                   					  :genea k
                                   					  :geneb (if (< (+ (second pivo-um) (1+ i)) (length (gethash (first pivo-um) *genomas*)))
                                                   (first (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) (1+ i))))
                                                   (first (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) (1+ i)) (length (gethash (first pivo-um) *genomas*))))) )
                                   					  :weight (if (> (first (elt pesos i)) 1) (* 1.0 (first (elt pesos i))) 1.0)
                                   					  :position  (second pivo-um)
                                   					  )
                                         );list
                                   );append
                           );setf
                     );dotimes---------------------------------------------------------------------------------------------------------------------------------------------------------------

            ;;Criando arestas entre os genes não conservados dentro janela de enpanção:------------------------------------------------------------------------------------------------------
            (dotimes (i (1- (length pesos)))
                     (unless (> (first (elt pesos i)) 1)
                       (loop for j from (1+ i) to (1- (length pesos))
                         do (progn
                    			      (setf interaction (make-ppi-struct
                                         					  :genea (if (< (+ (second pivo-um) (+ i 1)) (length (gethash (first pivo-um) *genomas*)))
                                                         (first (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) (+ i 1))))
                                                         (first (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) (+ i 1)) (length (gethash (first pivo-um) *genomas*))))) )
                                         					  :geneb (if (< (+ (second pivo-um) (+ j 1)) (length (gethash (first pivo-um) *genomas*)))
                                                         (first (elt (gethash (first pivo-um) *genomas*) (+ (second pivo-um) (+ j 1))))
                                                         (first (elt (gethash (first pivo-um) *genomas*) (- (+ (second pivo-um) (+ j 1)) (length (gethash (first pivo-um) *genomas*))))) )
                                         					  :weight  1.0
                                         					  :position  (if (< (+ (second pivo-um) (+ i 1)) (length (gethash (first pivo-um) *genomas*)))
                                                             (+ (second pivo-um) (+ i 1))
                                                             (- (+ (second pivo-um) (+ i 1)) (length (gethash (first pivo-um) *genomas*)))
                                                             )
                                         					  )
                                   );setf interaction

                    			      ;;Esse if é para evitar duplicidade de interações na ppi
                    			      (if (and (eql nil (find-if #'(lambda (x) (and
                                                                            (equalp (ppi-struct-genea interaction) (ppi-struct-genea x))
                                                               						  (equalp (ppi-struct-geneb interaction) (ppi-struct-geneb x))
                                                                       );and
                                                                  );lambda
                                                							 (gethash (first pivo-um) ppi)))
                      				            (eql nil (gethash (ppi-struct-genea interaction)  *pan-genoma*))
                                      );and
                               ;;Se a interação da vez já não estiver contida na ppi, e se a proteína pi não estiver contida no pan-genoma, faça:
                               (setf (gethash (first pivo-um) ppi) (append (gethash (first pivo-um) ppi) (list interaction)))
                               );
                             );progn
                         );loop
                       );unless
                     );dotimes
            );unless---------------------------------------------------------------------------------------------------------------------------

          (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *pan-genoma*)) 100)) laco)))
                   (format *query-io* "=")
                   (force-output *query-io*)
                   )
          (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *pan-genoma*)) 100)) laco))))
          (incf genomenumber)

          );Fim do bloco de acoes para cada iteracao na tabela hash *pan-genoma*
      );Fim do loop na tabela hash *pan-genoma*

    ;;Normalizando os pesos das arestas pelo maior peso de cada genoma
    (loop for k being the hash-keys in ppi using (hash-value v)
      do (progn
          (setf maior-peso (loop for i in v
                             maximizing (ppi-struct-weight i) into max
                             finally (return max)))
          (dolist (p v)
                  (setf (ppi-struct-weight p) (* (/ (ppi-struct-weight p) maior-peso) percentage-cn))
                  )
          );progn
      );loop normalização.
    (format t "]~%")
    (return-from conserved-neighbourhood-dynamic ppi)
    ))
;;;;Fim funções para vizinhança gênica conservada-------------------------------
(defun relatorio-perfil-filogenetico (caminho percentage-pp ppdifftolerated pphistofilter
                                              ppaadifflimit ppaacheckminlimit aadifflimit
                                              aacheckminlimit
                                              )

  (let ((peso-grupo)(dividendo)(divisor)(total-arestas)(total-ppi-profile)
                    (diferenca-perfis)(perfil-cont 0)(similar-cont 0)
                    (laco 0)(genomenumber 0))

    (ensure-directories-exist caminho)
    (with-open-file (str caminho
                         :direction :output
                         :if-exists :supersede
                         :if-does-not-exist :create)

      (format str "~%")

      (if (= ppdifftolerated 0)
        (progn
         (format str "---------------------Report of complete ppi prediction by phylogenetic profile---------------------~%")
         (if (string= pphistofilter "Y")
           (cond
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 26)) (format str "Minimum identity percentage: 100%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 25)) (format str "Minimum identity percentage: 100%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 24)) (format str "Minimum identity percentage: 97,96%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 23)) (format str "Minimum identity percentage: 96,94%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 22)) (format str "Minimum identity percentage: 96,94%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 21)) (format str "Minimum identity percentage: 94,68%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 20)) (format str "Minimum identity percentage: 91,75%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 19)) (format str "Minimum identity percentage: 85,57%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 18)) (format str "Minimum identity percentage: 50%~%"))
             ((and (= ppaadifflimit 1) (= ppaacheckminlimit 26)) (format str "Minimum identity percentage: 97,87%~%"))
             ((and (= ppaadifflimit 1) (= ppaacheckminlimit 25)) (format str "Minimum identity percentage: 92,55%~%"))
             )
           (cond
             ((and (= aadifflimit 0) (= aacheckminlimit 26)) (format str "Minimum identity percentage: 100%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 25)) (format str "Minimum identity percentage: 100%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 24)) (format str "Minimum identity percentage: 97,96%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 23)) (format str "Minimum identity percentage: 96,94%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 22)) (format str "Minimum identity percentage: 96,94%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 21)) (format str "Minimum identity percentage: 94,68%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 20)) (format str "Minimum identity percentage: 91,75%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 19)) (format str "Minimum identity percentage: 85,57%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 18)) (format str "Minimum identity percentage: 50%~%"))
             ((and (= aadifflimit 1) (= aacheckminlimit 26)) (format str "Minimum identity percentage: 97,87%~%"))
             ((and (= aadifflimit 1) (= aacheckminlimit 25)) (format str "Minimum identity percentage: 92,55%~%"))
             )
           );if pphistofilter
         (format str "---------------------------------------------------------------------------------------------------~%")

         (format t "~%Writing phylogenetic profiles report;~%")
         (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
         (format *query-io* "[")
         (force-output *query-io*)

         (loop for k being the hash-keys in *agrupamento-por-perfis-filos-identicos* using (hash-value grupos)
           do (progn
               (setf total-arestas (loop for i in grupos
                                     summing (/ (* (length (second i)) (- (length (second i)) 1)) 2) into total
                                     finally (return total)))

               (format str "~%Genome: ~a ~%Total profiles: ~a ~%Total ppi: ~a~%" k (length grupos) total-arestas)
               (format str "---------------------------------------------------------------------------------------------------~%")
               (dolist (g grupos)

                       ;;Incrementando o contador de perfil
                       (setf perfil-cont (incf perfil-cont))
                       ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                       (setf dividendo (1+ (position-if #'(lambda (x) (= (third g) (third x))) (gethash k *pesos-grupos*))))
                       ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                       (setf divisor (length (gethash k *pesos-grupos*)))
                       ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                       (setf peso-grupo (* (/ dividendo divisor) percentage-pp))
                       ;;Calculando o total de arestas criadas para o perfil g
                       (setf total-ppi-profile (/ (* (length (second g)) (- (length (second g)) 1)) 2))

                       (format str "Profile ~6a => Total genes: ~7a| Total ppi: ~9a| Weight: ~,3f | Number of genomes: ~a;~%"
                               perfil-cont
                               (length (second g))
                               total-ppi-profile
                               (if (= total-ppi-profile 0) (- 0 0) (- peso-grupo 0))
                               (length(first g))
                               )
                       );dotimes grupos
               (setf perfil-cont 0)
               (format str "---------------------------------------------------------------------------------------------------~%")

               (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *agrupamento-por-perfis-filos-identicos*)) 100)) laco)))
                        (format *query-io* "=")
                        (force-output *query-io*)
                        )
               (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *agrupamento-por-perfis-filos-identicos*)) 100)) laco))))
               (incf genomenumber)

               );progn
           ); loop hash table *agrupamento-por-perfis-filos-identicos*
         (format t "]~%")
         );progn

        (progn
         (format str "-----------------------Report of complete ppi prediction by phylogenetic profile-----------------------~%")
         (if (string= pphistofilter "Y")
           (cond
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 26)) (format str "Minimum identity percentage: 100%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 25)) (format str "Minimum identity percentage: 100%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 24)) (format str "Minimum identity percentage: 97,96%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 23)) (format str "Minimum identity percentage: 96,94%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 22)) (format str "Minimum identity percentage: 96,94%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 21)) (format str "Minimum identity percentage: 94,68%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 20)) (format str "Minimum identity percentage: 91,75%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 19)) (format str "Minimum identity percentage: 85,57%~%"))
             ((and (= ppaadifflimit 0) (= ppaacheckminlimit 18)) (format str "Minimum identity percentage: 50%~%"))
             ((and (= ppaadifflimit 1) (= ppaacheckminlimit 26)) (format str "Minimum identity percentage: 97,87%~%"))
             ((and (= ppaadifflimit 1) (= ppaacheckminlimit 25)) (format str "Minimum identity percentage: 92,55%~%"))
             )
           (cond
             ((and (= aadifflimit 0) (= aacheckminlimit 26)) (format str "Minimum identity percentage: 100%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 25)) (format str "Minimum identity percentage: 100%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 24)) (format str "Minimum identity percentage: 97,96%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 23)) (format str "Minimum identity percentage: 96,94%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 22)) (format str "Minimum identity percentage: 96,94%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 21)) (format str "Minimum identity percentage: 94,68%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 20)) (format str "Minimum identity percentage: 91,75%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 19)) (format str "Minimum identity percentage: 85,57%~%"))
             ((and (= aadifflimit 0) (= aacheckminlimit 18)) (format str "Minimum identity percentage: 50%~%"))
             ((and (= aadifflimit 1) (= aacheckminlimit 26)) (format str "Minimum identity percentage: 97,87%~%"))
             ((and (= aadifflimit 1) (= aacheckminlimit 25)) (format str "Minimum identity percentage: 92,55%~%"))
             )
           );if pphistofilter
         (format str "-------------------------------------------------------------------------------------------------------~%")

         (format t "~%Writing phylogenetic profiles report;~%")
         (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
         (format *query-io* "[")
         (force-output *query-io*)

         (loop for k being the hash-keys in *agrupamento-por-perfis-filos-semelhantes* using (hash-value grupos)
           do (progn
               (setf total-arestas (loop for g in grupos
                                     summing (+
                                              ;;Calculando quantos pares de genes com perfis idênticos serão gerados.
                                              (/ (* (length (second (first g))) (- (length (second (first g))) 1)) 2)
                                              ;;Multiplicando o número de genes de perfis idênticos, pelo número de genes de perfis similares.
                                              (* (length (second (first g)))
                                                 ;;Somando a quantidade de genes de perfis similares.
                                                 (loop for gs in (second g) summing (length (second gs)) into total finally (return total))
                                                 );*
                                              );+
                                     into total
                                     finally (return total))
                     );setf total-arestas

               (format str "~%Genome: ~a ~%Total profiles: ~a ~%Total ppi: ~a~%" k (length grupos) total-arestas)
               (format str "-------------------------------------------------------------------------------------------------------~%")
               (dolist (g grupos)

                       ;;Incrementando o contador de perfil
                       (setf perfil-cont (incf perfil-cont))
                       ;;Pegando o índice do grupo de tamanho igual ao grupo g na lista pesos-grupos.
                       (setf dividendo (1+ (position-if #'(lambda (x) (= (third (first g)) (third (first x)))) (gethash k *pesos-grupos*))))
                       ;;Pegando o índice do grupo mais populoso, ou seja, o último grupo.
                       (setf divisor (length (gethash k *pesos-grupos*)))
                       ;;Calculando o peso do grupo g com base em sua posição, e na posição do último grupo (mais populoso).
                       (setf peso-grupo (* (/ dividendo divisor) percentage-pp))
                       ;;Calculando o total de arestas criadas para o perfil g
                       (setf total-ppi-profile (/ (* (length (second (first g))) (- (length (second (first g))) 1)) 2))

                       (format str "Profile ~9a => Total genes: ~7a| Total ppi: ~9a| Weight: ~,3f | Number of genomes: ~a;~%"
                               perfil-cont
                               (length (second (first g)))
                               total-ppi-profile
                               (if (= total-ppi-profile 0) (- 0 0) (- peso-grupo 0))
                               (length(first (first g)))
                               );format

                       (dolist (sg (second g))
                               ;;Incrementando o contador de perfil
                               (setf similar-cont (incf similar-cont))
                               ;Calculando a difença entre os perfis.
                               (setf diferenca-perfis (comparar-perfis (first (first g)) (first sg)))
                               (format str "Similar ~9a => Total genes: ~7a| Total ppi: ~9a| Weight: ~,3f | Number of genomes: ~a;~%"
                                       ;perfil-cont
                                       (concatenate 'string (write-to-string perfil-cont) "." (write-to-string similar-cont))
                                       ;similar-cont
                                       (length (second sg))
                                       ;;Multiplicando a quantidade de genes do perfi g, pelo total de genes do perfil sg (perfil i similar a g)
                                       ;;É nessário fazer essa multiplicação, pois cada gene do perfil g interage com cada gene do perfil similar sg
                                       (* (length (second (first g))) (length (second sg)))
                                       (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100)))
                                       (length (first sg))
                                       );format
                               );dolist sg
                       (setf similar-cont 0)
                       );dotimes grupos
               (setf perfil-cont 0)
               (format str "------------------------------------------------------------------------------------------------------~%")

               (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *agrupamento-por-perfis-filos-semelhantes*)) 100)) laco)))
                        (format *query-io* "=")
                        (force-output *query-io*)
                        )
               (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *agrupamento-por-perfis-filos-semelhantes*)) 100)) laco))))
               (incf genomenumber)

               );progn
           ); loop hash table *agrupamento-por-perfis-filos-semelhantes*
         (format t "]~%")
         );progn
        );if
      (format str "~%")
      );with-open-file
    );let
  );defun

;;Função para criar um relatório de pan-genoma----------------------------------
(declaim (ftype (function ( string fixnum fixnum) ) relatorio-pangenoma-1))
(defun relatorio-pangenoma-1(caminho aadifflimit aacheckminlimit)
  (let ((protein)(laco 0)(genomenumber 0))
    (ensure-directories-exist caminho)
    (with-open-file (str caminho
                         :direction :output
                         :if-exists :supersede
                         :if-does-not-exist :create)

      (format str "~%")

      (format str "-----------------------------------Pan genoma------------------------------------~%")
      (cond
        ((and (= aadifflimit 0) (= aacheckminlimit 26)) (format str "Minimum identity percentage: 100%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 25)) (format str "Minimum identity percentage: 100%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 24)) (format str "Minimum identity percentage: 97,96%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 23)) (format str "Minimum identity percentage: 96,94%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 22)) (format str "Minimum identity percentage: 96,94%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 21)) (format str "Minimum identity percentage: 94,68%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 20)) (format str "Minimum identity percentage: 91,75%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 19)) (format str "Minimum identity percentage: 85,57%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 18)) (format str "Minimum identity percentage: 50%~%"))
        ((and (= aadifflimit 1) (= aacheckminlimit 26)) (format str "Minimum identity percentage: 97,87%~%"))
        ((and (= aadifflimit 1) (= aacheckminlimit 25)) (format str "Minimum identity percentage: 92,55%~%"))
        )
      (format str "---------------------------------------------------------------------------------")

      (format t "~%Writing pan-genome format 1;~%")
      (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
      (format *query-io* "[")
      (force-output *query-io*)

      (loop for k being the hash-keys in *pan-genoma* using (hash-value v)
        do (progn
            (format str "~%~%Protein: ~a | Genome: ~a~%" (subseq k (1+ (position #\. k))) (first (proteina-localidade v)))
            (format str "~%Similar proteins:~%~%")
            (dolist (i (proteina-similares v))
                    (setf protein (first (elt (gethash (first i) *genomas*)(second i))))
                    (format str "Protein: ~a | Genome: ~a;~%"
                            (subseq protein (1+ (position #\. protein)))
                            (first i))
                    )
            (format str "---------------------------------------------------------------------------------")

            (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *pan-genoma*)) 100)) laco)))
                     (format *query-io* "=")
                     (force-output *query-io*)
                     )
            (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *pan-genoma*)) 100)) laco))))
            (incf genomenumber)

            );progn
        );loop hash table
      (format t "]~%")
      (format str "~%")
      );with-open-file
    )
  );----------------------------------------------------------------------------

(declaim (ftype (function ( string ) ) relatorio-pangenoma-2))
(defun relatorio-pangenoma-2 (caminho)
  (let ((similar)(laco 0)(genomenumber 0))
    (ensure-directories-exist caminho)
    (with-open-file (str caminho
                         :direction :output
                         :if-exists :supersede
                         :if-does-not-exist :create)

      (format t "~%Writing pan-genome format 2;~%")
      (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
      (format *query-io* "[")
      (force-output *query-io*)

      (format str "#Columns per row: Genome, Gene, Similar list~%~%")
      (loop for k being the hash-keys in *pan-genoma* using (hash-value v)
        do (progn
            (format str "~a, ~a," (first (proteina-localidade v)) (subseq k (1+ (position #\. k))))
            (dolist (i (proteina-similares v))
                    (setf similar (first (elt (gethash (first i) *genomas*)(second i))))
                    (format str " ~a" (subseq similar (1+ (position #\. similar))))
                    )
            (format str ";~%")

            (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *pan-genoma*)) 100)) laco)))
                     (format *query-io* "=")
                     (force-output *query-io*)
                     )
            (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *pan-genoma*)) 100)) laco))))
            (incf genomenumber)

            );progn
        );loop hash table
      (format t "]~%")
      (format str "~%")
      );with-open-file
    )
  )
;-------------------------------------------------------------------------------

;;Função para criar um relatório que mostre a quantidade de genes conservados e
;;não conservados nas vizinhanças dos genes do pan-genoma-----------------------
(defun relatorio-conservao-genica(caminho aadifflimit aacheckminlimit expansion-type w1 window-size)

  (let ((laco 0)(genomenumber 0))

    (ensure-directories-exist caminho)
    (with-open-file (str caminho
                         :direction :output
                         :if-exists :supersede
                         :if-does-not-exist :create)

      (format str "~%")

      (format str "#-----------------------------------------------Gene neighborhood conservation report----------------------------------------------~%")
      (format str "~%")
      (cond
        ((and (= aadifflimit 0) (= aacheckminlimit 26)) (format str "#Minimum identity percentage: 100%,"))
        ((and (= aadifflimit 0) (= aacheckminlimit 25)) (format str "#Minimum identity percentage: 100%,"))
        ((and (= aadifflimit 0) (= aacheckminlimit 24)) (format str "#Minimum identity percentage: 97,96%,"))
        ((and (= aadifflimit 0) (= aacheckminlimit 23)) (format str "#Minimum identity percentage: 96,94%,"))
        ((and (= aadifflimit 0) (= aacheckminlimit 22)) (format str "#Minimum identity percentage: 96,94%,"))
        ((and (= aadifflimit 0) (= aacheckminlimit 21)) (format str "#Minimum identity percentage: 94,68%,"))
        ((and (= aadifflimit 0) (= aacheckminlimit 20)) (format str "#Minimum identity percentage: 91,75%,"))
        ((and (= aadifflimit 0) (= aacheckminlimit 19)) (format str "#Minimum identity percentage: 85,57%,"))
        ((and (= aadifflimit 0) (= aacheckminlimit 18)) (format str "#Minimum identity percentage: 50%,"))
        ((and (= aadifflimit 1) (= aacheckminlimit 26)) (format str "#Minimum identity percentage: 97,87%,"))
        ((and (= aadifflimit 1) (= aacheckminlimit 25)) (format str "#Minimum identity percentage: 92,55%,"))
        )
      (if (string= expansion-type "fixed")
        (format str " Expansion type: fixed, Window size: ~a~%" w1)
        (format str " Expansion type: dynamic, Window size: ~a~%" window-size)
        )
      (format str "~%")
      (format str "#----------------------------------------------------------------------------------------------------------------------------------~%~%")

      (format t "~%Writing Gene neighborhood conservation report;~%")
      (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
      (format *query-io* "[")
      (force-output *query-io*)

      (format str "#Columns per row: Genome, Gene, Number of genes conserved, Number of genes not conserved, Number of genomes of each conserved gene;~%~%")
      (loop for k being the hash-keys in *relatorio-vizinhanca-genica* using (hash-value v)
        do (progn
            (format str "~a, ~a, ~a, ~a,"
                    (first (expansao-localidade v))
                    (subseq k (1+ (position #\. k)))
                    (count-if #'(lambda (x) (> (first x) 1)) (expansao-conservacoes v))
                    (count-if #'(lambda (x) (= (first x) 1)) (expansao-conservacoes v))
                    )
            (dolist (i (expansao-conservacoes v))
                    (unless (= (first i) 1)
                      (format str " ~a" (second i))
                      )
                    )
            (unless (> (count-if #'(lambda (x) (> (first x) 1)) (expansao-conservacoes v)) 0)
              (format str " 0")
              )
            (format str ";~%")

            (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *relatorio-vizinhanca-genica*)) 100)) laco)))
                     (format *query-io* "=")
                     (force-output *query-io*)
                     )
            (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *relatorio-vizinhanca-genica*)) 100)) laco))))
            (incf genomenumber)

            );progn
        );loop hash table
      (format t "]~%")
      (format str "~%")
      );with-open-file
    );let
  );----------------------------------------------------------------------------

;;Função para criar o relatório da quantidade de interações por feature em cada genoma.
(defun ppi-number-report (ppi-conserved-neighbourhood ppi-phylogenetic-profiles ppi-gene-fusion
                                                      caminho gene-fusion ppcn ppcomplete pplimit
                                                      pptrim ppthreshold ppdeletegroup ppdeleteprofile)

  (let ((laco 0)
        (genomenumber 0)
        )
    (declare (type fixnum laco))
    (declare (type fixnum genomenumber))

    (if (= (length *genomes-files*) 1)
      (setf ppcn "N"
            ppcomplete "N"
            pplimit "N"
            pptrim "N"
            ppthreshold "N"
            ppdeletegroup "N"
            ppdeleteprofile "N"
            )
      )

    (ensure-directories-exist caminho)
    (with-open-file (str caminho
                         :direction :output
                         :if-exists :supersede
                         :if-does-not-exist :create)

      (format str "~%")

      (format str "---------------------Amount of predicted interactions---------------------~%~%")
      (format str "--------------------------------------------------------------------------~%")

      (cond
        ((and (or (string= ppcn "Y") (string= ppcomplete "Y") (string= pplimit "Y") (string= pptrim "Y")
                  (string= ppthreshold "Y") (string= ppdeletegroup "Y") (string= ppdeleteprofile "Y"))
              (string= gene-fusion "N"))

         (format t "~%Writing ppi report;~%")
         (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
         (format *query-io* "[")
         (force-output *query-io*)

         (dolist (g *genomes-files*)

                 (format str "Genome name: ~22t~a~%" g)
                 (format str "Number of ppi by cn: ~22t~a~%" (length (gethash g ppi-conserved-neighbourhood)))
                 (format str "Number of ppi by pp: ~22t~a~%" (length (gethash g ppi-phylogenetic-profiles)))
                 (format str "--------------------------------------------------------------------------~%")

                 (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco)))
                          (format *query-io* "=")
                          (force-output *query-io*)
                          )
                 (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco))))
                 (incf genomenumber)

                 );dolist
         (format t "]~%")
         )
        ((and (or (string= ppcn "Y") (string= ppcomplete "Y") (string= pplimit "Y") (string= pptrim "Y")
                  (string= ppthreshold "Y") (string= ppdeletegroup "Y") (string= ppdeleteprofile "Y"))
              (string= gene-fusion "Y"))

         (format t "~%Writing ppi report;~%")
         (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
         (format *query-io* "[")
         (force-output *query-io*)

         (dolist (g *genomes-files*)

                 (format str "Genome name: ~22t~a~%" g)
                 (format str "Number of ppi by cn: ~22t~a~%" (length (gethash g ppi-conserved-neighbourhood)))
                 (format str "Number of ppi by pp: ~22t~a~%" (length (gethash g ppi-phylogenetic-profiles)))
                 (format str "Number of ppi by gf: ~22t~a~%" (length (gethash g ppi-gene-fusion)))
                 (format str "--------------------------------------------------------------------------~%")

                 (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco)))
                          (format *query-io* "=")
                          (force-output *query-io*)
                          )
                 (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco))))
                 (incf genomenumber)

                 );dolist
         (format t "]~%")
         )
        ((and (and (string= ppcn "N") (string= ppcomplete "N") (string= pplimit "N") (string= pptrim "N")
                   (string= ppthreshold "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N"))
              (string= gene-fusion "Y"))

         (format t "~%Writing ppi report;~%")
         (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
         (format *query-io* "[")
         (force-output *query-io*)

         (dolist (g *genomes-files*)

                 (format str "Genome name: ~22t~a~%" g)
                 (format str "Number of ppi by cn: ~22t~a~%" (length (gethash g ppi-conserved-neighbourhood)))
                 (format str "Number of ppi by gf: ~22t~a~%" (length (gethash g ppi-gene-fusion)))
                 (format str "--------------------------------------------------------------------------~%")

                 (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco)))
                          (format *query-io* "=")
                          (force-output *query-io*)
                          )
                 (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco))))
                 (incf genomenumber)

                 );dolist
         (format t "]~%")
         )
        ((and (and (string= ppcn "N") (string= ppcomplete "N") (string= pplimit "N") (string= pptrim "N")
                   (string= ppthreshold "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N"))
              (string= gene-fusion "N"))

         (format t "~%Writing ppi report;~%")
         (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
         (format *query-io* "[")
         (force-output *query-io*)

         (dolist (g *genomes-files*)

                 (format str "Genome name: ~22t~a~%" g)
                 (format str "Number of ppi by cn: ~22t~a~%" (length (gethash g ppi-conserved-neighbourhood)))
                 (format str "--------------------------------------------------------------------------~%")

                 (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco)))
                          (format *query-io* "=")
                          (force-output *query-io*)
                          )
                 (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco))))
                 (incf genomenumber)

                 );dolist
         (format t "]~%")
         )
        );cond
      );with-open-file
    );let
  );defun
;-------------------------------------------------------------------------------

(declaim (ftype (function ( string fixnum fixnum) ) gene-fusion-report))
(defun gene-fusion-report (caminho aadifflimit aacheckminlimit)

  (let ((laco 0)(genomenumber 0))
    (ensure-directories-exist caminho)
    (with-open-file (str caminho
                         :direction :output
                         :if-exists :supersede
                         :if-does-not-exist :create)

      (format str "~%")

      (format str "--------------------------------------Gene fusion report---------------------------------------~%")
      (cond
        ((and (= aadifflimit 0) (= aacheckminlimit 26)) (format str "Minimum identity percentage: 100%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 25)) (format str "Minimum identity percentage: 100%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 24)) (format str "Minimum identity percentage: 97,96%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 23)) (format str "Minimum identity percentage: 96,94%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 22)) (format str "Minimum identity percentage: 96,94%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 21)) (format str "Minimum identity percentage: 94,68%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 20)) (format str "Minimum identity percentage: 91,75%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 19)) (format str "Minimum identity percentage: 85,57%~%"))
        ((and (= aadifflimit 0) (= aacheckminlimit 18)) (format str "Minimum identity percentage: 50%~%"))
        ((and (= aadifflimit 1) (= aacheckminlimit 26)) (format str "Minimum identity percentage: 97,87%~%"))
        ((and (= aadifflimit 1) (= aacheckminlimit 25)) (format str "Minimum identity percentage: 92,55%~%"))
        )
      (format str "-----------------------------------------------------------------------------------------------")

      (format t "~%Writing gene fusion report;~%")
      (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
      (format *query-io* "[")
      (force-output *query-io*)

      (loop for g being the hash-keys in *fusoes* using (hash-value fusoes)
        do (progn
            (format str "~%~%Genome: ~15t~a~%~%" g)
            (dotimes (i (length fusoes))
                     (format str "Fusion ~a => ~15tPPi: ~a -- ~a~%" (1+ i)
                             (subseq (first(fusion-ppi (elt fusoes i))) (1+ (position #\. (first(fusion-ppi (elt fusoes i))))))
                             (subseq (third(fusion-ppi (elt fusoes i))) (1+ (position #\. (third(fusion-ppi (elt fusoes i))))))
                             );format
                     (dolist (f (fusion-rosetta-stone (elt fusoes i)))
                             (format str "~15tRosetta-stone: ~a in ~a~%"
                                     (subseq (first f) (1+ (position #\. (first f))))
                                     (second f)
                                     );format
                             );dolist
                     (format str "~%")
                     )
            (format str "-----------------------------------------------------------------------------------------------")

            (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *fusoes*)) 100)) laco)))
                     (format *query-io* "=")
                     (force-output *query-io*)
                     )
            (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (hash-table-count *fusoes*)) 100)) laco))))
            (incf genomenumber)

            );progn
        );loop hash table
      (format t "]~%")
      (format str "~%")
      );with-open-file
    );let
  )
;-------------------------------------------------------------------------------

;;Função que mescla as redes ppis de cada feature gerando uma ppi unica---------
(defun genppi (
               workingdir
               gene-fusion
               percentage-gf
               percentage-pp
               ppdifftolerated
               pphistofilter
               ppaadifflimit
               ppaacheckminlimit
               ppcn
               ppcomplete
               pplimit
               ppiterlimit
               pptrim
               trim
               ppthreshold
               threshold
               plusminus
               ppdeletegroup
               grouplimit
               ppdeleteprofile
               profiles
               expansion-type
               window-size
               percentage-cn
               w1
               cw1
               w2
               cw2
               w3
               cw3
               w4
               cw4
               aadifflimit
               aacheckminlimit
               );parameters

  (defparameter *genome-ppi* (make-hash-table :test #'equalp)
    "Tabela hash para armazenar a ppi de cada genoma")
  (let ((ppi-phylogenetic-profiles (make-hash-table :test #'equalp))
        (ppi-conserved-neighbourhood (make-hash-table :test #'equalp))
        (ppi-gene-fusion (make-hash-table :test #'equalp))
        (perfil-filo-g)
        (perfil-filo-A)
        (perfil-filo-B)
        (lista-ppi-pp (list))
        (caminho)
        (ppi-final (list))
        (laco 0)
        (genomenumber 0)
        )

    ;;Carregando os arquivos e gerando o pan-genoma
    (histo-genomas (list-directory workingdir))
    (if (string= pphistofilter "N")
      (pan-genoma-1 aadifflimit aacheckminlimit)
      (if (> (length *genomes-files*) 1)
        (pan-genoma-2 aadifflimit aacheckminlimit ppaadifflimit ppaacheckminlimit)
        (pan-genoma-1 aadifflimit aacheckminlimit)
        )
      )

    (unless (> (hash-table-count *pan-genoma*) 0)
      (format t "~%No protein in the pan-genome~%")
      )

    (if (or
         (and (= (hash-table-count *genomas*) 1)
              (string= gene-fusion "N")
              )
         (and (and (string= ppcn "N") (string= ppcomplete "N") (string= pplimit "N") (string= pptrim "N")
                   (string= ppthreshold "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N")
                   )
              (string= gene-fusion "N")
              );and
         );or
      (setf percentage-cn (- 1.0 0))
      )

    ;;Gerando a ppi por vizinhaçã gênica conservada---------------------------------------------------------------------
    (unless (= (hash-table-count *pan-genoma*) 0)
      (setf ppi-conserved-neighbourhood
            (if (string= expansion-type "fixed")
              (conserved-neighbourhood-fixed percentage-cn w1 cw1 w2 cw2 w3 cw3 w4 cw4 aadifflimit aacheckminlimit)
              (conserved-neighbourhood-dynamic percentage-cn window-size aadifflimit aacheckminlimit)
              );if
            );setf
      );unless

    (cond
      ;;Inicio da Condição e ação 1-----------------------------------------------------------------------------------------
      ((and (string= ppcn "Y") (> (hash-table-count *genomas*) 1))
       ;;Gerando ppi por perfil filogenético para as interações preditas por vizinhaça gênica conservada.

       (format t "~%Predicting ppi by phylogenetic profiles;~%")
       (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
       (format *query-io* "[")
       (force-output *query-io*)

       ;;Gerando ppi por perfil filogenético para as interações preditas por vizinhaça gênica conservada.
       (dolist (g *genomes-files*)
               (unless (eql nil (gethash g ppi-conserved-neighbourhood))
                 (progn
                  (setf perfil-filo-g (gethash g *perfil-filogenetico*))
                  (unless (eql perfil-filo-g nil)
                    (dolist (i (gethash g ppi-conserved-neighbourhood))
                            (setf perfil-filo-A (find-if #'(lambda (x) (equalp (ppi-struct-genea i) (first x))) perfil-filo-g))
                            (setf perfil-filo-B (find-if #'(lambda (x) (equalp (ppi-struct-geneb i) (first x))) perfil-filo-g))
                            (unless (or (eql perfil-filo-A nil)
                                        (eql perfil-filo-B nil)
                                        );or
                              (if (eql nil (set-exclusive-or (second perfil-filo-A) (second perfil-filo-B) :test #'string=))
                                (progn
                                 (push (list (list (first perfil-filo-A) '-- (first perfil-filo-B))
                                             (list 'Weight '= percentage-pp) (third perfil-filo-A)) lista-ppi-pp)
                                 );progn
                                (if (<= (length (set-exclusive-or (second perfil-filo-A) (second perfil-filo-B) :test #'string=)) ppdifftolerated)
                                  (progn
                                   (push (list (list (first perfil-filo-A) '-- (first perfil-filo-B))
                                               (list 'Weight '= percentage-pp) (third perfil-filo-A)) lista-ppi-pp)
                                   );progn
                                  )
                                ;Mesclando ppi:
                                ;(setf (third (second i)) (+ (third (second i)) (* percentage-pp 1.0)))
                                );if
                              );unless
                            );dolist
                    ;gravando ppi por perfil filogenetico na tabela hash ppi-phylogenetic-profiles.
                    (setf (gethash g ppi-phylogenetic-profiles) lista-ppi-pp)
                    (setf lista-ppi-pp nil)
                    );unlles
                  );progn
                 );if probe-file
               (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco)))
                        (format *query-io* "=")
                        (force-output *query-io*)
                        )
               (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco))))
               (incf genomenumber)
               );dolist genomas-files
       (format t "]~%")
       );Fim da Condição e ação 1-------------------------------------------------------------------------------------------

      ;Inicio da Condição e ação 2---------------------------------------------------------------------------------------
      ((and (string= ppcomplete "Y") (> (hash-table-count *genomas*) 1))
       (setf ppi-phylogenetic-profiles (phylogenetic-profiles-complete percentage-pp ppdifftolerated))
       );Fim Condição e ação 2-----------------------------------------------------------------------------------------------------

      ;Inicio da Condição e ação 3---------------------------------------------------------------------------------------
      ((and (string= pplimit "Y") (> (hash-table-count *genomas*) 1))
       (setf ppi-phylogenetic-profiles (phylogenetic-profiles-ppiterlimit percentage-pp ppdifftolerated ppiterlimit))
       );Fim Condição e ação 3-----------------------------------------------------------------------------------------------------

      ;Inicio da Condição e ação 4---------------------------------------------------------------------------------------
      ((and (string= pptrim "Y") (> (hash-table-count *genomas*) 1))
       (setf ppi-phylogenetic-profiles (phylogenetic-profiles-trim percentage-pp ppdifftolerated trim))
       );Fim Condição e ação 4-----------------------------------------------------------------------------------------------------

      ;Inicio da Condição e ação 5---------------------------------------------------------------------------------------
      ((and (string= ppthreshold "Y") (> (hash-table-count *genomas*) 1))
       (setf ppi-phylogenetic-profiles (phylogenetic-profiles-threshold percentage-pp ppdifftolerated threshold plusminus))
       );Fim Condição e ação 5-----------------------------------------------------------------------------------------------------

      ;Inicio da Condição e ação 6---------------------------------------------------------------------------------------
      ((and (string= ppdeletegroup "Y") (> (hash-table-count *genomas*) 1))
       (setf ppi-phylogenetic-profiles (phylogenetic-profiles-delete-clusters percentage-pp ppdifftolerated grouplimit))
       );Fim Condição e ação 6-----------------------------------------------------------------------------------------------------

      ;Inicio da Condição e ação 7---------------------------------------------------------------------------------------
      ((and (string= ppdeleteprofile "Y") (> (hash-table-count *genomas*) 1))
       (setf ppi-phylogenetic-profiles (phylogenetic-profiles-delete-perfil percentage-pp ppdifftolerated profiles))
       );Fim Condição e ação 7-----------------------------------------------------------------------------------------------------
      );cond

    ;;Caso o usuário tenha escolhido rodar a fusão gênica
    (if (string= gene-fusion "Y")
      (setf ppi-gene-fusion (rosetta-stone aadifflimit aacheckminlimit percentage-gf))
      )

    ;Se foi encontrado ppi para uma das features--------------------------------
    (if (or *ppi-identified-cn* *ppi-identified-pp* *ppi-identified-gf*)
      (progn

       (if (> (hash-table-count *pan-genoma*) 0)
         (progn
          ;;Relatório do pan-genoma
          (setf caminho (concatenate 'string workingdir "/pan-genome/" "pan-genome-format-1.txt"))
          (relatorio-pangenoma-1 caminho aadifflimit aacheckminlimit)
          (setf caminho (concatenate 'string workingdir "/pan-genome/" "pan-genome-format-2.txt"))
          (relatorio-pangenoma-2 caminho)

          ;;Relatório de vizinhança gênica conservada
          (setf caminho (concatenate 'string workingdir "/gene-neighborhood-conservation-report/" "report.txt"))
          (relatorio-conservao-genica caminho aadifflimit aacheckminlimit expansion-type w1 window-size)
          )
         )

       ;;Relatório de phylogenetic profiles
       (if (and (> (length *genomes-files*) 1)
                (string= ppcomplete "Y")
                )
         (progn
          (setf caminho (concatenate 'string workingdir "/phylogenetic-profiles-report/report.txt"))
          (relatorio-perfil-filogenetico caminho percentage-pp ppdifftolerated pphistofilter
                                         ppaadifflimit ppaacheckminlimit aadifflimit aacheckminlimit)
          );progn
         );if relatório pp

       ;;Relatório para fusões gênicas
       (if *ppi-identified-gf*
         (progn
          (setf caminho (concatenate 'string workingdir "/gene-fusion-report/report.txt"))
          (gene-fusion-report caminho aadifflimit aacheckminlimit)
          )
         )

       ;;Relatório de ppi
       ;;Imprimindo relatório das quantidades de interações por feature.
       (setf caminho (concatenate 'string workingdir "/ppi-report/report.txt"))
       (ppi-number-report ppi-conserved-neighbourhood ppi-phylogenetic-profiles ppi-gene-fusion
                          caminho gene-fusion ppcn ppcomplete pplimit
                          pptrim ppthreshold ppdeletegroup ppdeleteprofile)


       ;;Resetando variáveis da barra de progresso para caso a opção -ppcn seja escolhida.
       (setf genomenumber 0)
       (setf laco 0)

       (format t "~%Writing ppi files;~%")
       (format t "[05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100]%~%")
       (format *query-io* "[")
       (force-output *query-io*)
       ;;Este dolist gera a ppi-final de cada genoma
       (dolist (g *genomes-files*)

               (if *ppi-identified-cn*
                 (setf ppi-final (append ppi-final (gethash g ppi-conserved-neighbourhood)))
                 )
               (if *ppi-identified-pp*
                 (setf ppi-final (append ppi-final (gethash g ppi-phylogenetic-profiles)))
                 )
               (if *ppi-identified-gf*
                 (setf ppi-final (append ppi-final (gethash g ppi-gene-fusion)))
                 )

               ;Esta linha foi necessária para resolver um problema decorrente da ordenação a sequir.
               (setf ppi-final (append ppi-final (list)))

               ;;Ordenando a ppi final do genoma g
               (setf ppi-final (sort ppi-final #'< :key #'(lambda (x) (ppi-struct-position x))))

               ;;Formatando o valor do peso da ppi final do genoma g
               (dolist (i ppi-final)
                       (setf (ppi-struct-weight i)
                             (with-input-from-string
                               (in (format nil "~,3f" (ppi-struct-weight i)))
                               (read in))
                             );setf
                       );dolist

               ;;Gravando arquivo ppi.dot para o genoma g
               (setf caminho (concatenate 'string workingdir "/ppi-files/" g ".dot"))
               (ensure-directories-exist caminho)
               (with-open-file (str caminho
                                    :direction :output
                                    :if-exists :supersede
                                    :if-does-not-exist :create)

                 (format str (concatenate 'string "graph " g " {~%"))

                 (dolist (i ppi-final)
                         (format str "~S -- ~S [WEIGHT = ~S];~%"
                                 ;(ppi-struct-genea i)
                                 ;(ppi-struct-geneb i)
                                 (subseq (ppi-struct-genea i) (1+ (position #\. (ppi-struct-genea i))))
                                 (subseq (ppi-struct-geneb i) (1+ (position #\. (ppi-struct-geneb i))))
                                 (ppi-struct-weight i)
                                 );format
                         );dolist
                 (format str "}~%")
                 );with-open-file

               ;;Resetando a variável ppi-final para o próximo genoma g
               (setf ppi-final (list))

               ;;Início da geração de arquivos de PPI .SIF----------------------
               ;;Gravando arquivo ppi.sif para o genoma g
               (setf caminho (concatenate 'string workingdir "/ppi-files/" g ".sif"))
               (ensure-directories-exist caminho)
               (with-open-file (str caminho
                                    :direction :output
                                    :if-exists :supersede
                                    :if-does-not-exist :create)

                 (if *ppi-identified-cn*
                   (progn
                    ;;Ordenando as ppis do genoma g
                    (setf (gethash g ppi-conserved-neighbourhood) (sort (gethash g ppi-conserved-neighbourhood) #'< :key #'(lambda (x) (ppi-struct-position x))))

                    (dolist (i (gethash g ppi-conserved-neighbourhood))
                            (format str "~S~Ccn~C~S~%"
                                    ;(ppi-struct-genea i)
                                    ;(ppi-struct-geneb i)
                                    (subseq (ppi-struct-genea i) (1+ (position #\. (ppi-struct-genea i))))
                                    #\tab
                                    #\tab
                                    (subseq (ppi-struct-geneb i) (1+ (position #\. (ppi-struct-geneb i))))
                                    );format
                            );dolist
                    );progn
                   );if

                 (if *ppi-identified-pp*
                   (progn
                    ;;Ordenando as ppis do genoma g
                    (setf (gethash g ppi-phylogenetic-profiles) (sort (gethash g ppi-phylogenetic-profiles) #'< :key #'(lambda (x) (ppi-struct-position x))))

                    (dolist (i (gethash g ppi-phylogenetic-profiles))
                            (format str "~S~Cpp~C~S~%"
                                    ;(ppi-struct-genea i)
                                    ;(ppi-struct-geneb i)
                                    (subseq (ppi-struct-genea i) (1+ (position #\. (ppi-struct-genea i))))
                                    #\tab
                                    #\tab
                                    (subseq (ppi-struct-geneb i) (1+ (position #\. (ppi-struct-geneb i))))
                                    );format
                            );dolist
                    );progn
                   );if

                 (if *ppi-identified-gf*
                   (progn
                    ;;Ordenando as ppis do genoma g
                    (setf (gethash g ppi-gene-fusion) (sort (gethash g ppi-gene-fusion) #'< :key #'(lambda (x) (ppi-struct-position x))))

                    (dolist (i (gethash g ppi-gene-fusion))
                            (format str "~S~Cgf~C~S~%"
                                    ;(ppi-struct-genea i)
                                    ;(ppi-struct-geneb i)
                                    (subseq (ppi-struct-genea i) (1+ (position #\. (ppi-struct-genea i))))
                                    #\tab
                                    #\tab
                                    (subseq (ppi-struct-geneb i) (1+ (position #\. (ppi-struct-geneb i))))
                                    );format
                            );dolist
                    );progn
                   );if
                 );with-open-file
               ;;Fim da geração de arquivos de PPI .SIF-------------------------

               (dotimes (j (ceiling (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco)))
                        (format *query-io* "=")
                        (force-output *query-io*)
                        )
               (setf laco (ceiling (+ laco (- (* 60 (/ (/ (* (1+ genomenumber) 100) (length *genomes-files*)) 100)) laco))))
               (incf genomenumber)

               );dolist
       (format t "]~%")

       (format t "~%~%PPi prediction completed successfully.~%")

       ;;Exibindo o caminho dos arquivos de pan-genoma e vizinhaça gênica conservada.
       (if (> (hash-table-count *pan-genoma*) 0)
         (progn
          (if (char= #\/ (elt workingdir (1- (length workingdir))))
            (progn
             (format t "~%Pan-genome path: ~a" (concatenate 'string workingdir "pan-genome/"))
             (format t "~%Gene-neighborhood-conservation-report path: ~a" (concatenate 'string workingdir "gene-neighborhood-conservation-report/" "report.txt"))
             )
            (progn
             (format t "~%Pan-genome path: ~a" (concatenate 'string workingdir "/pan-genome/"))
             (format t "~%Gene-neighborhood-conservation-report path: ~a" (concatenate 'string workingdir "/gene-neighborhood-conservation-report/" "report.txt"))
             )
            );if char=
          );progn
         );if hash-table.

       ;;Exibindo o caminho do relatório de perfil filogenético.
       (if (and (> (length *genomes-files*) 1)
                (string= ppcomplete "Y")
                )
         (if (char= #\/ (elt workingdir (1- (length workingdir))))
           (format t (concatenate 'string "~%Phylogenetic-profiles-report path: "
                                  (concatenate 'string workingdir "phylogenetic-profiles-report/report.txt")))
           (format t (concatenate 'string "~%Phylogenetic-profiles-report path: "
                                  (concatenate 'string workingdir "/phylogenetic-profiles-report/report.txt")))
           )
         )

       ;;Exibindo o caminho do relatório de fusão gênica.
       (if *ppi-identified-gf*
         (if (char= #\/ (elt workingdir (1- (length workingdir))))
           (format t (concatenate 'string "~%Gene-fusion-report path: "
                                  (concatenate 'string workingdir "gene-fusion-report/report.txt")))
           (format t (concatenate 'string "~%Gene-fusion-report path: "
                                  (concatenate 'string workingdir "/gene-fusion-report/report.txt")))
           )
         )

       ;;Exibindo o caminho do arquivos .dot de ppi
       (if (char= #\/ (elt workingdir (1- (length workingdir))))
         (format t (concatenate 'string "~%PPi-files path: " (concatenate 'string workingdir "ppi-files")))
         (format t (concatenate 'string "~%PPi-files path: " (concatenate 'string workingdir "/ppi-files")))
         )

       ;;Exibindo os caminho do relatório de ppi
       (if (char= #\/ (elt workingdir (1- (length workingdir))))
         (format t (concatenate 'string "~%PPi-report path: " (concatenate 'string workingdir "ppi-report/report.txt")))
         (format t (concatenate 'string "~%PPi-report path: " (concatenate 'string workingdir "/ppi-report/report.txt")))
         )

       );progn Fim do bloco de ações para caso houver ppi de uma ou mais featrues
      ;-------------------------------------------------------------------------

      ;;Caso não tenho sido constado PPis, faça:--------------------------------
      (progn
       (if (= (hash-table-count *pan-genoma*) 0)
         (progn
          (terpri)
          (format t "No ppi identified~%")
          )
         )
       );progn
      );if *ppi-identified-pp* *ppi-identified-cn* *ppi-identified-gf*

    ;;A menos que não houve genes no pan-genoma ou nenhuma ppi foi constatada, faça:
    (unless (or (= (hash-table-count *pan-genoma*) 0) (or *ppi-identified-pp* *ppi-identified-cn* *ppi-identified-gf*))
      ;;Criando um relatório do pan-genoma
      (setf caminho (concatenate 'string workingdir "/pan-genome/" "pan-genome-format-1.txt"))
      (relatorio-pangenoma-1 caminho aadifflimit aacheckminlimit)
      (setf caminho (concatenate 'string workingdir "/pan-genome/" "pan-genome-format-2.txt"))
      (relatorio-pangenoma-2 caminho)
      (setf caminho (concatenate 'string workingdir "/gene-neighborhood-conservation-report/" "report.txt"))
      (relatorio-conservao-genica caminho aadifflimit aacheckminlimit expansion-type w1 window-size)
      (terpri)
      (terpri)
      (format t "No ppi identified~%")
      (if (char= #\/ (elt workingdir (1- (length workingdir))))
        (format t "~%Pan-genome path: ~a" (concatenate 'string workingdir "pan-genome/"))
        (format t "~%Pan-genome path: ~a" (concatenate 'string workingdir "/pan-genome/"))
        )
      (if (char= #\/ (elt workingdir (1- (length workingdir))))
        (format t "~%Gene-neighborhood-conservation-report path: ~a" (concatenate 'string workingdir "gene-neighborhood-conservation-report/" "report.txt"))
        (format t "~%Gene-neighborhood-conservation-report path: ~a" (concatenate 'string workingdir "/gene-neighborhood-conservation-report/" "report.txt"))
        )
      );unless
    );let
  );defun
;;------------------------------------------------------------------------------

;;Função principal para receber os parametros do programa e executa-lo----------
(defun main ()
  (let (
        (parameter 1)
        (parametertag)
        (parametervalue)
        (help T)
        (workingdir)
        (gene-fusion "N")
        (percentage-gf 0.05)
        (percentage-pp 0.30)
        (aux-percentage-pp nil)
        (ppdifftolerated 0)
        (pphistofilter "N")
        (ppaadifflimit 0)
        (ppaacheckminlimit 26)
        (ppcn "N")
        (ppcomplete "N")
        (pplimit "N")
        (ppiterlimit 500000)
        (pptrim "N")
        (trim 45000)
        (ppthreshold "N")
        (threshold 0)
        (plusminus "nil")
        (ppdeletegroup "N")
        (grouplimit 45000)
        (ppdeleteprofile "N")
        (profiles "0")
        (expansion-type "fixed")
        (window-size 1)
        (percentage-cn 0.65)
        (aux-percentage-cn nil)
        (w1 10)
        (cw1 4)
        (w2 7)
        (cw2 3)
        (w3 5)
        (cw3 2)
        (w4 3)
        (cw4 1)
        (aadifflimit 1)
        (aacheckminlimit 25)
        (directory-num 0)
        (qtd-arquivos-fasta 0)
    	   ); let pars

   	;O número 31 fixo neste código significa que o programa pode receber até 28 parâmetros na linha de comando.
   	;Não me preocupo em saber quantos parâmetros foram passados, apenas verifico se cada um dos seis posssíveis
   	;foi passado e em caso positivo eu processo o parâmetro.
   	(dotimes (set 31)
           		;Os parâmetros devem estar sempre em pares de tag e valor da tag. Por isso, para cada parâmetro encontrado,
           		;o primeiro e ímpar é a tag e o segundo e par é o valor do parâmetro.

             (setf parametertag (nth parameter sb-ext:*posix-argv*))

             (unless (or (string= parametertag "-pphistofilter") (string= parametertag "-ppcn")
                         (string= parametertag "-ppcomplete") (string= parametertag "-help")
                         (string= parametertag "-genefusion") (string= parametertag "-version")
                         )
               (setf parametervalue (nth (+ parameter 1) sb-ext:*posix-argv*))
               )

          	  ;(if (and parametertag parametervalue)
             ;A menos que a tag não exista, verifica-se quem ela é.
             (unless (not parametertag)
               (setf help nil)
          	    (cond
                 ((string= parametertag "-version")
                  (progn
                   (terpri)
                   (format t "GENPPI VERSION: 1.0~%")
                   (format t "RELEASE NUMBER: 4354980a4dfaede2695e82e525914cead0f2c9ee~%")
                   (format t "REPOSITORY: https://github.com/santosardr/genppi ~%")
                   (terpri)
                   (SB-EXT:EXIT)
                   )
                  )
                 ((string= parametertag "-help")
                  (progn
                   (terpri)
                   (format t "Mandatory parameter~%~%")
                   (format t "Directory parameter:~%")
                   (format t "-dir <workingdir> directory~%~%")
                   (format t "Optional parameters~%~%")
                   (format t "Parameters of ppi by conserved neighbourhood:~%")
                   (format t "-cnp <conserved neighbourhood score percentage> (65-default)~%")
                   (format t "-expt <type of expansion of the conserved gene neighborhood> 'fixed' or 'dynamic' (fixed-default)~%~%")
                   (format t "Parameters of conserved neighbourhood by fixed expansion:~%")
                   (format t "-w1 <window size 1> (10-default)~%")
                   (format t "-cw1 <gene conservation required for window 1> (4-default)~%")
                   (format t "-w2 <window size 2> (7-default)~%")
                   (format t "-cw2 <gene conservation required for window 2> (3-default)~%")
                   (format t "-w3 <window size 3> (5-default)~%")
                   (format t "-cw3 <gene conservation required for window 3> (2-default)~%")
                   (format t "-w4 <window size 4> (3-default)~%")
                   (format t "-cw4 <gene conservation required for window 4> (1-default)~%~%")
                   (format t "Parameters of conserved neighbourhood by dynamic expansion:~%")
                   (format t "-ws <dynamic expansion window size> (1-default)~%~%")
                   (format t "Parameters of ppi by phylogenetic profile:~%")
                   (format t "-ppp <phylogenetic profiles score percentage> (30-default)~%")
                   (format t "-ppdifftolerated <difference in phylogenetic profiles tolerated to infer ppi> (0-default)~%~%")
                   (format t "Amino acid histogram parameter settings for the phylogenetic profile:~%")
                   (format t "-pphistofilter <build the phylogenetic profile of genes with a higher percentage of identity>~%")
                   (format t "-ppaadifflimit <amino acid difference limit> (0-default)~%")
                   (format t "-ppaacheckminlimit <minimum amount of amino acids to check> (26-default)~%~%")
                   (format t "Methods of ppi prediction by phylogenetic profile and its parameters:~%~%")
                   (format t "Method 1 - Predict ppi by phylogenetic profile only for interactions predicted by conserved neighborhood~%")
                   (format t "-ppcn~%~%")
                   (format t "Method 2 - PPi prediction by phylogenetic profile without filters 2~%")
                   (format t "-ppcomplete~%~%")
                   (format t "Method 3 - Prediction of ppi by phylogenetic profile with a limit of interactions~%")
                   (format t "-ppiterlimit <maximum number of interactions desired> (500000-default)~%~%")
                   (format t "Method 4 - Prediction of ppi by phylogenetic profile with interactions limit by weight~%")
                   (format t "-trim <maximum number of interactions by weight> (45000-default)~%~%")
                   (format t "Method 5 - Prediction of ppi by the phylogenetic profile only for genes with profiles that cover a greater or lesser number of genomes than an informed threshold~%")
                   (format t "-threshold <phylogenetic profiles threshold>~%")
                   (format t "-plusminus <parameter that receives the greater than or less than sign to apply the ppthreshod filter> '<' or '>' (signs greater than and less than, must be enclosed in single or double quotes)~%~%")
                   (format t "Method 6 - Delete groups of ppi predicted by phylogenetic profile that exceed a limit of interactions by weight~%")
                   (format t "-grouplimit <limit of tolerated interactions to maintain a group of ppi with the same weight> (45000-default)~%~%")
                   (format t "Method 7 - To exclude genes with unwanted profiles in predicting ppi by phylogenetic profile~%")
                   (format t (concatenate 'string "-profiles <number of genomes in the unwanted profiles> Entry example: 7 (genes that co-occur in a total of 7 genomes will be excluded). To insert more than one profile, the entry must be enclosed in single or double quotes, and the values separated by semicolons. Example: ""\"7; 15; 21\"~%~%"))
                   (format t "Parameters of ppi by gene fusion:~%")
                   (format t "-genefusion <make ppi predictions by gene fusion>~%")
                   (format t "-gfp <gene fusion score percentage> (5-default)~%")
                   (format t "Note: if gene fusion is included, there will be an increase of more than 100% in the execution time~%~%")
                   (format t "Amino acid histogram parameters:~%")
                   (format t "-aadifflimit <amino acid difference limit> (1-default)~%")
                   (format t "-aacheckminlimit <minimum amount of amino acids to check> (25-default)~%~%")
                   (format t "Possible configurations of the histogram parameters:~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "-aadifflimit   -aacheckminlimit   minimal protein identity %~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      0               26                     100%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      0               25                     100%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      0               24                    97,96%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      0               23                    96,94%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      0               22                    96,94%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      0               21                    94,68%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      0               20                    91,75%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      0               19                    85,57%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      0               18                    50,00%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      1               26                    97,87%~%")
                   (format t "-------------------------------------------------------------~%")
                   (format t "      1               25                    92,55%~%")
                   (format t "-------------------------------------------------------------~%")
                   (terpri)
                   (SB-EXT:EXIT)
                   )
                  )
                	((string= parametertag "-dir")
                  (setf workingdir (if parametervalue
                                     (if (char= (elt parametervalue 0) #\-) "./" parametervalue)
                                     "./"
                                     );if parametervalue
                        )
                  );diretório de execução
                 ((string= parametertag "-genefusion") (setf gene-fusion "Y"))
                 ((string= parametertag "-gfp")
                  (setf percentage-gf  (if parametervalue
                                         (if (numberp (read-from-string parametervalue))
                                           (if (or (< (parse-integer parametervalue) 0) (> (parse-integer parametervalue) 100))
                                             (progn
                                              (terpri)
                                              (format t "Warning:~%")
                                              (format t "Invalid parameter: -gfp <gene fusion score percentage> ~%")
                                              (format t "The prediction percentage by gene fusion can only take values from 0 to 100.")
                                              (terpri)
                                              (SB-EXT:EXIT)
                                              )
                                             (/ (parse-integer parametervalue) 100)
                                             )
                                           (progn
                                            (terpri)
                                            (format t "Warning:~%")
                                            (format t"Invalid parameter: -gfp <gene fusion score percentage>~%")
                                            (format t "The prediction percentage by gene fusion can only take values from 0 to 100.")
                                            (terpri)
                                     	      (SB-EXT:EXIT)
                                            )
                                           )
                                         percentage-gf
                                         )
                        ))
                 ((string= parametertag "-ppp")
                  (setf percentage-pp  (if parametervalue
                                         (if (numberp (read-from-string parametervalue))
                                           (if (or (< (parse-integer parametervalue) 0) (> (parse-integer parametervalue) 100))
                                             (progn
                                              (terpri)
                                              (format t "Warning:~%")
                                              (format t "Invalid parameter: -ppp <phylogenetic profiles score percentage>~%")
                                              (format t "The prediction percentage by phylogenetic profile can only take values from 0 to 100.")
                                              (terpri)
                                              (SB-EXT:EXIT)
                                              )
                                             (progn
                                              (setf aux-percentage-pp t)
                                              (/ (parse-integer parametervalue) 100)
                                              )
                                             )
                                           (progn
                                            (terpri)
                                            (format t "Warning:~%")
                                            (format t"Invalid parameter: -ppp <phylogenetic profiles score percentage>~%")
                                            (format t "The prediction percentage by phylogenetic profile can only take values from 0 to 100.")
                                            (terpri)
                                     	      (SB-EXT:EXIT)
                                            )
                                           )
                                         percentage-pp
                                         )
                        ))
                 ((string= parametertag "-ppdifftolerated")
                  (setf ppdifftolerated  (if parametervalue
                                           (if (numberp (read-from-string parametervalue))
                                             (if (< (parse-integer parametervalue) 0)
                                               (progn
                                                (terpri)
                                                (format t "Warning:~%")
                                                (format t "Invalid parameter: -ppdifftolerated <difference in phylogenetic profiles tolerated to infer ppi>~%")
                                                (format t "The difference in phylogenetic profiles tolerated to infer ppi cannot be less than zero.")
                                                (terpri)
                                                (SB-EXT:EXIT)
                                                )
                                               (- (parse-integer parametervalue) 0)
                                               )
                                             (progn
                                              (terpri)
                                              (format t "Warning:~%")
                                              (format t "Invalid parameter: -ppdifftolerated <difference in phylogenetic profiles tolerated to infer ppi>~%")
                                              (format t "The difference in phylogenetic profiles tolerated to infer ppi must be an integer.")
                                              (terpri)
                                       	      (SB-EXT:EXIT)
                                              )
                                             )
                                           ppdifftolerated
                                           )
                        ))
                 ((string= parametertag "-pphistofilter") (setf pphistofilter  "Y"))
                 ((string= parametertag "-ppaadifflimit")
                  (setf ppaadifflimit  (if parametervalue
                                         (if (numberp (read-from-string parametervalue))
                                           (if (or (= (parse-integer parametervalue) 0) (= (parse-integer parametervalue) 1))
                                             (parse-integer parametervalue)
                                             (progn
                                              (terpri)
                                              (format t "Warning:~%")
                                              (format t "Invalid parameter: -ppaadifflimit <amino acid difference limit>~%~%")
                                              (terpri)
                                              (SB-EXT:EXIT)
                                              )
                                             )
                                           (progn
                                            (terpri)
                                            (format t "Warning:~%")
                                            (format t "Invalid parameter: -ppaadifflimit <amino acid difference limit>~%~%")
                                            (terpri)
                                            (SB-EXT:EXIT)
                                            )
                                           )
                                         ppaadifflimit
                                         )
                        ))
                 ((string= parametertag "-ppaacheckminlimit")
                  (setf ppaacheckminlimit  (if parametervalue
                                             (if (numberp (read-from-string parametervalue))
                                               (if (or (= (parse-integer parametervalue) 26) (= (parse-integer parametervalue) 25)
                                                       (= (parse-integer parametervalue) 24) (= (parse-integer parametervalue) 23)
                                                       (= (parse-integer parametervalue) 22) (= (parse-integer parametervalue) 21)
                                                       (= (parse-integer parametervalue) 20) (= (parse-integer parametervalue) 19)
                                                       (= (parse-integer parametervalue) 18)
                                                       );or
                                                 (parse-integer parametervalue)
                                                 (progn
                                                  (terpri)
                                                  (format t "Warning:~%")
                                                  (format t "Invalid parameter: -ppaacheckminlimit <minimum amount of amino acids to check>~%~%")
                                                  (terpri)
                                                  (SB-EXT:EXIT)
                                                  )
                                                 )
                                               (progn
                                                (terpri)
                                                (format t "Warning:~%")
                                                (format t "Invalid parameter: -ppaacheckminlimit <minimum amount of amino acids to check>~%~%")
                                                (terpri)
                                                (SB-EXT:EXIT)
                                                )
                                               )
                                             ppaacheckminlimit
                                             )
                        ))
                 ((string= parametertag "-ppcn")
                  (setf ppcn "Y")
                  (unless (and (string= ppcomplete "N") (string= pplimit "N") (string= pptrim "N")
                               (string= ppthreshold "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N"))
                    (terpri)
                    (format t "Warning:~%")
                    (format t "The use of methods of prediction of ppi by phylogenetic profile is limited to only 1 among the 7 available methods")
                    (terpri)
                    (SB-EXT:EXIT)
                    ); unless or
                  )
                 ((string= parametertag "-ppcomplete")
                  (setf ppcomplete "Y")
                  (unless (and (string= ppcn "N") (string= pplimit "N") (string= pptrim "N")
                               (string= ppthreshold "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N"))
                    (terpri)
                    (format t "Warning:~%")
                    (format t "The use of methods of prediction of ppi by phylogenetic profile is limited to only 1 among the 7 available methods")
                    (terpri)
                    (SB-EXT:EXIT)
                    ); unless or
                  )
                 ((string= parametertag "-ppiterlimit")
                  (setf pplimit "Y")
                  (unless (and (string= ppcomplete "N") (string= ppcn "N") (string= pptrim "N")
                               (string= ppthreshold "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N"))
                    (terpri)
                    (format t "Warning:~%")
                    (format t "The use of methods of prediction of ppi by phylogenetic profile is limited to only 1 among the 7 available methods")
                    (terpri)
                    (SB-EXT:EXIT)
                    ); unless or
                  (setf ppiterlimit  (if parametervalue
                                       (if (char= (elt parametervalue 0) #\-)
                                         ppiterlimit
                                         (if (numberp (read-from-string parametervalue))
                                           (if (<= (parse-integer parametervalue) 0)
                                             (progn
                                              (terpri)
                                              (format t "Warning:~%")
                                              (format t "Invalid parameter: -ppiterlimit <maximum number of interactions desired>~%")
                                              (format t "The -ppiterlimit parameter must be greater than zero.")
                                              (terpri)
                                              (SB-EXT:EXIT)
                                              )
                                             (- (parse-integer parametervalue) 0)
                                             )
                                           (progn
                                            (terpri)
                                            (format t "Warning:~%")
                                            (format t "Invalid parameter: -ppiterlimit <maximum number of interactions desired>~%")
                                            (format t "Invalid character in the -ppiterlimit parameter.")
                                            (terpri)
                                            (SB-EXT:EXIT)
                                            )
                                           )
                                         );if
                                       ppiterlimit
                                       )
                        ))
                 ((string= parametertag "-trim")
                  (setf pptrim "Y")
                  (unless (and (string= ppcomplete "N") (string= pplimit "N") (string= ppcn "N")
                               (string= ppthreshold "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N"))
                    (terpri)
                    (format t "Warning:~%")
                    (format t "The use of methods of prediction of ppi by phylogenetic profile is limited to only 1 among the 7 available methods")
                    (terpri)
                    (SB-EXT:EXIT)
                    ); unless or

                  (setf trim  (if parametervalue
                                (if (char= (elt parametervalue 0) #\-)
                                  trim
                                  (if (numberp (read-from-string parametervalue))
                                    (if (<= (parse-integer parametervalue) 0)
                                      (progn
                                       (terpri)
                                       (format t "Warning:~%")
                                       (format t "Invalid parameter: -trim <maximum number of interactions by weight>~%")
                                       (format t "The -trim parameter must be greater than zero.")
                                       (terpri)
                                       (SB-EXT:EXIT)
                                       )
                                      (- (parse-integer parametervalue) 0)
                                      )
                                    (progn
                                     (terpri)
                                     (format t "Warning:~%")
                                     (format t "Invalid parameter: -trim <maximum number of interactions by weight>~%")
                                     (format t "Invalid character in the -trim parameter.")
                                     (terpri)
                              	      (SB-EXT:EXIT)
                                     )
                                    )
                                  );if
                                trim
                                )
                        ))
                 ((string= parametertag "-threshold")
                  (setf ppthreshold "Y")
                  (unless (and (string= ppcomplete "N") (string= pplimit "N") (string= pptrim "N")
                               (string= ppcn "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N"))
                    (terpri)
                    (format t "Warning:~%")
                    (format t "The use of methods of prediction of ppi by phylogenetic profile is limited to only 1 among the 7 available methods")
                    (terpri)
                    (SB-EXT:EXIT)
                    ); unless or
                  (setf threshold  (if parametervalue
                                     (if (numberp (read-from-string parametervalue))
                                       (if (<= (parse-integer parametervalue) 0)
                                         (progn
                                          (terpri)
                                          (format t "Warning:~%")
                                          (format t "Invalid parameter: -threshold <phylogenetic profiles threshold>~%")
                                          (format t "The -threshold parameter must be greater than zero.")
                                          (terpri)
                                          (SB-EXT:EXIT)
                                          )
                                         (- (parse-integer parametervalue) 0)
                                         )
                                       (progn
                                        (terpri)
                                        (format t "Warning:~%")
                                        (format t "Invalid parameter: -threshold <phylogenetic profiles threshold>~%")
                                        (format t "Invalid character in the -threshold parameter.")
                                        (terpri)
                                 	      (SB-EXT:EXIT)
                                        )
                                       )
                                     (progn
                                      (terpri)
                                      (format t "Warning:~%")
                                      (format t"When using the method 5, it is necessary to enter a value greater than zero through the -threshold parameter")
                                      (terpri)
                                      (SB-EXT:EXIT)
                                      )
                                     )
                        ))
                 ((string= parametertag "-plusminus")
                  (setf ppthreshold "Y")
                  (unless (and (string= ppcomplete "N") (string= pplimit "N") (string= pptrim "N")
                               (string= ppcn "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N"))
                    (terpri)
                    (format t "Warning:~%")
                    (format t "The use of methods of prediction of ppi by phylogenetic profile is limited to only 1 among the 7 available methods")
                    (terpri)
                    (SB-EXT:EXIT)
                    ); unless or
                  (setf plusminus  (if parametervalue
                                     (progn
                                      (unless (or (string= (string parametervalue) "<") (string= (string parametervalue) ">"))
                                        (progn
                                         (terpri)
                                         (format t "Warning:~%")
                                         (format t "Invalid parameter: -plusminus <parameter that receives the greater than or less than sign to apply the ppthreshod filter>~%")
                                         (format t "The -plusminus parameter can take '<' or '>'")
                                         (terpri)
                                         (SB-EXT:EXIT)
                                         );progn
                                        );unless
                                      (string parametervalue)
                                      );progn
                                     (progn
                                      (terpri)
                                      (format t "Warning:~%")
                                      (format t"When using the method 5, it is necessary to insert the less than (<) or greater than (>) sign through the -plusminus parameter (signs greater than and less than, must be enclosed in single or double quotes)")
                                      (terpri)
                                      (SB-EXT:EXIT)
                                      )
                                     )))
                 ((string= parametertag "-grouplimit")
                  (setf ppdeletegroup "Y")
                  (unless (and (string= ppcomplete "N") (string= pplimit "N") (string= pptrim "N")
                               (string= ppthreshold "N") (string= ppcn "N") (string= ppdeleteprofile "N"))
                    (terpri)
                    (format t "Warning:~%")
                    (format t "The use of methods of prediction of ppi by phylogenetic profile is limited to only 1 among the 7 available methods")
                    (terpri)
                    (SB-EXT:EXIT)
                    ); unless or
                  (setf grouplimit  (if parametervalue
                                      (if (char= (elt parametervalue 0) #\-)
                                        grouplimit
                                        (if (numberp (read-from-string parametervalue))
                                          (if (<= (parse-integer parametervalue) 0)
                                            (progn
                                             (terpri)
                                             (format t "Warning:~%")
                                             (format t "Invalid parameter: -grouplimit <limit of tolerated interactions to maintain a group of ppi with the same weight>~%")
                                             (format t "The -grouplimit parameter must be greater than zero.")
                                             (terpri)
                                             (SB-EXT:EXIT)
                                             )
                                            (- (parse-integer parametervalue) 0)
                                            )
                                          (progn
                                           (terpri)
                                           (format t "Warning:~%")
                                           (format t "Invalid parameter: -grouplimit <limit of tolerated interactions to maintain a group of ppi with the same weight>~%")
                                           (format t "Invalid character in the -grouplimit parameter.")
                                           (terpri)
                                    	      (SB-EXT:EXIT)
                                           )
                                          )
                                        );unless
                                      grouplimit
                                      )
                        ))
                 ((string= parametertag "-profiles")
                  (setf ppdeleteprofile "Y")
                  (unless (and (string= ppcomplete "N") (string= pplimit "N") (string= pptrim "N")
                               (string= ppthreshold "N") (string= ppdeletegroup "N") (string= ppcn "N"))
                    (terpri)
                    (format t "Warning:~%")
                    (format t "The use of methods of prediction of ppi by phylogenetic profile is limited to only 1 among the 7 available methods")
                    (terpri)
                    (SB-EXT:EXIT)
                    ); unless or
                  (setf profiles  (if parametervalue
                                    (progn
                                     (loop for c across parametervalue
                                       do (progn
                                           (unless (or (char= c #\;)
                                                       (char= c #\0) (char= c #\1) (char= c #\2) (char= c #\3) (char= c #\4)
                                                       (char= c #\5) (char= c #\6) (char= c #\7) (char= c #\8) (char= c #\9)
                                                       );or
                                             (progn
                                              (terpri)
                                              (format t "Warning:~%")
                                              (format t "Invalid parameter: -profiles <number of genomes in the unwanted profiles. Entry example: 7 (genes that co-occur in a total of 7 genomes will be excluded). To enter more than one profile, simply separate the input values with a semicolon. Example: 7; 15; 21>~%")
                                              (format t "Invalid character in the -profiles parameter.")
                                              (terpri)
                                              (SB-EXT:EXIT)
                                              )
                                             );unless or
                                           );progn
                                       );loop
                                     (string parametervalue)
                                     );progn
                                    (progn
                                     (terpri)
                                     (format t "Warning:~%")
                                     (format t (concatenate 'string "When using the method 7, it is necessary to enter a value greater than zero through the -profiles parameter. Entry example: 7 (genes that co-occur in a total of 7 genomes will be excluded). ""To insert more than one profile, the entry must be enclosed in single or double quotes, and the values separated by semicolons. Example: ""\"7; 15; 21\"~%~%"))
                                     (terpri)
                                     (SB-EXT:EXIT)
                                     )
                                    )))
                 ((string= parametertag "-cnp")
                  (setf percentage-cn  (if parametervalue
                                         (if (numberp (read-from-string parametervalue))
                                           (if (or (< (parse-integer parametervalue) 0) (> (parse-integer parametervalue) 100))
                                             (progn
                                              (terpri)
                                              (format t "Warning:~%")
                                              (format t "Invalid parameter: -cpp <conserved neighbourhood score percentage>~%")
                                              (format t "The prediction percentage by conserved neighbourhood can only take values from 0 to 100.")
                                              (terpri)
                                              (SB-EXT:EXIT)
                                              )
                                             (progn
                                              (setf aux-percentage-cn T)
                                              (/ (parse-integer parametervalue) 100)
                                              )
                                             )
                                           (progn
                                            (terpri)
                                            (format t "Warning:~%")
                                            (format t"Invalid parameter: -cnp <conserved neighbourhood score percentage>~%")
                                            (format t "The prediction percentage by conserved neighbourhood can only take values from 0 to 100.")
                                            (terpri)
                                     	      (SB-EXT:EXIT)
                                            )
                                           )
                                         percentage-cn)
                        ))
                 ((string= parametertag "-expt")
                  (if parametervalue
                    (if (numberp (read-from-string parametervalue))
                      (progn
                       (terpri)
                       (format t "Warning:~%")
                       (format t"Invalid parameter: -expt <type of expansion of the conserved gene neighborhood>~%")
                       (format t "The expansion of the conserved gene neighborhood can be 'fixed' or 'dynamic'")
                       (terpri)
                       (SB-EXT:EXIT)
                       )
                      (if (or (string= (string-downcase parametervalue) "fixed") (string= (string-downcase parametervalue) "dynamic"))
                        (setf expansion-type (string-downcase parametervalue))
                        (progn
                         (terpri)
                         (format t "Warning:~%")
                         (format t"Invalid parameter: -expt <type of expansion of the conserved gene neighborhood>~%")
                         (format t "The expansion of the conserved gene neighborhood can be 'fixed' or 'dynamic'")
                         (terpri)
                         (SB-EXT:EXIT)
                         )
                        )
                      )
                    (progn
                     (terpri)
                     (format t "Warning:~%")
                     (format t"Invalid parameter: -expt <type of expansion of the conserved gene neighborhood>~%")
                     (format t "The expansion of the conserved gene neighborhood can be 'fixed' or 'dynamic'")
                     (terpri)
                     (SB-EXT:EXIT)
                     )))
                 ((string= parametertag "-ws")
                  (setf window-size  (if parametervalue
                                       (if (numberp (read-from-string parametervalue))
                                         (if (<= (parse-integer parametervalue) 0)
                                           (progn
                                            (terpri)
                                            (format t "Warning:~%")
                                            (format t "The -ws parameter cannot be less than or equal to zero")
                                            (terpri)
                                            (SB-EXT:EXIT)
                                            )
                                           (parse-integer parametervalue)
                                           )
                                         (progn
                                          (terpri)
                                          (format t "Warning:~%")
                                          (format t "Invalid parameter: -ws <dynamic expansion window size>")
                                          (terpri)
                                   	      (SB-EXT:EXIT)
                                          )
                                         );if numberp
                                       (progn
                                        (terpri)
                                        (format t "Warning:~%")
                                        (format t"Invalid parameter: -ws <dynamic expansion window size>")
                                        (terpri)
                                        (SB-EXT:EXIT)
                                        )
                                       )
                        ))
                 ((string= parametertag "-w1")
                  (setf w1  (if parametervalue
                              (if (numberp (read-from-string parametervalue))
                                (if (<= (parse-integer parametervalue) 0)
                                  (progn
                                   (terpri)
                                   (format t "Warning:~%")
                                   (format t "The -w1 parameter cannot be less than or equal to zero")
                                   (terpri)
                                   (SB-EXT:EXIT)
                                   )
                                  (parse-integer parametervalue)
                                  )
                                (progn
                                 (terpri)
                                 (format t "Warning:~%")
                                 (format t"Invalid parameter: -w1 <window size 1>")
                                 (terpri)
                          	      (SB-EXT:EXIT)
                                 )
                                )
                              (progn
                               (terpri)
                               (format t "Warning:~%")
                               (format t"Invalid parameter: -w1 <window size 1>")
                               (terpri)
                               (SB-EXT:EXIT)
                               )
                              )
                        ))
                 ((string= parametertag "-cw1")
                  (setf cw1  (if parametervalue
                               (if (numberp (read-from-string parametervalue))
                                 (if (<= (parse-integer parametervalue) 0)
                                   (progn
                                    (terpri)
                                    (format t "Warning:~%")
                                    (format t "The -cw1 parameter cannot be less than or equal to zero")
                                    (terpri)
                                    (SB-EXT:EXIT)
                                    )
                                   (parse-integer parametervalue)
                                   )
                                 (progn
                                  (terpri)
                                  (format t "Warning:~%")
                                  (format t"Invalid parameter: -cw1 <gene conservation required for window 1>")
                                  (terpri)
                           	      (SB-EXT:EXIT)
                                  )
                                 )
                               (progn
                                (terpri)
                                (format t "Warning:~%")
                                (format t"Invalid parameter: -cw1 <gene conservation required for window 1>")
                                (terpri)
                                (SB-EXT:EXIT)
                                )
                               )
                        ))
                 ((string= parametertag "-w2")
                  (setf w2  (if parametervalue
                              (if (numberp (read-from-string parametervalue))
                                (if (<= (parse-integer parametervalue) 0)
                                  (progn
                                   (terpri)
                                   (format t "Warning:~%")
                                   (format t "The -w2 parameter cannot be less than or equal to zero")
                                   (terpri)
                                   (SB-EXT:EXIT)
                                   )
                                  (parse-integer parametervalue)
                                  )
                                (progn
                                 (terpri)
                                 (format t "Warning:~%")
                                 (format t"Invalid parameter: -w2 <window size 2>")
                                 (terpri)
                          	      (SB-EXT:EXIT)
                                 )
                                )
                              (progn
                               (terpri)
                               (format t "Warning:~%")
                               (format t"Invalid parameter: -w2 <window size 2>")
                               (terpri)
                               (SB-EXT:EXIT)
                               )
                              )
                        ))
                 ((string= parametertag "-cw2")
                  (setf cw2  (if parametervalue
                               (if (numberp (read-from-string parametervalue))
                                 (if (<= (parse-integer parametervalue) 0)
                                   (progn
                                    (terpri)
                                    (format t "Warning:~%")
                                    (format t "The -cw2 parameter cannot be less than or equal to zero")
                                    (terpri)
                                    (SB-EXT:EXIT)
                                    )
                                   (parse-integer parametervalue)
                                   )
                                 (progn
                                  (terpri)
                                  (format t "Warning:~%")
                                  (format t"Invalid parameter: -cw2 <gene conservation required for window 2>")
                                  (terpri)
                           	      (SB-EXT:EXIT)
                                  )
                                 )
                               (progn
                                (terpri)
                                (format t "Warning:~%")
                                (format t"Invalid parameter: -cw2 <gene conservation required for window 2>")
                                (terpri)
                                (SB-EXT:EXIT)
                                )
                               )
                        ))
                 ((string= parametertag "-w3")
                  (setf w3  (if parametervalue
                              (if (numberp (read-from-string parametervalue))
                                (if (<= (parse-integer parametervalue) 0)
                                  (progn
                                   (terpri)
                                   (format t "Warning:~%")
                                   (format t "The -w3 parameter cannot be less than or equal to zero")
                                   (terpri)
                                   (SB-EXT:EXIT)
                                   )
                                  (parse-integer parametervalue)
                                  )
                                (progn
                                 (terpri)
                                 (format t "Warning:~%")
                                 (format t"Invalid parameter: -w3 <window size 3>")
                                 (terpri)
                          	      (SB-EXT:EXIT)
                                 )
                                )
                              (progn
                               (terpri)
                               (format t "Warning:~%")
                               (format t"Invalid parameter: -w3 <window size 3>")
                               (terpri)
                               (SB-EXT:EXIT)
                               )
                              )
                        ))
                 ((string= parametertag "-cw3")
                  (setf cw3  (if parametervalue
                               (if (numberp (read-from-string parametervalue))
                                 (if (<= (parse-integer parametervalue) 0)
                                   (progn
                                    (terpri)
                                    (format t "Warning:~%")
                                    (format t "The -cw3 parameter cannot be less than or equal to zero")
                                    (terpri)
                                    (SB-EXT:EXIT)
                                    )
                                   (parse-integer parametervalue)
                                   )
                                 (progn
                                  (terpri)
                                  (format t "Warning:~%")
                                  (format t"Invalid parameter: -cw3 <gene conservation required for window 3>")
                                  (terpri)
                           	      (SB-EXT:EXIT)
                                  )
                                 )
                               (progn
                                (terpri)
                                (format t "Warning:~%")
                                (format t"Invalid parameter: -cw3 <gene conservation required for window 3>")
                                (terpri)
                                (SB-EXT:EXIT)
                                )
                               )
                        ))
                 ((string= parametertag "-w4")
                  (setf w4  (if parametervalue
                              (if (numberp (read-from-string parametervalue))
                                (if (<= (parse-integer parametervalue) 0)
                                  (progn
                                   (terpri)
                                   (format t "Warning:~%")
                                   (format t "The -w4 parameter cannot be less than or equal to zero")
                                   (terpri)
                                   (SB-EXT:EXIT)
                                   )
                                  (parse-integer parametervalue)
                                  )
                                (progn
                                 (terpri)
                                 (format t "Warning:~%")
                                 (format t"Invalid parameter: -w4 <window size 4>")
                                 (terpri)
                          	      (SB-EXT:EXIT)
                                 )
                                )
                              (progn
                               (terpri)
                               (format t "Warning:~%")
                               (format t"Invalid parameter: -w4 <window size 4>")
                               (terpri)
                               (SB-EXT:EXIT)
                               )
                              )
                        ))
                 ((string= parametertag "-cw4")
                  (setf cw4  (if parametervalue
                               (if (numberp (read-from-string parametervalue))
                                 (if (<= (parse-integer parametervalue) 0)
                                   (progn
                                    (terpri)
                                    (format t "Warning:~%")
                                    (format t "The -cw4 parameter cannot be less than or equal to zero")
                                    (terpri)
                                    (SB-EXT:EXIT)
                                    )
                                   (parse-integer parametervalue)
                                   )
                                 (progn
                                  (terpri)
                                  (format t "Warning:~%")
                                  (format t"Invalid parameter: -cw4 <gene conservation required for window 4>")
                                  (terpri)
                           	      (SB-EXT:EXIT)
                                  )
                                 )
                               (progn
                                (terpri)
                                (format t "Warning:~%")
                                (format t"Invalid parameter: -cw4 <gene conservation required for window 4>")
                                (terpri)
                                (SB-EXT:EXIT)
                                )
                               )
                        ))
                 ((string= parametertag "-aadifflimit")
                  (setf aadifflimit  (if parametervalue
                                       (if (numberp (read-from-string parametervalue))
                                         (if (or (= (parse-integer parametervalue) 0) (= (parse-integer parametervalue) 1))
                                           (parse-integer parametervalue)
                                           (progn
                                            (terpri)
                                            (format t "Warning:~%")
                                            (format t "Invalid parameter: -aadifflimit <amino acid difference limit>~%~%")
                                            (format t "Possible configurations of the histogram parameters:~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "-aadifflimit   -aacheckminlimit   minimal protein identity %~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               26                     100%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               25                     100%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               24                    97,96%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               23                    96,94%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               22                    96,94%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               21                    94,68%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               20                    91,75%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               19                    85,57%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               18                    50,00%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      1               26                    97,87%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      1               25                    92,55%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (terpri)
                                            (SB-EXT:EXIT)
                                            )
                                           )
                                         (progn
                                          (terpri)
                                          (format t "Warning:~%")
                                          (format t "Invalid parameter: -aadifflimit <amino acid difference limit>~%~%")
                                          (format t "Possible configurations of the histogram parameters:~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "-aadifflimit   -aacheckminlimit   minimal protein identity %~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      0               26                     100%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      0               25                     100%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      0               24                    97,96%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      0               23                    96,94%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      0               22                    96,94%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      0               21                    94,68%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      0               20                    91,75%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      0               19                    85,57%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      0               18                    50,00%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      1               26                    97,87%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (format t "      1               25                    92,55%~%")
                                          (format t "-------------------------------------------------------------~%")
                                          (terpri)
                                          (SB-EXT:EXIT)
                                          )
                                         )
                                       (progn
                                        (terpri)
                                        (format t "Warning:~%")
                                        (format t "Invalid parameter: -aadifflimit <amino acid difference limit>~%~%")
                                        (format t "Possible configurations of the histogram parameters:~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "-aadifflimit   -aacheckminlimit   minimal protein identity %~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      0               26                     100%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      0               25                     100%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      0               24                    97,96%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      0               23                    96,94%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      0               22                    96,94%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      0               21                    94,68%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      0               20                    91,75%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      0               19                    85,57%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      0               18                    50,00%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      1               26                    97,87%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (format t "      1               25                    92,55%~%")
                                        (format t "-------------------------------------------------------------~%")
                                        (terpri)
                                        (SB-EXT:EXIT)
                                        )
                                       )
                        ))
                 ((string= parametertag "-aacheckminlimit")
                  (setf aacheckminlimit  (if parametervalue
                                           (if (numberp (read-from-string parametervalue))
                                             (if (or (= (parse-integer parametervalue) 26) (= (parse-integer parametervalue) 25)
                                                     (= (parse-integer parametervalue) 24) (= (parse-integer parametervalue) 23)
                                                     (= (parse-integer parametervalue) 22) (= (parse-integer parametervalue) 21)
                                                     (= (parse-integer parametervalue) 20) (= (parse-integer parametervalue) 19)
                                                     (= (parse-integer parametervalue) 18)
                                                     );or
                                               (parse-integer parametervalue)
                                               (progn
                                                (terpri)
                                                (format t "Warning:~%")
                                                (format t "Invalid parameter: -aacheckminlimit <minimum amount of amino acids to check>~%~%")
                                                (format t "Possible configurations of the histogram parameters:~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "-aadifflimit   -aacheckminlimit   minimal protein identity %~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      0               26                     100%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      0               25                     100%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      0               24                    97,96%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      0               23                    96,94%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      0               22                    96,94%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      0               21                    94,68%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      0               20                    91,75%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      0               19                    85,57%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      0               18                    50,00%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      1               26                    97,87%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (format t "      1               25                    92,55%~%")
                                                (format t "-------------------------------------------------------------~%")
                                                (terpri)
                                                (SB-EXT:EXIT)
                                                )
                                               )
                                             (progn
                                              (terpri)
                                              (format t "Warning:~%")
                                              (format t "Invalid parameter: -aacheckminlimit <minimum amount of amino acids to check>~%~%")
                                              (format t "Possible configurations of the histogram parameters:~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "-aadifflimit   -aacheckminlimit   minimal protein identity %~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      0               26                     100%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      0               25                     100%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      0               24                    97,96%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      0               23                    96,94%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      0               22                    96,94%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      0               21                    94,68%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      0               20                    91,75%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      0               19                    85,57%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      0               18                    50,00%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      1               26                    97,87%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (format t "      1               25                    92,55%~%")
                                              (format t "-------------------------------------------------------------~%")
                                              (terpri)
                                              (SB-EXT:EXIT)
                                              )
                                             )
                                           (progn
                                            (terpri)
                                            (format t "Warning:~%")
                                            (format t "Invalid parameter: -aadifflimit <amino acid difference limit>~%~%")
                                            (format t "Possible configurations of the histogram parameters:~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "-aadifflimit   -aacheckminlimit   minimal protein identity %~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               26                     100%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               25                     100%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               24                    97,96%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               23                    96,94%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               22                    96,94%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               21                    94,68%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               20                    91,75%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               19                    85,57%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      0               18                    50,00%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      1               26                    97,87%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (format t "      1               25                    92,55%~%")
                                            (format t "-------------------------------------------------------------~%")
                                            (terpri)
                                            (SB-EXT:EXIT)
                                            )
                                           )
                        ))
                 );cond
        	      );unless

             (if (or (string= parametertag "-pphistofilter") (string= parametertag "-ppcn")
                     (string= parametertag "-ppcomplete") (string= parametertag "-help")
                     (string= parametertag "-genefusion")
                     )
               (setf parameter (+ parameter 1))
               #|(if (char= (elt parametervalue 0) #\-)
                   (setf parameter (+ parameter 1))
                   (setf parameter (+ parameter 2))
                   )|#
               (if (and (string= parametertag "-dir") parametervalue)
                 (if (eql (pathname-directory (pathname parametervalue)) nil)
                   (if (char= (elt parametervalue 0) #\-)
                     (setf parameter (+ parameter 1))
                     (setf parameter (+ parameter 2))
                     )
                   (setf parameter (+ parameter 2))
                   )
                 (if (and (or (string= parametertag "-ppiterlimit") (string= parametertag "-trim")
                              (string= parametertag "-grouplimit")
                              )
                          parametervalue)
                   (if (char= (elt parametervalue 0) #\-)
                     (setf parameter (+ parameter 1))
                     (setf parameter (+ parameter 2))
                     )
                   (setf parameter (+ parameter 2))
                   );if or
                 ); if and
               );if or
          	  );dotimes

    (unless (eql help nil)
      (progn
       (terpri)
       (format t "Mandatory parameter~%~%")
       (format t "Directory parameter:~%")
       (format t "-dir <workingdir> directory~%~%")
       (format t "Optional parameters~%~%")
       (format t "Parameters of ppi by conserved neighbourhood:~%")
       (format t "-cnp <conserved neighbourhood score percentage> (65-default)~%")
       (format t "-expt <type of expansion of the conserved gene neighborhood> 'fixed' or 'dynamic' (fixed-default)~%~%")
       (format t "Parameters of conserved neighbourhood by fixed expansion:~%")
       (format t "-w1 <window size 1> (10-default)~%")
       (format t "-cw1 <gene conservation required for window 1> (4-default)~%")
       (format t "-w2 <window size 2> (7-default)~%")
       (format t "-cw2 <gene conservation required for window 2> (3-default)~%")
       (format t "-w3 <window size 3> (5-default)~%")
       (format t "-cw3 <gene conservation required for window 3> (2-default)~%")
       (format t "-w4 <window size 4> (3-default)~%")
       (format t "-cw4 <gene conservation required for window 4> (1-default)~%~%")
       (format t "Parameters of conserved neighbourhood by dynamic expansion:~%")
       (format t "-ws <dynamic expansion window size> (1-default)~%~%")
       (format t "Parameters of ppi by phylogenetic profile:~%")
       (format t "-ppp <phylogenetic profiles score percentage> (30-default)~%")
       (format t "-ppdifftolerated <difference in phylogenetic profiles tolerated to infer ppi> (0-default)~%~%")
       (format t "Amino acid histogram parameter settings for the phylogenetic profile:~%")
       (format t "-pphistofilter <build the phylogenetic profile of genes with a higher percentage of identity>~%")
       (format t "-ppaadifflimit <amino acid difference limit> (0-default)~%")
       (format t "-ppaacheckminlimit <minimum amount of amino acids to check> (26-default)~%~%")
       (format t "Methods of ppi prediction by phylogenetic profile and its parameters:~%~%")
       (format t "Method 1 - Predict ppi by phylogenetic profile only for interactions predicted by conserved neighborhood~%")
       (format t "-ppcn~%~%")
       (format t "Method 2 - PPi prediction by phylogenetic profile without filters 2~%")
       (format t "-ppcomplete~%~%")
       (format t "Method 3 - Prediction of ppi by phylogenetic profile with a limit of interactions~%")
       (format t "-ppiterlimit <maximum number of interactions desired> (500000-default)~%~%")
       (format t "Method 4 - Prediction of ppi by phylogenetic profile with interactions limit by weight~%")
       (format t "-trim <maximum number of interactions by weight> (45000-default)~%~%")
       (format t "Method 5 - Prediction of ppi by the phylogenetic profile only for genes with profiles that cover a greater or lesser number of genomes than an informed threshold~%")
       (format t "-threshold <phylogenetic profiles threshold>~%")
       (format t "-plusminus <parameter that receives the greater than or less than sign to apply the ppthreshod filter> '<' or '>' (signs greater than and less than, must be enclosed in single or double quotes)~%~%")
       (format t "Method 6 - Delete groups of ppi predicted by phylogenetic profile that exceed a limit of interactions by weight~%")
       (format t "-grouplimit <limit of tolerated interactions to maintain a group of ppi with the same weight> (45000-default)~%~%")
       (format t "Method 7 - To exclude genes with unwanted profiles in predicting ppi by phylogenetic profile~%")
       (format t (concatenate 'string "-profiles <number of genomes in the unwanted profiles> Entry example: 7 (genes that co-occur in a total of 7 genomes will be excluded). To insert more than one profile, the entry must be enclosed in single or double quotes, and the values separated by semicolons. Example: ""\"7; 15; 21\"~%~%"))
       (format t "Parameters of ppi by gene fusion:~%")
       (format t "-genefusion <make ppi predictions by gene fusion>~%")
       (format t "-gfp <gene fusion score percentage> (5-default)~%")
       (format t "Note: if gene fusion is included, there will be an increase of more than 100% in the execution time~%~%")
       (format t "Amino acid histogram parameters:~%")
       (format t "-aadifflimit <amino acid difference limit> (1-default)~%")
       (format t "-aacheckminlimit <minimum amount of amino acids to check> (25-default)~%~%")
       (format t "Possible configurations of the histogram parameters:~%")
       (format t "-------------------------------------------------------------~%")
       (format t "-aadifflimit   -aacheckminlimit   minimal protein identity %~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      0               26                     100%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      0               25                     100%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      0               24                    97,96%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      0               23                    96,94%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      0               22                    96,94%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      0               21                    94,68%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      0               20                    91,75%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      0               19                    85,57%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      0               18                    50,00%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      1               26                    97,87%~%")
       (format t "-------------------------------------------------------------~%")
       (format t "      1               25                    92,55%~%")
       (format t "-------------------------------------------------------------~%")
       (terpri)
       (SB-EXT:EXIT)
       )
      )

    ;Se faltou o parâmetro do arquivo de dados principal ou o diretório de trabalho então gere um erro e aborte a execução do programa
   	(if (not workingdir)
 	    (progn
       (terpri)
       (format t "Warning:~%")
 	     (format t"There are missing parameters: -dir <workingdir>")
       (terpri)
	      (SB-EXT:EXIT)
	      )
 	    )

   	;Se existe algum problema de acesso ao diretório de dados principal então aborte e gere uma mensagem de erro
   	(if (not (probe-file workingdir))
 	    (progn
       (terpri)
       (format t "Warning:~%")
 	     (format t "~%~a~%" "Working directory is missing. Assuming current path")
	      ;(setf workingdir "./")
	      )
      (progn
       (dolist (path (list-directory workingdir))

               (if (eql (pathname-type (pathname path)) nil)
                 ;;Se o caminho representa um diretório, incremento o numero de pastas no diretorio.
                 (setf directory-num (incf directory-num))
                 ;;Se o caminho não for de uma pasta, faça:
                 (progn
                  (if (or  (equalp (pathname-type (pathname path)) "fasta")
                           (equalp (pathname-type (pathname path)) "fa")
                           (equalp (pathname-type (pathname path)) "faa")
                           );or
                    (incf qtd-arquivos-fasta)
                    );if or
                  );progn
                 );if
               );dolist
       (if (or (= directory-num (length (list-directory workingdir))) (= qtd-arquivos-fasta 0))
         ;;Se há somente pastas no diretório especificadado, faça:
         (progn
          (terpri)
          (format t "Warning:~%")
          (format t "There are no valid files in this directory.")
          (terpri)
          (format t "Protein sequence files must be in fasta format (.fasta, .faa or .fa).")
          (terpri)
          (SB-EXT:EXIT)
          )
         (progn
          (if (or (string= ppcn "Y") (string= ppcomplete "Y") (string= pplimit "Y") (string= pptrim "Y")
                  (string= ppthreshold "Y") (string= ppdeletegroup "Y") (string= ppdeleteprofile "Y")
                  );or
            (progn
             (cond
               ((string= ppthreshold "Y")
                (unless (> threshold 0)
                  (terpri)
                  (format t "Warning:~%")
                  (format t"When using the method 5, it is necessary to enter a value greater than zero through the -threshold parameter")
                  (terpri)
                  (SB-EXT:EXIT)
                  );unless threshold
                (unless (not (string= plusminus "nil"))
                  (terpri)
                  (format t "Warning:~%")
                  (format t"When using the method 5, it is necessary to insert the less than (<) or greater than (>) sign through the -plusminus parameter (signs greater than and less than, must be enclosed in single or double quotes)")
                  (terpri)
                  (SB-EXT:EXIT)
                  );unless plusminus
                )
               ((string= ppdeleteprofile "Y")
                (unless (not (string= profiles "0"))
                  (terpri)
                  (format t "Warning:~%")
                  (format t (concatenate 'string "When using the method 7, it is necessary to enter a value greater than zero through the -profiles parameter. Entry example: 7 (genes that co-occur in a total of 7 genomes will be excluded). ""To insert more than one profile, the entry must be enclosed in single or double quotes, and the values separated by semicolons. Example: ""\"7; 15; 21\"~%~%"))
                  (terpri)
                  (SB-EXT:EXIT)
                  );unless
                )
               );cond
             );progn
            );if or

          (terpri)
          (format t "GENPPI VERSION: 1.0~%")
          (format t "RELEASE NUMBER: 4354980a4dfaede2695e82e525914cead0f2c9ee~%")
          (format t "REPOSITORY: https://github.com/santosardr/genppi ~%~%")
          (format t "Directory parameter:~%")
          (format t "-dir <workingdir> = ~a~%~%" workingdir)
          (format t "Parameters of ppi by conserved neighbourhood:~%")

          (cond
            ((and (string= gene-fusion "Y") (eql aux-percentage-cn nil))
             (format t "-cnp <conserved neighbourhood score percentage> = 65% (default)~%")
             )
            ((and (string= gene-fusion "Y") (not (eql aux-percentage-cn nil)))
             (format t "-cnp <conserved neighbourhood score percentage> = ~a%~%" (* percentage-cn 100))
             )
            ((and (string= gene-fusion "N")
                  (eql aux-percentage-cn nil)
                  (or (string= ppcn "Y") (string= ppcomplete "Y") (string= pplimit "Y") (string= pptrim "Y")
                      (string= ppthreshold "Y") (string= ppdeletegroup "Y") (string= ppdeleteprofile "Y")
                      );or
                  (> qtd-arquivos-fasta 1)
                  );and
                   (format t "-cnp <conserved neighbourhood score percentage> = 70%~%")
                   (setf percentage-cn (/ 70 100))
                   )
            ((and (string= gene-fusion "N")
                  (eql aux-percentage-cn nil)
                  (or (string= ppcn "Y") (string= ppcomplete "Y") (string= pplimit "Y") (string= pptrim "Y")
                      (string= ppthreshold "Y") (string= ppdeletegroup "Y") (string= ppdeleteprofile "Y")
                      );or
                  (= qtd-arquivos-fasta 1)
                  );and
                   (format t "-cnp <conserved neighbourhood score percentage> = 100%~%")
                   )
            ((and (string= gene-fusion "N") (not (eql aux-percentage-cn nil)))
             (format t "-cnp <conserved neighbourhood score percentage> = ~a%~%" (* percentage-cn 100))
             )
            ((and (string= gene-fusion "N")
                  (and (string= ppcn "N") (string= ppcomplete "N") (string= pplimit "N") (string= pptrim "N")
                       (string= ppthreshold "N") (string= ppdeletegroup "N") (string= ppdeleteprofile "N")
                       );and
                  );and
                   (format t "-cnp <conserved neighbourhood score percentage> = 100%~%")
                   )
            );cond

          (if (string= expansion-type "fixed")
            (progn
             (format t "-expt <type of expansion of the conserved gene neighborhood> = ~a~%" "fixed (default)")
             (format t "-w1 <window size 1> = ~a~%" (if (= w1 10) (string "10 (default)") (- w1 0)))
             (format t "-cw1 <gene conservation required for window 1> = ~a~%" (if (= cw1 4) (string "4 (default)") (- cw1 0)))
             (format t "-w2 <window size 2> = ~a~%" (if (= w2 7) (string "7 (default)") (- w2 0)))
             (format t "-cw2 <gene conservation required for window 2> = ~a~%" (if (= cw2 3) (string "3 (default)") (- cw2 0)))
             (format t "-w3 <window size 3> = ~a~%" (if (= w3 5) (string "5 (default)") (- w3 0)))
             (format t "-cw3 <gene conservation required for window 3> = ~a~%" (if (= cw3 2) (string "2 (default)") (- cw3 0)))
             (format t "-w4 <window size 4> = ~a~%" (if (= w4 3) (string "3 (default)") (- w4 0)))
             (format t "-cw4 <gene conservation required for window 4> = ~a~%~%" (if (= cw4 1) (string "1 (default)") (- cw4 0)))

             (if (> w2 w1)
               (progn
                (terpri)
                (format t "Warning:~%")
                (format t"The expansion window size w2 cannot be larger than the maximum expansion (w1)")
                (terpri)
                (SB-EXT:EXIT)
                );progn
               );if
             (if (> w3 w1)
               (progn
                (terpri)
                (format t "Warning:~%")
                (format t"The expansion window size w3 cannot be larger than the maximum expansion (w1)")
                (terpri)
                (SB-EXT:EXIT)
                );progn
               );if
             (if (> w4 w1)
               (progn
                (terpri)
                (format t "Warning:~%")
                (format t"The expansion window size w4 cannot be larger than the maximum expansion (w1)")
                (terpri)
                (SB-EXT:EXIT)
                );progn
               );if
             (if (> cw1 w1)
               (progn
                (terpri)
                (format t "Warning:~%")
                (format t"The cw1 parameter cannot be larger than the window size w1")
                (terpri)
                (SB-EXT:EXIT)
                );progn
               );if
             (if (> cw2 w2)
               (progn
                (terpri)
                (format t "Warning:~%")
                (format t"The cw2 parameter cannot be larger than the window size w2")
                (terpri)
                (SB-EXT:EXIT)
                );progn
               );if
             (if (> cw3 w3)
               (progn
                (terpri)
                (format t "Warning:~%")
                (format t"The cw3 parameter cannot be larger than the window size w3")
                (terpri)
                (SB-EXT:EXIT)
                );progn
               );if
             (if (> cw4 w4)
               (progn
                (terpri)
                (format t "Warning:~%")
                (format t"The cw4 parameter cannot be larger than the window size w4")
                (terpri)
                (SB-EXT:EXIT)
                );progn
               );if

             );Prong
            (progn
             (format t "-expt <type of expansion of the conserved gene neighborhood> = ~a~%" expansion-type)
             (format t "-ws <dynamic expansion window size> = ~a~%~%" (if (= window-size 1)(string "1 (default)")(- window-size 0)))
             )
            );if

          (if (or (string= ppcn "Y") (string= ppcomplete "Y") (string= pplimit "Y") (string= pptrim "Y")
                  (string= ppthreshold "Y") (string= ppdeletegroup "Y") (string= ppdeleteprofile "Y")
                  );or
            (progn
             (cond
               ((string= ppcn "Y")
                (format t "Parameters of ppi by phylogenetic profile:~%")
                (format t "Method 1 - Predict ppi by phylogenetic profile only for interactions predicted by conserved neighborhood~%")
                )
               ((string= ppcomplete "Y")
                (format t "Parameters of ppi by phylogenetic profile:~%")
                (format t "Method 2 - PPi prediction by phylogenetic profile without filters~%")
                )
               ((string= pplimit "Y")
                (format t "Parameters of ppi by phylogenetic profile:~%")
                (format t "Method 3 - Prediction of ppi by phylogenetic profile with a limit of interactions~%")
                (if (= ppiterlimit 500000)
                  (format t "-ppiterlimit <maximum number of interactions desired> = 500000 (default)~%")
                  (format t "-ppiterlimit <maximum number of interactions desired> = ~a~%"ppiterlimit)
                  )
                )
               ((string= pptrim "Y")
                (format t "Parameters of ppi by phylogenetic profile:~%")
                (format t "Method 4 - Prediction of ppi by phylogenetic profile with interactions limit by weight~%")
                (if (= trim 45000)
                  (format t "-trim <maximum number of interactions by weight> = 45000 (default)~%")
                  (format t "-trim <maximum number of interactions by weight> = ~a~%"trim)
                  )
                )
               ((string= ppthreshold "Y")
                (format t "Parameters of ppi by phylogenetic profile:~%")
                (format t "Method 5 - Prediction of ppi by the phylogenetic profile only for genes with profiles that cover a greater or lesser number of genomes than an informed threshold~%")
                (format t "-threshold <phylogenetic profiles threshold> = ~a~%"threshold)
                (format t (concatenate 'string "-plusminus <only genes that occurred in " (if (string= plusminus "<") (string "less ") (string "more ")) "than ~a genomes will be considered in the analysis>~%") threshold)
                )
               ((string= ppdeletegroup "Y")
                (format t "Parameters of ppi by phylogenetic profile:~%")
                (format t "Method 6 - Delete groups of ppi predicted by phylogenetic profile that exceed a limit of interactions by weight~%")
                (if (= grouplimit 45000)
                  (format t "-grouplimit <limit of tolerated interactions to maintain a group of ppi with the same weight> = 45000 (default)~%")
                  (format t "-grouplimit <limit of tolerated interactions to maintain a group of ppi with the same weight> = ~a~%"grouplimit)
                  )
                )
               ((string= ppdeleteprofile "Y")
                (format t "Parameters of ppi by phylogenetic profile:~%")
                (format t "Method 7 - To exclude genes with unwanted profiles in predicting ppi by phylogenetic profile~%")
                (format t "-profiles <number of genomes in the unwanted profiles> Entry = ~a (genes that co-occur in a total of ~a genomes will be excluded)~%" profiles profiles)
                )
               );cond

             (cond
               ((eql aux-percentage-pp nil)
                (format t "-ppp <phylogenetic profiles score percentage> = 30% (default)~%")
                )
               ((not (eql aux-percentage-pp nil))
                (format t "-ppp <phylogenetic profiles score percentage> = ~a%~%" (* percentage-pp 100))
                )
               );cond

             (if (= ppdifftolerated 0)
               (format t "-ppdifftolerated <difference in phylogenetic profiles tolerated to infer ppi> = 0 (default)~%")
               (format t "-ppdifftolerated <difference in phylogenetic profiles tolerated to infer ppi> = ~a~%" ppdifftolerated)
               )
             (if (string= pphistofilter "Y")
               (progn
                (format t "-pphistofilter <build the phylogenetic profile of genes with a higher percentage of identity>~%")
                (if (= ppaadifflimit 0)
                  (format t "-ppaadifflimit <amino acid difference limit> = 0 (default)~%")
                  (format t "-ppaadifflimit <amino acid difference limit> = ~a~%" ppaadifflimit)
                  )
                (if (= ppaacheckminlimit 26)
                  (format t "-ppaacheckminlimit <minimum amount of amino acids to check> = 26 (default)~%~%")
                  (format t "-ppaacheckminlimit <minimum amount of amino acids to check> = ~a~%~%" ppaacheckminlimit)
                  )
                );progn
               (format t "~%")
               );if pphistofilter
             );progn
            );if

          (cond
            ((and (string= gene-fusion "Y") (= (float percentage-gf) 0.05))
             (format t "Parameters of ppi by gene fusion:~%")
             (format t "-gfp <gene fusion score percentage> = 5% (default)~%~%")
             )
            ((and (string= gene-fusion "Y") (not (= (float percentage-gf) 0.05)))
             (format t "Parameters of ppi by gene fusion:~%")
             (format t "-gfp <gene fusion score percentage> = ~a~%~%" (* percentage-gf 100))
             )
            );cond

          (format t "Amino acid histogram parameters:~%")
          (if (= aadifflimit 1)
            (format t "-aadifflimit <amino acid difference limit> 1 (default)~%")
            (format t "-aadifflimit <amino acid difference limit> ~a~%" aadifflimit)
            )
          (if (= aacheckminlimit 25)
            (format t "-aacheckminlimit <minimum amount of amino acids to check> 25 (default)~%")
            (format t "-aacheckminlimit <minimum amount of amino acids to check> ~a~%" aacheckminlimit)
            )
          (terpri)
          (terpri)
          (format t "Making ppi prediction, please wait.~%")
          (terpri)
          (genppi
           workingdir
           gene-fusion
           (float percentage-gf)
           (float percentage-pp)
           ppdifftolerated
           pphistofilter
           ppaadifflimit
           ppaacheckminlimit
           ppcn
           ppcomplete
           pplimit
           ppiterlimit
           pptrim
           trim
           ppthreshold
           threshold
           plusminus
           ppdeletegroup
           grouplimit
           ppdeleteprofile
           profiles
           expansion-type
           window-size
           (float percentage-cn)
           w1
           cw1
           w2
           cw2
           w3
           cw3
           w4
           cw4
           aadifflimit
           aacheckminlimit
           );parameters genppi
          (terpri)
          );progn
         );if
       );progn
 	    );if
   	);let
  );defun
;;------------------------------------------------------------------------------
