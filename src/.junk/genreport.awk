head -n 20 report.txt | awk '/^Profile/{for(i=1;i<=NF;i++){print i "\t" $i}}'
1	Profile
2	1
3	=>
4	Total
5	genes:
6	1
7	|
8	Total
9	ppi:
10	0
11	|
12	Weight:
13	0.000
14	|
15	Number
16	of
17	genomes:
18	14;

head -n 20 report.txt | awk '/^Genome:/{genome=$2} /^Profile/{print genome "\t" $2 "\t" $6 "\t" $6 "\t" $10 "\t" substr($18,1,length($18)-1)}'
CpFt2193-67-STRING	1	1	1	0	14
CpFt2193-67-STRING	2	1	1	0	8
CpFt2193-67-STRING	3	1	1	0	8
CpFt2193-67-STRING	4	1	1	0	5
CpFt2193-67-STRING	5	1	1	0	8
CpFt2193-67-STRING	6	1	1	0	12

echo -e "Genome\tProfile\tGenes\tInteractions\tGenomes" > pp.tab; awk '/^Genome:/{genome=$2} /^Profile/{print genome "\t" $2 "\t" $6 "\t" $10 "\t" substr($18,1,length($18)-1)}' report.txt >> pp.tab

echo -e "Genome\tGenes\tInteractions\tGenomes" > pp.tab; awk '/^Genome:/{genome=$2} /^Profile/{print genome "\t" $6 "\t" $10 "\t" substr($18,1,length($18)-1)}' report.txt >> pp.tab

#FIGURA 1: phylogenetic-profiles-report
echo "Genome,Genes,Interactions,Genomes" > pp.csv; awk '/^Genome:/{genome=$2} /^Profile/{print genome "," $6 "," $10 "," substr($18,1,length($18)-1)}' report.txt >> pp.csv

data=read.csv("pp.csv",header=TRUE,stringsAsFactors=FALSE, colClasses =c("character", "integer", "integer", "integer"));
X11();x=log(data[data$Genome=="CpFt2193-67-STRING",2]);hist(x,main="Histograma de Qte de Genes por Perfil Filogenético",xlab="log(Num Genes)", ylab="Qte proteínas");
X11();x=data[data$Genome=="CpFt2193-67-STRING",3];hist(log(x[x>0]),main="Histograma de Interações Protéicas",xlab="log(Num Interações)", ylab="Qte proteínas");
X11();boxplot(data[data$Genome=="CpFt2193-67-STRING",4])

#BOXPLOT FIGURA 1 phylogenetic-profiles-report
library(ggplot2)
data=read.csv("pp.csv",header=TRUE,stringsAsFactors=FALSE, colClasses =c("character", "integer", "integer", "integer"));
p<-ggplot(data,aes(x=Genome, y=Genomes))+geom_boxplot(varwidth = TRUE); p+ theme(axis.text.x = element_text(angle = 90, hjust = 1));
ggsave("BoxPlot-PP-by-Genomes.png", units="cm", width=35, height=20, dpi=900)

#HISTOGRAMA
#FIGURA 4 : phylogenetic-profiles-report
awk '/^Genome:/{print $2} /^Profile/{print genome $6 }' report.txt > ppgene.csv
../../../../src/verticalsplit.bash
paste column0* > data.csv

library(ggplot2)
library(dplyr)
library(hrbrthemes)
library(tidyverse)
library(hrbrthemes)
library(viridis)
library(forcats)

data <- read.table("data.csv", header=TRUE, sep="\t")

data <- data %>%
  gather(key="text", value="value") %>%
  mutate(text = gsub("\\.", " ",text)) %>%
#  mutate(value = log(round(as.numeric(value),0)))
  mutate(value = log(as.numeric(value)))
#  mutate(value = as.numeric(value))
data<-subset(data, value>1)

# plot
p <- data %>%
  mutate(text = fct_reorder(text, value)) %>%
  ggplot( aes(x=value, color=text, fill=text)) +
    geom_histogram(alpha=0.6, binwidth = 0.5) +
    scale_fill_viridis(discrete=TRUE) +
    scale_color_viridis(discrete=TRUE) +
    theme_ipsum() +
    theme(
      legend.position="none",
      panel.spacing = unit(0.1, "lines"),
      strip.text.x = element_text(size = 8)
    ) +
    xlab("") +
    ylab("Quantidade de Genes > 1 de Perfis Filogenéticos por Genoma") +
    facet_wrap(~text)

    ggsave("Qte genes maior que 1 por Perfil Filogenético.png", units="cm", width=35, height=20, dpi=900)

#Utilizando o relatório ordinário como fonte de dados para boxplot
#FIGURA 2 e 5: ppi-report
echo "Genome,CN,PP" > data.csv
awk -F ':' '/^Genome/{genome=$2} /^Number of ppi by cn/{cn=$2} /^Number of ppi by pp/{print genome "," cn "," $2 }' report.txt >> data.csv
sed -i "s/[ ]\+//g" data.csv
library(tidyr)
library(ggplot2)
data=read.csv("data.csv",header=TRUE,stringsAsFactors=FALSE, colClasses =c("character", "integer", "integer"));
df <- gather(data, event, total, CN:PP)
plot <- ggplot(df, aes(reorder(Genome,total), total, fill=event)); 
plot <- plot + geom_bar(stat = "identity", position = 'dodge') + theme(axis.text.x = element_text(angle = 90, hjust = 1)); 
plot

#FIGURA 2: ppi-report
#Plotando apenas um tipo de dado por gráfico
#PP
plot <- ggplot(data, aes(x=reorder(Genome, PP), y=PP ));
plot <- plot + geom_bar(stat = "identity", position = 'dodge', fill="#66FAE9") + theme(axis.text.x = element_text(angle = 90, hjust = 1));
plot
ggsave("PP.png", units="cm", width=35, height=20, dpi=900)

#FIGURA 5: ppi-report
#CN
plot <- ggplot(data, aes(x=reorder(Genome, CN), y=CN ));
plot <- plot + geom_bar(stat = "identity", position = 'dodge', fill="#FF6666") + theme(axis.text.x = element_text(angle = 90, hjust = 1));
plot
ggsave("CN.png", units="cm", width=35, height=20, dpi=900)

#FIGURA 3: gene-neighborhood-conservation-report
awk -F',' 'BEGIN{printf "Genome\tGenes7\n"} !/#/{print $1 "\t" $3}' report.txt > cn.csv
#BOXPLOT FIGURA 3
library(ggplot2)
data=read.csv("cn.csv",header=TRUE,stringsAsFactors=FALSE, colClasses =c("character", "integer"), sep="\t");
p<-ggplot(data,aes(x=Genome, y=Genes7))+geom_boxplot(varwidth = TRUE); p+ theme(axis.text.x = element_text(angle = 90, hjust = 1));
ggsave("BoxPlot-CN-by-Genomes.png", units="cm", width=35, height=20, dpi=900)


