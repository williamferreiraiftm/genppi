					 (push (make-ppi-struct
						:genea (first (elt (second g) i))
						:geneb (first (elt (second g) j))
						:weight peso-grupo
						:position (second (elt (second g) i)))
					       ppi)

					 (push (make-ppi-struct
						:genea (first (elt (second (first g)) i))
						:geneb (first (elt (second (first g)) j))
						:weight peso-grupo
						:position (second (elt (second (first g)) i)))
					       ppi)

					 (push (make-ppi-struct
						:genea (if (< (second pg) (second ps)) (first pg) (first ps) )
						:geneb (if (< (second pg) (second pg)) (first ps) (first ps) )
						:weight (- peso-grupo (* peso-grupo (/ (/ (* diferenca-perfis 100) (length *genomes-files*)) 100)))
						:position (if (< (second pg) (second ps))  (second pg) (second ps) ) )
					       ppi)

