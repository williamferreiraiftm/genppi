defun equidimensional (a)
   (or (< (array-rank a) 2)
       (apply #'= (array-dimensions a))))
 (deftype square-matrix (&optional type size)
   `(and (array ,type (,size ,size))
         (satisfies equidimensional)))

 (let ( (var (make-array '(2 2) )   )) (declare (type square-matrix  var)) (setf (aref  var  0 1) 2) var)

CL-USER> (defstruct ppi-struct (genea "" :type string) (geneb "" :type string  ) (weight 0.0 :type short-float ) (position 0 :type integer))
 COMMON-LISP-USER::COPY-PPI-STRUCT in DEFUN
 COMMON-LISP-USER::PPI-STRUCT-P in DEFUN
WARNING:
   redefining (COMMON-LISP:SETF COMMON-LISP-USER::PPI-STRUCT-GENEA) in DEFUN
 COMMON-LISP-USER::PPI-STRUCT-GENEA in DEFUN
WARNING:
   redefining (COMMON-LISP:SETF COMMON-LISP-USER::PPI-STRUCT-GENEB) in DEFUN
 COMMON-LISP-USER::PPI-STRUCT-GENEB in DEFUN
WARNING:
   redefining (COMMON-LISP:SETF COMMON-LISP-USER::PPI-STRUCT-WEIGHT) in DEFUN
 COMMON-LISP-USER::PPI-STRUCT-WEIGHT in DEFUN
WARNING:
   redefining (COMMON-LISP:SETF COMMON-LISP-USER::PPI-STRUCT-POSITION) in DEFUN
 COMMON-LISP-USER::PPI-STRUCT-POSITION in DEFUN
 COMMON-LISP-USER::MAKE-PPI-STRUCT in DEFUN
PPI-STRUCT
CL-USER> (defvar a  (make-ppi-struct :genea "genea" :geneb "geneb"  :weight 0.3 :position 1 ))
A

CL-USER> (ppi-struct-genea a)
"genea"

CL-USER> (ppi-struct-geneb a)
"geneb"

CL-USER> (ppi-struct-position a)
1

CL-USER> (defvar b  (make-ppi-struct :genea "genea1" :geneb "geneb1"  :weight 0.6 :position 2 ))
B

CL-USER> (defvar lista (list A B))
LISTA

CL-USER> (car lista)
#S(PPI-STRUCT :GENEA "genea" :GENEB "geneb" :WEIGHT 0.3 :POSITION 1)

CL-USER> (setf b (make-ppi-struct :genea "genea1" :geneb "geneb1"  :weight 0.6 :position 2 ))
#S(PPI-STRUCT :GENEA "genea1" :GENEB "geneb1" :WEIGHT 0.6 :POSITION 2)

CL-USER> (setf lista (list A B))
(#S(PPI-STRUCT :GENEA "genea" :GENEB "geneb" :WEIGHT 0.3 :POSITION 1)
 #S(PPI-STRUCT :GENEA "genea1" :GENEB "geneb1" :WEIGHT 0.6 :POSITION 2))

CL-USER> lista
(#S(PPI-STRUCT :GENEA "genea" :GENEB "geneb" :WEIGHT 0.3 :POSITION 1)
 #S(PPI-STRUCT :GENEA "genea1" :GENEB "geneb1" :WEIGHT 0.6 :POSITION 2))

CL-USER> (let ( (a1 (make-ppi-struct :genea "genea" :geneb "geneb"  :weight 0.3 :position 1 ) ) ) (declare (type ppi-struct a1)) (ppi-struct-position a1))
1

CL-USER> (defun sortppi ( a b )  (if (> (ppi-struct-position a) (ppi-struct-position b)) 1 0))
SORTPPI

CL-USER> (sort lista  #'sortppi)
(#S(PPI-STRUCT :GENEA "genea" :GENEB "geneb" :WEIGHT 0.3 :POSITION 1)
 #S(PPI-STRUCT :GENEA "genea1" :GENEB "geneb1" :WEIGHT 0.6 :POSITION 2))

CL-USER> (defun sortppi ( a b )  (if (< (ppi-struct-position a) (ppi-struct-position b)) 1 0))
SORTPPI

CL-USER> (sort lista  #'sortppi)
(#S(PPI-STRUCT :GENEA "genea1" :GENEB "geneb1" :WEIGHT 0.6 :POSITION 2)
 #S(PPI-STRUCT :GENEA "genea" :GENEB "geneb" :WEIGHT 0.3 :POSITION 1))

CL-USER> (let ( (a1 (make-ppi-struct :genea "genea" :geneb "geneb"  :weight 0.3 :position 1 ) ) ) (declare (type ppi-struct a1)) (ppi-struct-position a1))
1
