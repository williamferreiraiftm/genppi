select count(1) from
(select distinct noa, nob from (
	select noa, nob from string
	intersect
	select noa, nob from genppi
union
	select noa, nob from
		(select nob, noa from string
		intersect
		select noa, nob from genppi) as oposite
) as complete ) as total
--delete from genppi;
--select count(1) from genppi
--select count(1) from string
--select * from genppi where noa = "CPTC_00032" and nob = "CPTC_02179"
--select * from string   where noa = "CPTC_00032" and nob = "CPTC_02179"